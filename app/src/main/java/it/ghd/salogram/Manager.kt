package it.ghd.salogram

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.util.Patterns
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.DownloadListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.gson.Gson
import com.onesignal.OneSignal
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.model.*
import me.leolin.shortcutbadger.ShortcutBadger
import okhttp3.Credentials
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

class Manager {

    companion object {
        val instance = Manager()

        private var homeJSON = JSONObject()
        val SHARED_PREFERENCES_USER_FIELD = "user"
        private val SHARED_PREFERENCES_TOKEN_FIELD = "token"
        val SHARED_PREFERENCES_PUSH_TOKEN_FIELD = "push_token"
        val SHARED_PREFERENCES_NOTIFICATIONS_FIELD = "notifications"
        private val SERVER = "https://salogram.com/api/"
        const val SHARED_PREFERENCES_NAME = Utils.APP_NAME
        private const val MESSAGE_NO_INTERNET = "Connessione ad Internet non disponibile"
        public var logged: Boolean = false
        private const val TAG_NAME =  Utils.APP_NAME
        var currentNotificationBadge = 0
        var notifications:JSONArray = JSONArray()
        var categories:JSONArray = JSONArray()

        var postHomeContents:JSONArray = JSONArray()
        var storyHomeContents:JSONArray = JSONArray()

        var user:JSONObject = JSONObject()
            get() = field

        fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email.trim { it <= ' ' }) && Patterns.EMAIL_ADDRESS.matcher(
                email
            ).matches()
        }

        //usato per i video
        fun shareToFacebookPageByURL(
            mediaurl: String,
            content: String,
            listener: (Boolean, String?, String?) -> Unit
        ) {
            val type = "video"
            val address = SERVER + "tokens/post-to-page-by-url"
            Utils.log("Connecting to $address")

            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] address = $address")
            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] content = $content")

            AndroidNetworking.post(address).apply {
                addBodyParameter("content", content)
                addBodyParameter("type", type)
                addBodyParameter("mediaurl", mediaurl)
                    .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                    //.addJSONObjectBody(jsonObject)
                    .setTag(TAG_NAME)
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener { bytesUploaded, totalBytes ->
                        Log.v("FACEBOOKPAOLO", "$bytesUploaded / $totalBytes")
                    }
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            // do anything with response
                            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] " + response.toString())

                            val messaggio = response.getString("message")
                            //val result = Gson().fromJson(response.toString(), FacebookResponse::class.java)
                            val success = response.getString("status") == "success"

                            val postID = "https://www.facebook.com/" + response.getString("data")

                            if (success) {
                                listener(false, messaggio, postID)
                            }

                            val insuccess = response.getString("status") == "error"
                            if (insuccess) {
                                val dataError = response.getString("data")
                                val resultObj =
                                    Gson().fromJson(dataError, FacebookError::class.java)
                                val msg = resultObj.message
                                listener(true, msg, "")
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] onError")
                            Log.v(
                                "FACEBOOKPAOLO",
                                "[shareToFacebookPage] response: " + error.response
                            )
                            Log.v(
                                "FACEBOOKPAOLO",
                                "[shareToFacebookPage] codice: " + error.errorCode
                            )
                            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] body: " + error.errorBody)
                            Log.v(
                                "FACEBOOKPAOLO",
                                "[shareToFacebookPage] errorDetail: " + error.errorDetail
                            )

                            listener(true, error.toString(), "")
                        }
                    })
            }
        }

        fun shareToFacebookPage(
            file: File,
            content: String,
            is_video: Boolean,
            listener: (Boolean, String?, String?) -> Unit
        ) {

            val type = if (is_video) "video" else "image"
            val address = SERVER + "tokens/post-to-page"
            Utils.log("Connecting to $address")

            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] address = $address")

            AndroidNetworking.upload(address)
            .addMultipartFile("media",file)
            .addMultipartParameter("content", content)
            .addMultipartParameter("type", type)
            .addHeaders("Authorization", "Bearer ${Utils.appToken}")
            //.addJSONObjectBody(jsonObject)
            .setTag(TAG_NAME)
            .setPriority(Priority.HIGH)
            .build()
            .setUploadProgressListener {
                    bytesUploaded, totalBytes ->
                    Log.v("FACEBOOKPAOLO", "$bytesUploaded / $totalBytes")
            }
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {
                    // do anything with response
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] " + response.toString())

                    val messaggio = response.getString("message")
                    //val result = Gson().fromJson(response.toString(), FacebookResponse::class.java)
                    val success = response.getString("status") == "success"

                    val postID = "https://www.facebook.com/" + response.getString("data")

                    if (success) {
                        listener(false, messaggio, postID)
                    }

                    val insuccess = response.getString("status") == "error"
                    if (insuccess) {
                        val dataError = response.getString("data")
                        val resultObj = Gson().fromJson(dataError, FacebookError::class.java)
                        val msg = resultObj.message
                        listener(true, msg, "")
                    }
                }

                override fun onError(error: ANError) {
                    // handle error
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] onError")
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] response: " + error.response)
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] codice: " + error.errorCode)
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] body: " + error.errorBody)
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] errorDetail: " + error.errorDetail)

                    listener(true, error.toString(), "")
                }
            })
        }

        fun register(
            user: JSONObject,
            listener: (Boolean, String?) -> Unit
        ) {

            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {

                    val address = SERVER + "user" + "?" + Random.nextInt(
                        0,
                        Int.MAX_VALUE
                    )
                    Utils.log("Connecting to $address")

                    AndroidNetworking.patch(address)
                        .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        .addJSONObjectBody(user)
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val result = response.getString("status") == "success"

                                    if (result) {
                                        listener(false, null)
                                    } else {
                                        listener(
                                            true,
                                            response.getString("message") ?: "Errore sconosciuto"
                                        )
                                    }
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato")
                                    return
                                }
                            }

                            override fun onError(error: ANError?) {
                                // handle error
                                try {
                                    if (error == null) {
                                        return listener(
                                            true,
                                            "Ricevuta risposta vuota, riprova tra qualche minuto."
                                        )
                                    } else {
                                        val errBody = JSONObject(error.errorBody)

                                        if (errBody.has("status") && errBody.getString("status") == "fail") {
                                            if (errBody.has("message")) {
                                                if (errBody.get("message") is String) {
                                                    return listener(
                                                        true,
                                                        errBody.getString("message")
                                                    )
                                                } else if (errBody.get("message") is JSONArray) {
                                                    return listener(
                                                        true,
                                                        errBody.getJSONArray("message").toString()
                                                    )
                                                } else {
                                                    return listener(true, "Errore sconosciuto")
                                                }
                                            } else {
                                                return listener(true, "Errore sconosciuto")
                                            }


                                        } else {
                                            return listener(true, errBody.toString())
                                        }
                                    }
                                } catch (e: JSONException) {
                                    return listener(
                                        true,
                                        "Errore: " + (error?.localizedMessage ?: "Sconosciuto")
                                    )
                                }
                            }
                        })
                }//connected
            } //connection check

        }

        fun login(
            email: String,
            password: String,
            autoLogin: Boolean,
            listener: (Boolean, String?, Boolean, String?, JSONArray, Boolean) -> Unit
        ) {
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET, false, null, JSONArray(), false)
                } else {
                    
                    val address = SERVER + "auth/jwt/token"

                    Utils.log("Connecting to $address")

                    val loginInfo: String = "$email:$password"
                    val encodingByte: ByteArray = Base64.encode(
                        loginInfo.toByteArray(),
                        Base64.NO_WRAP
                    )
                    val encoding = String(encodingByte)

                    AndroidNetworking.post(address).apply {
                        addBodyParameter(
                            "dev",
                            (if (Utils.isEmulator || Utils.isDebugger) 1 else 0).toString()
                        )
                        addBodyParameter("device_name", "${android.os.Build.MODEL}")
                        addBodyParameter("device_type", "Android")

                        val pushToken = Utils.sharefPrefs.getString(
                            SHARED_PREFERENCES_PUSH_TOKEN_FIELD,
                            ""
                        ) ?: ""

                        if (pushToken.isNotEmpty()) addBodyParameter("device_token", pushToken)

                        Log.v("LOGIN_APP", "email = " + email)
                        Log.v("LOGIN_APP", "pushToken")
                        Log.v("LOGIN_APP", pushToken)

                        addBodyParameter("password", password)
                        addHeaders("Authorization", Credentials.basic(email, password))
                        addHeaders("Accept", "application/vnd.salogram.v3+json") //API v2

                      /*  pushToken?.let {
                            addBodyParameter("os", "android")
                            addBodyParameter("pushtoken", pushToken)
                            if (Utils.isDebugger || Utils.isEmulator) {
                                addBodyParameter("pushenv","dev")
                            }
                        }*/

                        setTag(Utils.APP_NAME)
                        setPriority(Priority.MEDIUM)

                    }.build().getAsJSONObject(object : JSONObjectRequestListener {
                        @SuppressLint("ApplySharedPref")
                        override fun onResponse(response: JSONObject) {
                            // do anything with response
                            try {

                                Log.v("PAOLO", "login ok")

                                val statusCode = response.getInt("status_code")
                                val data = response.getJSONObject("data") ?: JSONObject()
                                when (statusCode) {
                                    200 -> {

                                        // PHONE NUMBER CONFIRMED
                                        val askPhoneConfirm = data.getBoolean("ask_phone_confirm") ?: false
                                        val currentPhone = data.getString("current_phone") ?: ""
                                        Log.v("PAOLO", "askPhoneConfirm = " + askPhoneConfirm)
                                        Log.v("PAOLO", "currentPhone = " + currentPhone)

                                        ////////////////////////////////////////////
                                        // OWNERS && ACCOUNT
                                        ////////////////////////////////////////////
                                        Log.v("PAOLO", "ownerJson")
                                        val ownerJson = response.getJSONObject("owner") ?: JSONObject()
                                        Log.v("PAOLO", ownerJson.toString())
                                        val owner = Owner.parse(ownerJson)
                                        val accounts = response.getJSONArray("accounts") ?: JSONArray()
                                        ////////////////////////////////////////////

                                        val userID = data.getString("user_id") ?: ""
                                        val fbPageJson = if (data.has("facebook_page")) data.getJSONObject("facebook_page") else JSONObject()

                                        checkFacebokPages(fbPageJson)
                                        checkFacebookUser(userID)

                                        val accessToken = data.getString("jwt") ?: ""
                                        val emailClean = Utils.getOriginalMail(email)

                                        Utils.sharefPrefsEditor.apply {
                                            putJSONObject(SHARED_PREFERENCES_USER_FIELD, user)
                                            putString(SHARED_PREFERENCES_TOKEN_FIELD, accessToken)
                                            putString("email", emailClean)
                                            putString("password", password)
                                            putString("user_id", userID)
                                            putString("phone", currentPhone)
                                            putJSONObject("owner", ownerJson)
                                            putJSONArray(EditorMainFragment.USER_ACCOUNTS, accounts)
                                            putBoolean("autologin", autoLogin)
                                        }.commit()

                                        Utils.log("onResponse storing token: " + accessToken)

                                        if (data.has("user_id")) {
                                            OneSignal.setExternalUserId(data.getString("user_id"))
                                            OneSignal.setSMSNumber(currentPhone)
                                        }

                                        //END CHECK ME

                                        Manager.logged = true

                                        recordDeviceInfo { error, message ->

                                            val errore  = false
                                            val messaggio = null
                                            val firstStart = data.getBoolean("first_start")
                                            val accounts1 = JSONArray()
                                            val updateNeed = false
                                            listener(errore, messaggio, firstStart, userID, accounts1, askPhoneConfirm )
                                        }

                                    }

                                    301 -> {
                                        val accounts = data.getJSONArray("accounts") ?: JSONArray()
                                        //val login = data.getString("login") ?: ""
                                        listener(true, null, false, null, accounts, false)
                                    }
                                }

                                //val fbPageJson = data.getJSONObject("facebook_page")
                                return

                            } catch (e: JSONException) {
                                //e.printStackTrace()
                                Log.v("PAOLO", "JSONException")
                                Log.v("PAOLO", e.localizedMessage)


                                listener(true, "Errore formato", false, null, JSONArray(), false)
                                return
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error

                            Log.v("PAOLO", "onError login")

                            try {
                                val errBody = JSONObject(error.errorBody)

                                Log.v("PAOLO", "errBody = " + errBody)

                                var data = ""
                                /*
                                var data = errBody.getString("data")
                                if (!data.isNullOrEmpty()) {

                                }
                                */

                                listener(true, errBody.getString("message") ?: "Errore sconosciuto", false, data, JSONArray(), false)
                            } catch (e: JSONException) {
                                val needUpdate = false
                                Log.v("PAOLO", "onError login catch")
                                listener(true, "Errore: ${error.message}", false, null, JSONArray(), false)
                            }
                            return
                        }
                    })
                }//connected
            }//icheck
        }

        private fun checkFacebokPages(fbPageJson: JSONObject) {

            Log.v("FACEBOOKPAOLO", "[checkFacebokPages] fbPageJson = " + fbPageJson.toString())

            val facebookPage = Gson().fromJson(fbPageJson.toString(), FacebookPage::class.java)

            if (facebookPage.selected) {
                Log.v("FACEBOOKPAOLO", "[checkFacebokPages] salvo pagina facebook sui preferiti")
                Utils.sharefPrefsEditor.apply {
                    putJSONObject(EditorMainFragment.FB_PAGE, fbPageJson)
                }
            }
            else {
                Log.v("FACEBOOKPAOLO", "[checkFacebokPages] rimuovo pagina facebook dai preferiti")
                Utils.sharefPrefsEditor.apply {
                    remove(EditorMainFragment.FB_PAGE)
                }
            }

            Log.v("FACEBOOKPAOLO", "[checkFacebokPages] facebookPage = " + facebookPage.item_name)
        }

        private fun checkFacebookUser(userID : String) {
            val facebookUserID = Utils.sharefPrefs.getString(EditorMainFragment.FB_LOGGED_USER, "")

            if (!facebookUserID.isNullOrEmpty()) {
                if (facebookUserID == userID) {
                    Log.v("FACEBOOKPAOLO", "[checkFacebookUser] stesso utente ($facebookUserID)")
                }
                else {
                    Log.v("FACEBOOKPAOLO", "[checkFacebookUser] utente diverso ($facebookUserID)")
                    LoginManager.getInstance().logOut()
                    Utils.sharefPrefsEditor.apply {
                        remove(EditorMainFragment.FB_LOGGED_USER)
                    }
                }
            } else {
                Log.v("FACEBOOKPAOLO", "[checkFacebookUser] nessun utente facebook trovato")
            }
        }

        fun recordDeviceInfo(
            listener: (Boolean, String?) -> Unit
        ) {

            if (!logged){
                listener(false, null)
            }

            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {
                    val address =
                        SERVER + "devices"

                    Utils.log("Connecting to $address")

                    AndroidNetworking.post(address).apply {
                        addBodyParameter(
                            "dev",
                            (if (Utils.isEmulator || Utils.isDebugger) 1 else 0).toString()
                        )

                        val versionName = CustomApplication.context.packageManager.getPackageInfo(CustomApplication.context.packageName, 0).versionName
                        val versionCode = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                            CustomApplication.context.packageManager.getPackageInfo(CustomApplication.context.packageName, 0).longVersionCode
                        } else {
                            CustomApplication.context.packageManager.getPackageInfo(CustomApplication.context.packageName, 0).versionCode
                        }

                        addBodyParameter("device_name", android.os.Build.MODEL)
                        addBodyParameter("device_type", "Android")
                        addBodyParameter("device_os", "${android.os.Build.VERSION.SDK_INT}")
                        addBodyParameter("app_build", "$versionCode")
                        addBodyParameter("app_version", versionName)

                        if (Utils.oneSignalUserID != null) {
                            addBodyParameter("onesignal_player_id", Utils.oneSignalUserID)
                        }

                        addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        setTag(Utils.APP_NAME)
                        setPriority(Priority.MEDIUM)

                        val pushToken = Utils.pushToken ?: ""
                        if (pushToken.isNotEmpty()) {
                            addBodyParameter("device_token", pushToken)
                        }

                    }.build().getAsJSONObject(object : JSONObjectRequestListener {
                        @SuppressLint("ApplySharedPref")
                        override fun onResponse(response: JSONObject) {
                            // do anything with response
                            try {

                                //val data = response.getJSONObject("data") ?: JSONObject()

                                listener(false, null)
                                return
                            } catch (e: JSONException) {
                                e.printStackTrace()
                                listener(true, "Errore formato addtoFav")
                                return
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            try {
                                val errBody = JSONObject(error.errorBody)
                                listener(
                                    true,
                                    errBody.getString("message") ?: "Errore sconosciuto"
                                )
                            } catch (e: JSONException) {
                                listener(true, "Errore: $error.localizedMessage")
                            }
                            return
                        }
                    })
                }//connected
            }
        }

        fun updateProfile(
            user: JSONObject,
            listener: (Boolean, String?) -> Unit
        ) {
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {

                    val address = SERVER + "user"

                    Utils.log("Connecting to $address")

                    AndroidNetworking.patch(address).apply {
                        addJSONObjectBody(user)
                        addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        setTag(Utils.APP_NAME)
                        setPriority(Priority.MEDIUM)

                    }.build().getAsJSONObject(object : JSONObjectRequestListener {
                        @SuppressLint("ApplySharedPref")
                        override fun onResponse(response: JSONObject) {
                            // do anything with response
                            try {

                                //val data = response.getJSONObject("data") ?: JSONObject()

                                listener(false, null)
                                return
                            } catch (e: JSONException) {
                                e.printStackTrace()
                                listener(true, "Errore formato")
                                return
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            try {
                                val errBody = JSONObject(error.errorBody)
                                listener(
                                    true,
                                    errBody.getString("message") ?: "Errore sconosciuto"
                                )
                            } catch (e: JSONException) {
                                listener(true, "Errore: $error.localizedMessage")
                            }
                            return
                        }
                    })
                }//connected
            }//icheck
        }

        fun getPost(
            postID: Int,
            listener: (Boolean, String?, JSONObject?) -> Unit
        ) {
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {

                        val address = SERVER + "posts/$postID"

                        Log.v("PAOLO", "postID = $address")
                        //Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                listener(false, "Post scaricato correttamente", response)
                            }

                            override fun onError(anError: ANError?) {
                                listener(true, "Errore formato ${javaClass.enclosingMethod?.name}", null)
                            }
                        })
                    }
                }
            }
        }

        fun getAllPosts(
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address = SERVER + "posts"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(
                                        true,
                                        "Errore formato ${javaClass.enclosingMethod?.name}",
                                        null
                                    )
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getAllStories(
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address = SERVER + "stories"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(
                                        true,
                                        "Errore formato ${javaClass.enclosingMethod?.name}",
                                        null
                                    )
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }


        fun getAllTutorials(
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address = SERVER + "tutorials"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(
                                        true,
                                        "Errore formato ${javaClass.enclosingMethod?.name}",
                                        null
                                    )
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getHome(
            force: Boolean = false,
            isPosts: Boolean = true,
            removeArrays: Boolean = false,
            listener: (Boolean, String?, JSONObject?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {

                        if (force){
                            AndroidNetworking.evictAllBitmap();
                        }

                        val address =
                            SERVER + "home" + "?${Random.nextInt(
                                0,
                                Int.MAX_VALUE
                            )}"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONObject("data") ?: JSONObject()
                                    homeJSON = data

                                    notifications = data.getJSONArray("notifications")

                                    categories = data.getJSONArray("categories")

                                    currentNotificationBadge = notifications.length()

                                    for (not: JSONObject in notifications) {
                                        if (not.has("read_at") && !not.isNull("read_at")) {
                                            currentNotificationBadge--;
                                            //notificationMarkRead(not)
                                        }
                                    }

                                    //updateNotificationsCount()

                                    if (ShortcutBadger.isBadgeCounterSupported(CustomApplication.context)) {
                                        ShortcutBadger.applyCount(
                                            CustomApplication.context,
                                            currentNotificationBadge
                                        );
                                    }

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato getHome", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getAllCategories(
            isPosts: Boolean = true,
            removeArrays: Boolean = false,
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address =
                            SERVER + "categories"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    var destArray = JSONArray()

                                    val fieldName = if (isPosts) "posts" else "stories"

                                    for (jo: JSONObject in data) {
                                        if (!jo.isNull(fieldName) && jo.get(fieldName) is JSONArray) {
                                            if (jo.getJSONArray(fieldName).length() > 0) {

                                                var json = jo

                                                if (removeArrays) {
                                                    json.remove(fieldName)
                                                }
                                                destArray.put(jo)
                                            }
                                        }
                                    }

                                    listener(false, null, destArray)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato getallcategories", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun sendNotificationView(
            notificatID: Int, listener: (Boolean, String?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET)
                    } else {

                        val address = SERVER + "notifications/$notificatID}"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.put(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(false, null)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato addtoFav")
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto"
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage")
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getAllNotifications(
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){

            if (!logged){
                listener(false, null, null)
                return
            }

            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address =
                            SERVER + "notifications"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            doNotCacheResponse()
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    notifications = data

                                    currentNotificationBadge = data.length()

                                    for (not: JSONObject in notifications) {
                                        if (not.has("read_at") && !not.isNull("read_at")) {
                                            //notificationMarkRead(not)
                                            currentNotificationBadge--
                                        }
                                    }

                                    print("Notifications count: ${currentNotificationBadge}")

                                    //updateNotificationsCount()

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato refreshtoken", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getProfileInformations(
            listener: (Boolean, String?, JSONObject?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {

                        val address = SERVER + "user"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(false, null, data)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato getProfileInformations", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(true, errBody.getString("message") ?: "Errore sconosciuto", null)
                                    Log.v("NOTIFICHE_ERROR", errBody.getString("message"))
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun addtoFav(
            fav: JSONObject, listener: (Boolean, String?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET)
                    } else {


                        val address =
                            SERVER + "favourites/${fav.getInt("id")}"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.post(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(false, null)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato addtoFav")
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto"
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage")
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun removeFav(
            fav: JSONObject, listener: (Boolean, String?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET)
                    } else {


                        val address =
                            SERVER + "favourites/${fav.getInt("id")}"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.delete(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(false, null)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato removeFav")
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto"
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage")
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getAllFavorites(
            listener: (Boolean, String?, JSONArray?, JSONArray?) -> Unit
        ){

            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null, null)
                    } else {


                        val address =
                            SERVER + "favourites"

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(
                                        false,
                                        null,
                                        data.getJSONArray("posts") ?: JSONArray(),
                                        data.getJSONArray(
                                            "stories"
                                        ) ?: JSONArray()
                                    )
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato getAllFavorites", null, null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null,
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null, null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getAllFrames(
            id: Int = 0,
            posts: Boolean = true,
            listener: (Boolean, String?, JSONArray?) -> Unit
        ){
            refreshToken(false) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET, null)
                    } else {


                        val address = SERVER + "frames/"+ (if (posts) "post/" else "stories/") + id +"/" + "?"+ Random.nextInt(
                            0,
                            Int.MAX_VALUE
                        )

                        Utils.log("Connecting to $address")

                        AndroidNetworking.get(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)
                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONArray("data") ?: JSONArray()

                                    var filter: JSONArray = JSONArray()

                                    for (jo: JSONObject in data) {
                                        if (jo.getString("type") == "posts" && posts) {
                                            filter.put(jo)
                                        } else if (jo.getString("type") == "stories" && !posts) {
                                            filter.put(jo)
                                        }
                                    }

                                    listener(false, null, filter)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato getAllFrames", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto",
                                        null
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage", null)
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun forgotPassword(
            email: String,
            listener: (Boolean, String?) -> Unit
        ) {


            InternetCheck { connected ->
                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {

                    Utils.sharefPrefsEditor.remove("autologin").commit()

                    val address = SERVER + "auth/jwt/forgot?${email}"
                    Utils.log("Connecting to $address")

                    val body = JSONObject().apply {
                        put("email", email)
                    }

                    AndroidNetworking.post(address)
                        .addJSONObjectBody(body)
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //status = success / <0
                                    if (response.has("status")) {
                                        if (response.get("status") is String && response.getString("status")
                                                .contains(
                                                    "success"
                                                ) || response.get("status") is Int && response.getInt(
                                                "status"
                                            ) >= 0
                                        ) {

                                            return listener(false, null)
                                        }
                                    }
                                    return listener(
                                        true, if (response.has("message")) response.getString(
                                            "message"
                                        ) else "Errore sconosciuto"
                                    )

                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato ricevuto ${response.toString()}")
                                    return
                                }
                            }

                            override fun onError(error: ANError?) {
                                // handle error
                                try {
                                    if (error == null) {
                                        listener(
                                            true,
                                            "Ricevuta risposta vuota, riprova tra qualche minuto."
                                        )
                                    } else {
                                        val errBody = JSONObject(error.errorBody)

                                        if (errBody.has("status") && errBody.getString("status") == "fail") {
                                            if (errBody.has("message")) {
                                                if (errBody.get("message") is String) {
                                                    listener(
                                                        true,
                                                        errBody.getString("message")
                                                    )
                                                } else if (errBody.get("message") is JSONArray) {
                                                    listener(
                                                        true,
                                                        errBody.getJSONArray("message")
                                                            .toString()
                                                    )
                                                } else {
                                                    listener(true, "Errore sconosciuto")
                                                }
                                            } else {
                                                listener(true, "Errore sconosciuto")
                                            }
                                        } else {
                                            listener(true, errBody.toString())
                                        }
                                    }
                                } catch (e: JSONException) {
                                    listener(
                                        true,
                                        "Errore: " + (error?.localizedMessage
                                            ?: "Sconosciuto")
                                    )
                                }
                            }
                        })
                } //connection check
            }//f
        }//logout

        fun logout(
            listener: (Boolean, String?) -> Unit
        ) {

            refreshToken(false) { error, message ->

                logged = false

                val pushToken = Utils.pushToken //is stored inside shared prefs

                OneSignal.logoutSMSNumber()
                OneSignal.logoutEmail()

                if (pushToken == null) {
                    Log.v("LOGOUT", "push token nullo")
                    listener(false, null)
                }
                else {
                    Log.v("LOGOUT", "push token NON nullo")

                    InternetCheck { connected ->
                        Utils.log("Is connection enabled? " + connected)

                        if (!connected) {
                            listener(true, MESSAGE_NO_INTERNET)
                        } else {

                            val address = SERVER + "auth/jwt/token?" + Random.nextInt(0, Int.MAX_VALUE)
                            Utils.log("Connecting to $address")

                            val body = JSONObject().apply {

                            }

                            Log.v("LOGOUT", "chiamo DELETE")

                            AndroidNetworking.delete(address)
                                .addJSONObjectBody(body)
                                .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                                .setTag(TAG_NAME)
                                .setPriority(Priority.MEDIUM)
                                .build()
                                .getAsJSONObject(object : JSONObjectRequestListener {
                                    override fun onResponse(response: JSONObject) {
                                        // do anything with response
                                        try {

                                            //status = success

                                            if (response.has("status")) {
                                                response.getString("status").let {

                                                    if (it.contains("success")) {

                                                        Log.v("LOGOUT", "logout fatto correttamente")

                                                        AndroidNetworking.evictAllBitmap()
                                                        AndroidNetworking.forceCancelAll()
                                                        Utils.sharefPrefsEditor.clear().commit()
                                                        Log.v("NOTIFICHE","cancello tutte le preferenze")

                                                        return listener(false, null)
                                                    }

                                                    return listener(true, it)
                                                }
                                            }

                                            return listener(true, "Errore sconosciuto")

                                        } catch (e: JSONException) {
                                            e.printStackTrace()
                                            listener(true, "Errore formato")
                                            return
                                        }
                                    }

                                    override fun onError(error: ANError?) {
                                        // handle error
                                        try {
                                            if (error == null) {
                                                listener(
                                                    true,
                                                    "Ricevuta risposta vuota, riprova tra qualche minuto."
                                                )
                                            } else {

                                                Log.v("LOGOUT", "errore di DELETE")
                                                Log.v("LOGOUT", error.toString())

                                                val errBody = JSONObject(error.errorBody)

                                                if (errBody.has("status") && errBody.getString("status") == "fail") {
                                                    if (errBody.has("message")) {
                                                        if (errBody.get("message") is String) {
                                                            listener(
                                                                true,
                                                                errBody.getString("message")
                                                            )
                                                        } else if (errBody.get("message") is JSONArray) {
                                                            listener(
                                                                true,
                                                                errBody.getJSONArray("message")
                                                                    .toString()
                                                            )
                                                        } else {
                                                            listener(true, "Errore sconosciuto")
                                                        }
                                                    } else {
                                                        listener(true, "Errore sconosciuto")
                                                    }
                                                } else {
                                                    listener(true, errBody.toString())
                                                }
                                            }
                                        } catch (e: JSONException) {
                                            listener(
                                                true,
                                                "Errore: " + (error?.localizedMessage
                                                    ?: "Sconosciuto")
                                            )
                                        }
                                    }
                                })
                        } //connection check
                    }//f
                }//else refreshtokens
            }
        }//logout

        private fun refreshToken(refreshToken: Boolean, listener: (Boolean, String?) -> Unit) {

            if (!refreshToken){
                Utils.log("NOT REFRESHING TOKEN!")
                listener(false, null)
                return
            }

            InternetCheck { connected ->
                Utils.log("Is connection enabled? $connected")
                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {

                    //Getting token

                    val token =    Utils.appToken
                    //change token with  refresh token

                    val address = SERVER + "auth/jwt/refresh"
                    Utils.log("Connecting to $address")

                    AndroidNetworking.get(address)
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    val data = response.getJSONObject("data") ?: JSONObject()

                                    val accessToken = data.getString("jwt")
                                        ?: ""

                                    Utils.appToken = accessToken

                                    listener(false, null)
                                    return

                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato refreshToken")
                                    return
                                }
                            }

                            override fun onError(error: ANError?) {
                                // handle error
                                listener.let {
                                    try {
                                        if (error == null || error.errorBody == null) {
                                            return listener(
                                                true,
                                                "Ricevuta risposta vuota, riprova tra qualche minuto."
                                            )
                                        } else {
                                            val errBody = JSONObject(error.errorBody)

                                            if (errBody.has("status") && (errBody.getString("status") == "fail" || errBody.getString(
                                                    "status"
                                                ) == "errore")
                                            ) {
                                                if (errBody.has("message")) {
                                                    if (errBody.get("message") is String) {
                                                        return listener(
                                                            true,
                                                            errBody.getString("message")
                                                        )
                                                    } else if (errBody.get("message") is JSONArray) {
                                                        return listener(
                                                            true,
                                                            errBody.getJSONArray("message")
                                                                .toString()
                                                        )
                                                    } else {
                                                        return listener(true, "Errore sconosciuto")
                                                    }
                                                } else {
                                                    return listener(true, "Errore sconosciuto")
                                                }
                                            } else {
                                                return listener(true, errBody.toString())
                                            }
                                        }
                                    } catch (e: JSONException) {
                                        return listener(
                                            true,
                                            "Errore: " + (error?.localizedMessage ?: "Sconosciuto")
                                        )
                                    }
                                }
                            }
                        }) //get json
                } //else
            }
        }

        fun downloadFile(url: String, name: String, listener: (Boolean, String?, Uri?) -> Unit){

            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().build())

            Utils.log("Downloading into ${Utils.pathToDownloadFiles()}")

            AndroidNetworking.download(url, Utils.pathToDownloadFiles(), name)
                .setTag("donwload")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener { bytesDownloaded, totalBytes ->
                    // do anything with progress
                    if (Utils.isDebugger || Utils.isEmulator){
                        Utils.log("$bytesDownloaded of $totalBytes - ${bytesDownloaded.toFloat() / totalBytes * 100}%")
                    }
                }
                .startDownload(object : DownloadListener {
                    override fun onDownloadComplete() {
                        // do anything after completion
                        listener(false, null, Utils.uriForDownlaodedFile(name))
                    }

                    override fun onError(error: ANError?) {
                        // handle error
                        listener(true, error?.localizedMessage ?: "Sconosciuto", null)
                    }
                })
        }

        fun notificationIsRead(row: JSONObject): Boolean {
            var nots =
                Utils.sharefPrefs.getStringSet(
                    SHARED_PREFERENCES_NOTIFICATIONS_FIELD,
                    setOf<String>()
                )
                    ?: setOf<String>()

            return nots.contains(row.getString("id") ?: "")
        }

        fun updateNotificationsCount(){
            var nots =
                Utils.sharefPrefs.getStringSet(
                    SHARED_PREFERENCES_NOTIFICATIONS_FIELD,
                    setOf<String>()
                )
                    ?: setOf<String>()

            currentNotificationBadge = if (currentNotificationBadge-nots.size>=0) currentNotificationBadge-nots.size else 0
        }

        /*fun notificationMarkRead(row: JSONObject) {

            var nots =
                Utils.sharefPrefs.getStringSet(SHARED_PREFERENCES_NOTIFICATIONS_FIELD, setOf<String>())
                    ?.toMutableSet() ?: setOf<String>().toMutableSet()

            if (row.has("id")) {

                if (!nots.contains(row.getString("id"))) {

                    nots.add(row.getString("id")) //add

                    Utils.sharefPrefsEditor.putStringSet(
                        SHARED_PREFERENCES_NOTIFICATIONS_FIELD,
                        nots
                    ).commit() //update

                    currentNotificationBadge--

                    if (ShortcutBadger.isBadgeCounterSupported(CustomApplication.context)) {
                        ShortcutBadger.applyCount(CustomApplication.context, currentNotificationBadge);
                    }

                    //updateNotificationsCount()
                }
                else {
                    //already marked as read
                }
            }
        }*/

        //STATS
        fun recordShare(
            content: JSONObject,
            frame: JSONObject?,
            destination: String?,
            listener: (Boolean, String?) -> Unit
        ){
            refreshToken(true) { error, message ->

                InternetCheck { connected ->

                    Utils.log("Is connection enabled? " + connected)

                    if (!connected) {
                        listener(true, MESSAGE_NO_INTERNET)
                    } else {

                        val address = SERVER + "shares"

                        Utils.log("Connecting to $address")

                        val body = JSONObject().apply {
                            put("device_type", "android")
                            put(
                                "type",
                                if (content.getString("type") == "story") "story" else "post"
                            )
                            put("post_id", content.getInt("id"))

                            if (frame!=null){
                                put("frame_id", frame.getInt("id"))
                            }

                            if (destination != null){
                                put("destination", destination)
                            }

                            if (content.has("extra")) {
                                val extra = content.getString("extra")
                                put("extra", extra)
                                Log.v("FACEBOOKPAOLO", "aggiugno extra anche qui: $extra")
                            }
                        }

                        //recordFirebaseEvent()

                        AndroidNetworking.post(address).apply {
                            addHeaders("Authorization", "Bearer ${Utils.appToken}")
                            addJSONObjectBody(body)
                            setTag(Utils.APP_NAME)
                            setPriority(Priority.MEDIUM)

                        }.build().getAsJSONObject(object : JSONObjectRequestListener {
                            @SuppressLint("ApplySharedPref")
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //val data = response.getJSONObject("data") ?: JSONObject()

                                    listener(false, null)
                                    return
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato addtoFav")
                                    return
                                }
                            }

                            override fun onError(error: ANError) {
                                // handle error
                                try {
                                    val errBody = JSONObject(error.errorBody)
                                    listener(
                                        true,
                                        errBody.getString("message") ?: "Errore sconosciuto"
                                    )
                                } catch (e: JSONException) {
                                    listener(true, "Errore: $error.localizedMessage")
                                }
                                return
                            }
                        })
                    }//connected
                }//icheck
            }
        }

        fun getFacebookAccounts(
            listener: (Boolean, String?, FacebookRegistrationResponse?) -> Unit
        ){
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET, null)
                } else {

                    val address = SERVER + "tokens/accounts"
                    Utils.log("Connecting to $address")
                    Log.v("FACEBOOKPAOLO", address)

                    AndroidNetworking.get(address)
                    .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                    .setTag(TAG_NAME)
                    .setPriority(Priority.MEDIUM)
                    .build().getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject?) {
                            Log.v("FACEBOOKPAOLO", "[getFacebookAccounts] onResponse")
                            Log.v("FACEBOOKPAOLO", response.toString())
                            Log.v("FACEBOOKPAOLO", "[getFacebookAccounts] fine")
                            val pagine = Gson().fromJson(response.toString(), FacebookRegistrationResponse::class.java)
                            listener(false, "ok", pagine)
                            return
                        }

                        override fun onError(anError: ANError?) {
                            Log.v("FACEBOOKPAOLO", "[getFacebookAccounts] onError")
                            listener(true, anError?.localizedMessage, null)
                            return
                        }
                    })
                }
            }
        }

        fun getFacebookPages(
            listener: (Boolean, String?, FacebookDataResponse?) -> Unit
        ) {
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET, null)
                } else {
                    val address = SERVER + "tokens/pages"
                    Utils.log("Connecting to $address")
                    Log.v("FACEBOOKPAOLO", address)

                    AndroidNetworking.get(address)
                        .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build().getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                Log.v("FACEBOOKPAOLO", "onResponse")
                                Log.v("FACEBOOKPAOLO", "[getFacebookPages] " + response.toString())

                                val result = Gson().fromJson(response.toString(), FacebookRegistrationResponse::class.java)

                                listener(false, response.toString(), result.data)
                                return
                            }

                            override fun onError(anError: ANError?) {
                                Log.v("FACEBOOKPAOLO", "onError")
                                Log.v("FACEBOOKPAOLO", "[getFacebookPages] " + anError.toString())
                                listener(true, anError?.localizedMessage, null)
                                return
                            }
                        })
                }
            }
        }

        fun unlinkFacebookAccount(
            listener: (Boolean, String?) -> Unit
        ) {
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET)
                } else {

                    val address = SERVER + "tokens/accounts"
                    Utils.log("Connecting to $address")
                    Log.v("FACEBOOKPAOLO", address)

                    AndroidNetworking.delete(address)
                        .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                Log.v("FACEBOOKPAOLO", "response = " + response.toString())

                                val rispostaLaravel = Gson().fromJson(response.toString(), BaseLaravelResponse::class.java)

                                return listener(false, rispostaLaravel.message)
                            }

                            override fun onError(anError: ANError?) {
                                Log.v("FACEBOOKPAOLO", "anError = " + anError.toString())
                                val rispostaLaravel = Gson().fromJson(anError.toString(), BaseLaravelResponse::class.java)
                                return listener(true, rispostaLaravel.message)
                            }
                        })
                }
            }
        }

        // invia i dati di facebook al server
        fun sendFacebookAccessToken(
            accessToken : AccessToken,
            listener: (Boolean, String?, FacebookRegistrationResponse?) -> Unit
        ) {
            InternetCheck { connected ->

                Utils.log("Is connection enabled? " + connected)

                if (!connected) {
                    listener(true, MESSAGE_NO_INTERNET, null)
                } else {

                    val address = SERVER + "tokens/fb-access-token"
                    Utils.log("Connecting to $address")
                    Log.v("FACEBOOKPAOLO", address)

                    //val outputFormat = SimpleDateFormat("dd-MM-yyyy")
                    //var date: Date? = null
                    //date = inputFormat.parse("2020-05-03T00:00:00")
                    //val formattedDate: String = outputFormat.format(date)
                    val incomingFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ITALIAN)
                    //val date = incomingFormat.parse(expireDate)
                    val dataStringa = incomingFormat.format(accessToken.dataAccessExpirationTime)

                    Log.v("FACEBOOKPAOLO", "expire Date = " + dataStringa )

                    val body = JSONObject().apply {
                        put("access_token", accessToken.token)
                        put("fb_user_id", accessToken.userId)
                        put("token_type", "user_long")
                        put("expire_date", dataStringa)
                    }

                    Log.v("FACEBOOKPAOLO", body.toString())

                    AndroidNetworking.patch(address)
                        .addJSONObjectBody(body)
                        .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                        .setTag(TAG_NAME)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject) {
                                // do anything with response
                                try {

                                    //status = success / <0
                                    if (response.has("status")) {
                                        if (response.getString("status").contains("success")) {

                                            Log.v("FACEBOOKPAOLO", "data = " + response.toString())

                                            val result = Gson().fromJson(response.toString(), FacebookRegistrationResponse::class.java)

                                            if (result != null) {
                                                Log.v("FACEBOOKPAOLO", "messaggio = " + result.message)
                                                Log.v("FACEBOOKPAOLO", "pagine = " + result.data)
                                            }

                                            return listener(false, "ok", result)
                                        }
                                    }

                                    if (response.has("data")) {
                                        val data = response.getString("data")

                                        //Log.v("FACEBOOKPAOLO", "messaggio = " + result.message)
                                        //Log.v("FACEBOOKPAOLO", "pagine = " + result.data)
                                    }

                                    val output = if (response.has("message")) response.getString(
                                        "message"
                                    ) else "Errore sconosciuto"

                                    return listener(true, output, null)

                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    listener(true, "Errore formato ricevuto ${response.toString()}", null)
                                    return
                                }
                            }

                            override fun onError(error: ANError?) {
                                // handle error
                                try {
                                    if (error == null) {
                                        listener(
                                            true,
                                            "Ricevuta risposta vuota, riprova tra qualche minuto.",
                                             null
                                        )
                                    } else {
                                        val errBody = JSONObject(error.errorBody)

                                        Log.v("FACEBOOKPAOLO", "errBody = " + errBody.toString())

                                        if (errBody.has("status") && errBody.getString("status") == "fail") {
                                            if (errBody.has("message")) {
                                                if (errBody.get("message") is String) {
                                                    listener(
                                                        true,
                                                        errBody.getString("message"), null
                                                    )
                                                } else if (errBody.get("message") is JSONArray) {
                                                    listener(
                                                        true,
                                                        errBody.getJSONArray("message")
                                                            .toString(), null
                                                    )
                                                } else {
                                                    listener(true, "Errore sconosciuto", null)
                                                }
                                            } else {
                                                listener(true, "Errore sconosciuto", null)
                                            }
                                        } else {
                                            listener(true, errBody.toString(), null)
                                        }
                                    }
                                } catch (e: JSONException) {
                                    listener(
                                        true,
                                        "Errore: " + (error?.localizedMessage
                                            ?: "Sconosciuto"), null
                                    )
                                }
                            }
                        })
                } //connection check
            }//f
        }

        fun updateSelectedFacebookPage(pagina: FacebookPage) {

            val address = SERVER + "tokens/active-facebook-page/${pagina.item_id}/"

            AndroidNetworking.patch(address)
                //.addJSONObjectBody(body)
                .addHeaders("Authorization", "Bearer ${Utils.appToken}")
                .setTag(TAG_NAME)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        Log.v("FACEBOOKPAOLO", "risposta ok")
                        Log.v("FACEBOOKPAOLO", response.toString())

                        Utils.sharefPrefsEditor.apply {
                            remove(EditorMainFragment.FB_PAGE)
                        }

                        val json = pagina.getJSON()
                        Utils.sharefPrefsEditor.apply {
                            putJSONObject(EditorMainFragment.FB_PAGE, json)
                        }
                    }

                    override fun onError(anError: ANError?) {
                        Log.v("FACEBOOKPAOLO", "risposta errore")
                        Log.v("FACEBOOKPAOLO", anError.toString())
                    }
                })
        }

    } //companion object

}