package it.ghd.salogram.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.internal.rtl.RtlTextView
import it.ghd.salogram.ImageManager
import it.ghd.salogram.R
import it.ghd.salogram.model.UserAccount
import java.util.*
import kotlin.random.Random

class GroupAdapter(
    private val layout : Int,
    private val gruppi: List<UserAccount>,
    private val listener: (Int)  -> Unit
) : RecyclerView.Adapter<GroupAdapter.GroupHolder>()  {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GroupHolder {

        val inflatedView = parent.gonfia(layout, false)
        //val inflatedView = parent.gonfia(R.layout.md_listitem_multichoice, false)
        return GroupHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: GroupHolder, position: Int) {
        val itemPagina = gruppi[position]
        holder.bindGruppo(itemPagina, position)
    }

    override fun getItemCount() = gruppi.size

    inner class GroupHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        //2
        private var view: View = v
        private var container: LinearLayout
        private var pageName: TextView
        private var pagePicture : ImageView
        private var account: UserAccount? = null

        //3
        init {
            pageName = view.findViewById(R.id.md_title)
            pagePicture = view.findViewById(R.id.md_picture)
            container = view.findViewById(R.id.container)
        }

        fun bindGruppo(account: UserAccount, position: Int) {
            this.account = account
            pageName.text = account.goupName
            val gruppoLogo = "https://salogram.com/web/logos/${account.groupSlug}.png?time=" + Random.nextInt(0, Int.MAX_VALUE)
            ImageManager.loadImage(gruppoLogo, pagePicture)
            container.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            Log.v("PAOLO", "onClick $adapterPosition")
            listener(adapterPosition)
        }
    }
}

private fun ViewGroup.gonfia(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}
