package it.ghd.salogram.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.internal.rtl.RtlTextView
import it.ghd.salogram.ImageManager
import it.ghd.salogram.ImageViewTopCrop
import it.ghd.salogram.R
import it.ghd.salogram.model.FacebookPage
import kotlinx.android.synthetic.main.activity_profilo.*


class MyRecyclerAdapter(
    private val pagine: List<FacebookPage>,
) : RecyclerView.Adapter<MyRecyclerAdapter.FacebookPageHolder>()  {

   //var selectedPosition = -1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FacebookPageHolder {

        val inflatedView = parent.gonfia(R.layout.list_item_facebook_page, false)
        //val inflatedView = parent.gonfia(R.layout.md_listitem_multichoice, false)
        return FacebookPageHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: FacebookPageHolder, position: Int) {
        val itemPagina = pagine[position]
        holder.bindPagina(itemPagina, position)
    }

    override fun getItemCount() = pagine.size

    inner class FacebookPageHolder(v: View) : RecyclerView.ViewHolder(v) {
        //2
        private var view: View = v
        private var pageName: RtlTextView
        private var pagePicture : ImageView
        private var pageCheck: AppCompatRadioButton //AppCompatCheckBox
        private var pagina: FacebookPage? = null

        //3
        init {
            pageName = view.findViewById(R.id.md_title)
            pageCheck = view.findViewById(R.id.md_control)
            pagePicture = view.findViewById(R.id. md_picture)
            pageCheck.isEnabled = true
            pageCheck.isFocusable = true
            pageCheck.isClickable = true
        }

        fun bindPagina(pagina: FacebookPage, position: Int) {
            this.pagina = pagina
            //view.ite.text = pagina.item_id
            pageName.text = pagina.item_name
            ImageManager.loadImage(pagina.item_pic, pagePicture)
            //pageCheck.isChecked = selectedPosition == position
            //pageCheck.isChecked = pagina.selected
            //pageCheck.isChecked = currentSelection == position
            Log.v("FACEBOOKPAOLO", "bind pagina ${pagina.item_name} = " + pagina.active)

            pageCheck.setOnCheckedChangeListener(null)
            pageName.setOnClickListener(null)
            //pageCheck.isChecked = selectedPosition == position
            pageCheck.isChecked = pagina.active
            pageName.setOnClickListener{ pageCheck.toggle() }
            pageCheck.setOnCheckedChangeListener { _, isChecked ->
                Log.v("FACEBOOKPAOLO", " - ")
                Log.v("FACEBOOKPAOLO", "cliccato su $adapterPosition con valore $isChecked")

                pagine.forEach{ it.active = false }

                //selectedPosition = adapterPosition

                view.post { //notifyItemChanged(adapterPosition)
                    Log.v("FACEBOOKPAOLO", "ho selezionato la pagina" + pagina.item_name)
                    pagina.active = isChecked
                    notifyDataSetChanged()
                }
            }
        }
    }
}

private fun ViewGroup.gonfia(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}
