package it.ghd.salogram.adapters

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.ImageManager
import it.ghd.salogram.Manager
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import it.ghd.salogram.activities.AcademyActivity
import it.ghd.salogram.activities.MainActivity
import kotlinx.android.synthetic.main.video_grid_cell.view.*
import org.json.JSONArray
import org.json.JSONObject

class VideoRecyclerAdapter(
    val items: JSONArray,
    val context: Context
) : RecyclerView.Adapter<VideoRecyclerAdapter.VideoHolder>() {

    companion object{
        var cellHeight:Int = -1
    }

    override fun getItemCount(): Int {
        return items.length()
    }

    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        holder.bindEvent(items.getJSONObject(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {

        val view = LayoutInflater.from(context).inflate(
            R.layout.video_grid_cell,
            parent,
            false
        )
        view.post {
            if (cellHeight <0) {
                cellHeight = parent.width / 2
            }

            view.layoutParams.height = cellHeight
            view.requestLayout()

        }

        return VideoHolder((view))
    }

    inner class VideoHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event

            ImageManager.loadImage(event.optString("thumbnail"), view.imageViewCell)
            view.titleTVCell.text = event.optString("title")
            //view.fabButtonCell.setOnClickListener(this)
            view.fabButtonCell.visibility = View.GONE
            view.playButton.setOnClickListener {
                onClick(it)
            }
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            (itemView.context as AcademyActivity).playVideo(content!!)

            val properties = Properties().apply {
                putValue("type", TagNames.ITEM_TYPE_TUTORIAL )
            }
            MainActivity.trackEvent(TagNames.PLAY_VIDEO, properties)
        }
    }
}