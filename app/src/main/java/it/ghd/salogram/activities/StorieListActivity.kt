package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.segment.analytics.Properties
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_stories_list.*
import kotlinx.android.synthetic.main.activity_stories_list.swipeToRefresh
import kotlinx.android.synthetic.main.activity_stories_list.videoContainer
import kotlinx.android.synthetic.main.activity_stories_list.videoView
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.post_filter_cell.view.*
import kotlinx.android.synthetic.main.post_grid_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class StorieListActivity : MainActivity() {

    var stories:JSONArray = JSONArray()
    var favs:JSONArray = JSONArray()
    var cats:JSONArray = JSONArray()
    var selectedCat:JSONObject? = null
    var selectedCatIndex:Int = 0

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_stories_list)

        screeName = TagNames.Screen.STORIES_LIST_SCREEN
        topTitleTV.text = ""

        menuStoriesButton.isSelected = true

        val gridLayoutManager = GridLayoutManager(this,2)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL

        storiesRV.layoutManager = gridLayoutManager
        //storiesRV.setHasFixedSize(true)

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = GridLayoutManager.HORIZONTAL
        storiesFilterRV.layoutManager = linearLayoutManager

        // cats = intent.getJSONArrayExtra("CATS")

        cats = Manager.storyHomeContents

        var counter = 0

        val cat = intent.getJSONObjectExtra("CAT")

        val catID = cat.getInt("category_id")

        for (jo:JSONObject in cats){
            if (jo.getInt("category_id") == catID) {
                selectedCatIndex = counter
                break
            }
            counter++
        }

        selectCategory(cat,selectedCatIndex)

        swipeToRefresh.setOnRefreshListener {
            loadData()
            trackEvent(TagNames.STORIES_LIST_REFRESH, null)
        }
    }

    override fun onResume() {
        super.onResume()

        if (firstStart){
            loadData()
            firstStart = false
        }

        Manager.storyHomeContents = JSONArray()
    }

    fun isFaved(favID:Int): Boolean {
        for (fav:JSONObject in favs){
            if (fav.getInt("id") == favID){
                return true
            }
        }
        return false
    }

    fun selectCategory(cat:JSONObject, index:Int){

        if (index==selectedCatIndex){
            return
        }

        selectedCat =  cat

        //val categoriesAdapter = CategoriesRecyclerAdapter(cats,this)
        //categoriesAdapter.setHasStableIds(true)
        storiesFilterRV.adapter!!.notifyItemChanged(index)
        storiesFilterRV.adapter!!.notifyItemChanged(selectedCatIndex)

        selectedCatIndex = index

        stories = selectedCat!!.getJSONArray("stories")
        val adapterContents = StoriesRecyclerAdapter(stories, this)
        //adapterContents.setHasStableIds(true)
        storiesRV.adapter = adapterContents
        val catName = cat.getString("name")
        topTitleTV.text =  catName

        val properties = Properties()
        properties["name"] = catName
        trackEvent(TagNames.TAP_ON_CATEGORY_FROM_STORIES_LIST, properties)
    }

    fun stopVideo(view: View) {
        videoView.stopPlayback()
        videoView.setVideoURI(null)
        videoContainer.visibility = View.GONE
        loadingOverlay?.visibility = View.GONE

        //TODO: properties
        trackEvent(TagNames.STOP_VIDEO, null)
    }

    fun playVideo(video:JSONObject) {

        videoContainer.visibility = View.VISIBLE
        /*videoView.setOnPreparedListener {
            videoContainer.visibility = View.VISIBLE
            loadingOverlay?.visibility = View.GONE
        }*/

        videoView.setOnCompletionListener {
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            loadingOverlay?.visibility = View.GONE
        }

        videoView.setOnErrorListener { mp, what, extra ->
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            loadingOverlay?.visibility = View.GONE

            true
        }

        videoView.setVideoURI(Uri.parse(video.getString("url")))
        videoView.start()
        //TODO: properties
        trackEvent(TagNames.PLAY_VIDEO, null)

        videoView.setOnPreparedListener {
            it.setOnBufferingUpdateListener { mp, percent ->

                if (percent>=100){
                    videoPreviewProgressBar2.visibility = View.GONE
                }
                else {
                    videoPreviewProgressBar2.visibility = View.VISIBLE
                }
            }
        }

        videoView.requestFocus()

        window.setFormat(PixelFormat.OPAQUE)
    }

    private fun loadData() {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getAllFavorites() { error, message, _, favsStoriesJsonArrayStories ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        swipeToRefresh.isRefreshing = false
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                favs = favsStoriesJsonArrayStories!!

                selectedCat = cats.getJSONObject(selectedCatIndex)

                topTitleTV.text = selectedCat!!.getString("name")

                val hasParent = selectedCat!!.getInt("parent") != 0

                val siblingCats = JSONArray()

                if (hasParent) {
                    val categoryID = selectedCat!!.getInt("parent")

                    for (jo: JSONObject in cats) {
                        if (jo.getInt("parent") == categoryID) {
                            siblingCats.put(jo)
                        }
                    }
                }

                val categoriesAdapter = CategoriesRecyclerAdapter(
                    if (hasParent) siblingCats else cats,
                    this
                )
                //categoriesAdapter.setHasStableIds(true)
                storiesFilterRV.adapter = categoriesAdapter

                //posts

                stories = selectedCat!!.getJSONArray("stories")

                val adapterContents = StoriesRecyclerAdapter(stories, this)
                //adapterContents.setHasStableIds(true)
                storiesRV.adapter = adapterContents

                loadingOverlay?.visibility = View.GONE

                swipeToRefresh.isRefreshing = false
            }
        }
    }
    fun removeFav(fav: JSONObject, index:Int){
        loadingOverlay?.visibility = View.VISIBLE

        Manager.removeFav(fav) { error, message ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                var foundIndex = -1
                var found = false

                for (jo: JSONObject in favs) {
                    foundIndex++
                    if (jo.getInt("id") == fav.getInt("id")) {
                        found = true
                        break
                    }

                }

                if (foundIndex >= 0 && found) {
                    favs.remove(foundIndex)
                }

                loadingOverlay?.visibility = View.GONE

                storiesRV.adapter!!.notifyItemChanged(index)

                //loadData()
            }
        }
    }

    fun addToFav(fav: JSONObject, index:Int){
        loadingOverlay?.visibility = View.VISIBLE

        Manager.addtoFav(fav) { error, message ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                favs.put(fav)

                loadingOverlay?.visibility = View.GONE

                storiesRV.adapter!!.notifyItemChanged(index)

                //loadData()
            }
        }
    }

    //DATA

    private class StoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, context: Context, index:Int) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
            view.titleTVCell.text = event.optString("name")

            view.fabButtonCell?.isSelected = (context as StorieListActivity).isFaved(event.getInt("id") ?: 0)
            view.fabButtonCell?.setOnClickListener {
                if (view.fabButtonCell?.isSelected == true) {
                    (context).removeFav(content!!, index)
                    val properties = Properties().apply {
                        putValue("type", TagNames.ITEM_TYPE_STORIES )
                    }
                    trackEvent(TagNames.REMOVE_FROM_FAVOURITE, properties)
                }
                else {
                    //add
                    (context ).addToFav(content!!, index)
                    val properties = Properties().apply {
                        putValue("type", TagNames.ITEM_TYPE_STORIES )
                    }
                    trackEvent(TagNames.ADD_TO_FAVOURITE, properties)
                }
            }

            view.playButton?.visibility =  if (content!!.getString("url").contains("mp4")) View.VISIBLE else View.GONE
            view.playButton?.setOnClickListener {
                (itemView.context as StorieListActivity).playVideo(content!!)
                val properties = Properties().apply {
                    putValue("type", TagNames.ITEM_TYPE_STORIES )
                }
                trackEvent(TagNames.PLAY_VIDEO, properties)
            }
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("DATA", content!!)
            intent.putExtra("POST",false)
            context.startActivity(intent)

            val properties = Properties().apply {
                putValue("type", TagNames.ITEM_TYPE_STORIES )
            }
            trackEvent(TagNames.TAP_ON_STORIES_FROM_LIST, properties)
        }
    }

    private class StoriesRecyclerAdapter(
        val items: JSONArray,
        val context: Context
    ) : RecyclerView.Adapter<StoriesHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: StoriesHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position), context,position)
        }

        override fun getItemViewType(position: Int): Int {

            val jo = items.getJSONObject(position)

            if (jo.getString("url").contains("mp4")){
                return 1
            }

            return  0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoriesHolder {
            return StoriesHolder(
                LayoutInflater.from(context).inflate( if (viewType==0) R.layout.stories_grid_cell else R.layout.stories_video_grid_cell, parent, false)
            )
        }
    }

    //CATEGORIES
    private class CategoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null
        private var index:Int = 0

        init {
            v.filterButton.setOnClickListener(this)
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, context: Context, index:Int) {
            this.content = event
            this.index = index
            view.filterButton.text = event.optString("name")
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            (itemView.context as StorieListActivity).selectCategory(content!!,index)
        }
    }

    private class CategoriesRecyclerAdapter(
        val items: JSONArray,
        val context: Context
    ) : RecyclerView.Adapter<CategoriesHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {

            holder.bindEvent(items.getJSONObject(position), context, position)
        }

        override fun getItemViewType(position: Int): Int {
            return if ((context as StorieListActivity).selectedCat!=null && context.selectedCat  == items.getJSONObject(position)) 1 else 0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
            return CategoriesHolder(
                LayoutInflater.from(context).inflate( if (viewType==1) R.layout.post_filter_cell else R.layout.post_filter_cell_unselected, parent, false)
            )
        }
    }
}