package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.segment.analytics.Properties
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_preferiti_list.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.post_grid_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class PreferitiActivity : MainActivity() {

    var stories = JSONArray()
    var posts = JSONArray()

    var viewPosts = true

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_preferiti_list)
        screeName = TagNames.Screen.PREFERITI_SCREEN

        topTitleTV.text = "PREFERITI"

        menuPreferitiButton.isSelected = true

        val linearLayoutManager = GridLayoutManager(this,2)
        linearLayoutManager.orientation = GridLayoutManager.VERTICAL

        favoritesRV.layoutManager = linearLayoutManager
        favoritesRV.setHasFixedSize(true)

        swipeToRefresh.setOnRefreshListener {
            loadData()
            trackEvent(TagNames.FAVOURITES_POST_LIST_REFRESH, null)
        }
    }

    override fun onResume() {
        super.onResume()

        if (firstStart){
            loadData()
            firstStart = false
        }
    }
    private fun loadData() {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getAllFavorites() { error, message, jsonArrayPosts, jsonArrayStories ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                        swipeToRefresh.isRefreshing = false
                    }
                }

            } else {

                loadingOverlay?.visibility = View.GONE
                swipeToRefresh.isRefreshing = false

                posts = jsonArrayPosts!!
                stories = jsonArrayStories!!

                if (viewPosts) showPosts(null)
                else showStories(null)
            }
        }
    }

    fun showStories(view:View?){
        viewPosts = false

        val adapterCategories = FavoritesRecyclerAdapter(if (viewPosts) posts else stories, this, viewPosts)
        favoritesRV.adapter = adapterCategories
        storiesButton.isSelected = true
        postsButton.isSelected = false
        checkButtons()
    }

    private fun checkButtons() {
        if (storiesButton.isSelected) {
            storiesButton.background = getDrawable(R.drawable.round_button_cyan)
            storiesButton.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
        else {
            storiesButton.background = getDrawable(R.drawable.round_button_cyan_border)
            storiesButton.setTextColor(ContextCompat.getColor(this, R.color.salogram_cyan))
        }

        if (postsButton.isSelected) {
            postsButton.background = getDrawable(R.drawable.round_button_cyan)
            postsButton.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
        else {
            postsButton.background = getDrawable(R.drawable.round_button_cyan_border)
            postsButton.setTextColor(ContextCompat.getColor(this, R.color.salogram_cyan))
        }
    }

    fun showPosts(view:View?){
        viewPosts = true
        val adapterCategories = FavoritesRecyclerAdapter(if (viewPosts) posts else stories, this, viewPosts)
        favoritesRV.adapter = adapterCategories
        storiesButton.isSelected = false
        postsButton.isSelected = true
        checkButtons()
    }

    fun removeFav(fav:JSONObject, index:Int){
        loadingOverlay?.visibility = View.VISIBLE

        Manager.removeFav(fav) { error, message ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                var index = -1
                var found = false

                //for (jo: JSONObject in (if (viewPosts) posts else stories)) {
                var elements = if (viewPosts) posts else stories

                (0 until elements.length()).forEach {
                    val jo = elements.getJSONObject(it)
                    index++
                    if (jo.getInt("id") == fav.getInt("id")) {
                        found = true
                        return@forEach //break
                    }

                }

                if (index >= 0 && found) {
                    (if (viewPosts) posts else stories).remove(index)
                }

                loadingOverlay?.visibility = View.GONE
                favoritesRV.adapter!!.notifyItemRemoved(index)
                // loadData()
            }
        }
    }

    //DATA

    private class FavoritesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, context: Context, index:Int) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
            view.titleTVCell.text = event.optString("name")

            view.fabButtonCell?.isSelected = true
            view.fabButtonCell?.setOnClickListener {
                //remove
                Manager.removeFav((event)) { error, message ->
                    //reload data
                    (context as PreferitiActivity).removeFav(content!!, index)
                    trackEvent(TagNames.REMOVE_FROM_FAVOURITE, null)
                }
            }
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("DATA", content!!)
            context.startActivity(intent)
            trackEvent(TagNames.TAP_ON_POST_FROM_FAVOURITES, null)

            val isVideo = content!!.getString("url").lowercase(Locale.getDefault()).contains("mp4")
            var type = content!!.get("type")
            if (type == "story") {type = "stories"}
            val media = if (isVideo) "video" else "image"
            val postID = content!!.get("id")

            val properties = Properties()
            properties["type"] = type
            properties["media"] = media
            properties["id"] = postID

            Log.v("SCHERMATA", "media = " + media)
            Log.v("SCHERMATA", "type = " + type)
            Log.v("SCHERMATA", "id = " +  postID)

            if (type == "post") {
                trackEvent(TagNames.TAP_ON_POST, properties)
            }
            else {
                trackEvent(TagNames.TAP_ON_STORIES, properties)
            }
            trackEvent(TagNames.TAP_ON_ITEM, null)
        }
    }

    private class FavoritesRecyclerAdapter(
        val items: JSONArray,
        val context: Context,
        val isPosts: Boolean
    ) : RecyclerView.Adapter<FavoritesHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun getItemViewType(position: Int): Int {
            return if (isPosts) {
                val jo = items.getJSONObject(position)

                if (jo.getString("url").contains("mp4")){
                    return 1
                }

                return  0

            } else {
                //stories
                val jo = items.getJSONObject(position)

                if (jo.getString("url").contains("mp4")){
                    return 3
                }

                return  2
            }
        }

        override fun onBindViewHolder(holder: FavoritesHolder, position: Int) {

            holder.bindEvent(items.getJSONObject(position), context, position)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritesHolder {
            return FavoritesHolder(

                when(viewType){
                    0-> LayoutInflater.from(context).inflate( R.layout.post_grid_cell , parent, false)
                    1-> LayoutInflater.from(context).inflate( R.layout.video_grid_cell , parent, false)
                    2-> LayoutInflater.from(context).inflate( R.layout.stories_grid_cell , parent, false)
                    3-> LayoutInflater.from(context).inflate( R.layout.stories_video_grid_cell , parent, false)
                    else ->   LayoutInflater.from(context).inflate( R.layout.post_grid_cell , parent, false)
                }
            )
        }
    }
}