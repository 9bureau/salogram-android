package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.ModalDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.checkbox.checkBoxPrompt
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.facebook.login.LoginManager
import com.onesignal.OneSignal
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import com.segment.analytics.Traits
import it.ghd.salogram.*
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.fragments.MenuFragment
import it.ghd.salogram.model.Owner
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.Exception
import java.util.*

abstract class MainActivity: AppCompatActivity() {

    var menuFragment: MenuFragment? = null
    var firstStart = true
    //protected val tracker = Tracker(this)
    protected var screeName : String? = null
    protected var properties = Properties()
    protected var owner : Owner? = null

    /*
    //TODO: bisogna controllare se c'è solo una 'activity nello stack e seè quella di login
    override fun onBackPressed() {
       askQuestion(getString(R.string.warning), "Vuoi effettuare il logout?", "Si", "No") {
           confirmed -> if (confirmed) { super.onBackPressed() }
       }
    }
    */

    protected abstract fun hasLogo() : Boolean
    protected abstract fun showGroupLogo()
    protected abstract fun onCreateAfterParent()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)
        Log.v("LOGO", "onCreate")
        if (Utils.sharefPrefs.contains("owner")) {
            val ownerJson = Utils.sharefPrefs.getJSONObject("owner") ?: JSONObject()
            owner = Owner.parse(ownerJson)
            Log.v("LOGO", "owner trovato")
        }

        onCreateAfterParent()

        if (owner != null)  {
            if ( hasLogo() ) {
                showGroupLogo()
            }
        }
    }

    companion object {
        fun trackEvent(event:String, properties: Properties?){
            if (Utils.sharefPrefs.contains("owner")) {
                Log.v("SCHERMATA", "owner trovato")
                try {

                    val ownerJson = Utils.sharefPrefs.getJSONObject("owner")
                    Log.v("SCHERMATA", "ownerJson = " + ownerJson.toString())
                    val owner = Owner.parse(ownerJson)
                    if (owner != null) {
                        properties?.set("owner", owner.slug)
                    }
                }
                catch (ex : JSONException) {
                    Log.v("SCHERMATA", "owner exception")
                    val proprieta = Properties()
                    proprieta["classe"] = "MainActivity"
                    proprieta["funzione"] = "trackEvent (companion)"
                    proprieta["eccezione"] = ex.localizedMessage
                    proprieta["origine"] = "try/catch"
                    Analytics.with(CustomApplication.context).track("Errore", proprieta)
                }
            } else {
                Log.v("SCHERMATA", "owner non trovato")
            }
            Analytics.with(CustomApplication.context).track(event, properties)
            Log.v("SCHERMATA", "evento: " + event)
        }
    }

    protected fun trackEvent(event:String, properties: Properties?){
        if (owner != null) properties?.set("owner", owner!!.slug)
        Analytics.with(this).track(event, properties)
        Log.v("SCHERMATA", "evento: " + event)
    }

    fun startAppLogin(
        userID: String?,
        username: String,
        firstStart: Boolean,
        autologin: Boolean,
        mostraNotifica: Boolean
    ) {
        // LOGIN SUCCESS

        Log.v("NOTIFICHE", "startAppLogin")

        Analytics.with(CustomApplication.context).identify(userID!!, Traits().putEmail(username), null)
        if (owner != null) {
            Analytics.with(CustomApplication.context).group(owner!!.name)
            Log.v("SCHERMATA", "group = " + owner!!.name)
        }

        var intent: Intent
        var notificationPostIntent: Intent? = null

        val userProperties = Properties()
        userProperties["username"] = username

        //ok
        if (firstStart) {
            Log.v("NOTIFICHE", "è un primo accesso")
            intent = Intent(this, RegistrazioneActivity::class.java).apply {
                putExtra("firstStart", true)
            }

            trackEvent(TagNames.USER_FIRST_ACCESS, userProperties)

        } else {

            trackEvent(TagNames.USER_NOT_FIRST_ACCESS, userProperties)

            Log.v("NOTIFICHE", "non è un primo accesso")

            if (Utils.sharefPrefs.contains("notification_post")) {
                if (mostraNotifica) {
                    Log.v("NOTIFICHE", "trovata notifica in attesa da mostrare")
                    val data = Utils.sharefPrefs.getJSONObject("notification_post")
                    notificationPostIntent = Intent(this, EditorActivity::class.java)
                    notificationPostIntent.flags =
                        Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
                    notificationPostIntent.putExtra("DATA", data)
                    notificationPostIntent.putExtra("POST", true)
                    Utils.sharefPrefsEditor.remove("notification_post")
                    trackEvent(TagNames.FOUND_NOTIFICATION_ON_PREFERENCES, userProperties)
                } else {
                    Log.v("NOTIFICHE", "trovata notifica in attesa da non mostrare")
                    trackEvent(TagNames.FOUND_WRONG_NOTIFICATION_ON_PREFERENCES, userProperties)
                    showAdvice("Spiacente, questa notifica non era destinata a te")
                }
            } else {
                Log.v("NOTIFICHE", "nessuna notifica in attesa")
                trackEvent(TagNames.NOT_FOUND_NOTIFICATION_ON_PREFERENCES, userProperties)
            }

            intent = Intent(this, PostHomeActivity::class.java).apply {}
        }

        val properties = Properties()
        properties["first_access"] = firstStart
        properties["autologin"] = autologin
        trackEvent(TagNames.USER_LOGIN, properties)

        Log.v("NOTIFICHE", "carico la home dei post")
        startActivity(intent)
        if (notificationPostIntent != null) {
            Log.v("NOTIFICHE", "carico la pagina del post trovato nella notifica")
            startActivity(notificationPostIntent)
        }

        //TODO: qui aggiungere popup per numero tel?

    }

    fun doGroupLogin(newmail: String, password : String, showNotifica : Boolean) {

        val autologin = Utils.sharefPrefs.getBoolean("autologin", false)
        Log.v("NOTIFICHE", "faccio login di gruppo con autologin " + autologin)

        Manager.login(newmail, password, autologin) {
                error, message, firstStart, userID, owners, askPhoneNumberConfirm ->

            if (error) {
                Log.v("PAOLO", "[doGroupLogin] errore: $message")
            } else {
                Log.v("PAOLO", "[doGroupLogin] success: $message, user id = $userID")

                if (askPhoneNumberConfirm) {
                    Log.v("PAOLO1", "chiedo conferma del numero di telefono 1")
                    askPhoneNumberConfirmGroupUser(userID, newmail, firstStart, autologin, showNotifica)
                }
                else {
                    Log.v("PAOLO1", "non chiedo conferma del numero di telefono 1")
                    startAppLogin(userID, newmail, firstStart, autologin, showNotifica)
                }
            }
        }
    }

    fun askPhoneNumberConfirmGroupUser(userID : String?, username : String, primoAccesso:Boolean, autologin:Boolean, showNotifica: Boolean) {

        trackEvent(TagNames.PHONE_NUMBER_CONFIRM_REQUEST, null)
        var currentPhone = Utils.sharefPrefs.getString("phone", "")

        if (currentPhone == "null") currentPhone = ""

        val prefix = "+39"

        if (currentPhone != null) {
            if (!currentPhone.startsWith(prefix)) {
                currentPhone = prefix + currentPhone
            }
        } else currentPhone = prefix

        Log.v("PAOLO1", "current_phone 1 = " + currentPhone)

        var consensoDato = false
        val dialogBehavior = ModalDialog
        val dialog = MaterialDialog(this, dialogBehavior).show {
            title(0, "Conferma o aggiungi il tuo numero di telefono")
            icon(R.mipmap.ic_launcher)
            customView(R.layout.phone_number_request_view, scrollable = true, horizontalPadding = true)
            noAutoDismiss()
            positiveButton(0, "Conferma") { dialog ->
                val phoneInput: EditText = dialog.getCustomView().findViewById(R.id.phone_number)
                val phone = phoneInput.text.toString().trim().lowercase(Locale.getDefault())
                Log.v("PAOLO1", "cliccato su OK, phone = $phone")

                if (phone.length < 10) {
                    Log.v("PAOLO1", "non proseguo 1")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }

                /*
                if (phone == "+39") {
                    Log.v("PAOLO1", "non proseguo 2")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }*/

                if (!phone.isValidPhone()) {
                    Log.v("PAOLO1", "non proseguo 3")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }

                if (!phone.startsWith("+39")) {
                    Log.v("PAOLO1", "non proseguo 4 ($phone)")
                    showAdvice("Il numero deve iniziare con il prefisso italiano +39")
                    return@positiveButton
                }

                if (!consensoDato) {
                    Log.v("PAOLO1", "non proseguo 5")
                    showAdvice("Devi dare il consenso per proseguire")
                    return@positiveButton

                } else {
                    Log.v("PAOLO1", "proseguo")
                    dismiss()
                    savePhoneNumberGroupUser(phone, userID, username, primoAccesso, autologin, showNotifica)
                }
            }
            negativeButton(android.R.string.cancel)
            //lifecycleOwner(this@LoginActivity)
            //debugMode(debugMode)
        }

        val customView = dialog.getCustomView()
        val privacyPhone : Button = customView.findViewById(R.id.privacy_phone)
        privacyPhone.setOnClickListener {
            Log.v("PAOLO1", "cliccato")
            val intent = Intent(this, PrivacyActivity::class.java).apply {
                setFlags(intent)
                putExtra("url","https://www.iubenda.com/privacy-policy/75745741")
            }
            startActivity(intent)
        }
        val phoneNumberInput: EditText = customView.findViewById(R.id.phone_number)
        phoneNumberInput.setText(currentPhone)
        val phoneNumberConsent: CheckBox = customView.findViewById(R.id.consenso)
        phoneNumberConsent.setOnCheckedChangeListener { _, isChecked ->
            consensoDato = isChecked
        }


        /*
        MaterialDialog(this).show {
            input(waitForPositiveButton = true, prefill = currentPhone, inputType = InputType.TYPE_CLASS_PHONE, maxLength = 15) { dialog, text ->
                val inputField = dialog.getInputField()
                val isValid = text.isNumeric()

                inputField.error = if (isValid) null else "Inserisci solo numeri, senza spazi e divisori"
                dialog.setActionButtonEnabled(WhichButton.POSITIVE, isValid)

                val phone = text.toString().trim().lowercase(Locale.getDefault())
                Log.v("PAOLO1", "phone = $phone")

                savePhoneNumberGroupUser(phone, userID, username, primoAccesso, autologin, showNotifica)
            }
            title(0, "Conferma o aggiungi il tuo numero di telefono")
            icon(R.mipmap.ic_launcher)
            checkBoxPrompt(R.string.accetto) { checked ->
                Log.v("PAOLO1", "checcato")
            }
            message(R.string.phone_number_advice_privacy)
            positiveButton(R.string.confirm)
        }
        */
    }

    fun savePhoneNumberGroupUser(phone: String, userID : String?, username : String, primoAccesso:Boolean, autologin:Boolean, showNotifica: Boolean) {

        val timestamp = System.currentTimeMillis() / 1000

        Log.v("PAOLO1", "timestamp = $timestamp")

        val user:JSONObject = JSONObject().apply {
            put("tel_confirm_date", timestamp)
            put("tel", phone)
        }

        OneSignal.setSMSNumber(phone)

        Manager.updateProfile(user) { error, message ->

            loadingOverlay?.visibility = View.GONE

            if (error && message != null) {
                Log.v("PAOLO1", "numero di telefono NON salvato: $message")
                showAlert(getString(R.string.errore), message) {

                }
            } else {
                //ok
                Log.v("PAOLO1", "numero di telefono salvato")
                trackEvent(TagNames.PHONE_NUMBER_CONFIRMED, null)
                startAppLogin(userID, username, primoAccesso, autologin, showNotifica)
            }
        }
    }

    override fun onPause() {
        super.onPause()

        overridePendingTransition(0, 0)

        if (menuFragment!=null) {
            supportFragmentManager.beginTransaction().remove(menuFragment!!).commit()
        }
    }

    override fun onResume() {
        super.onResume()

        Log.v("NOTIFICHE", "riesumato " + screeName)

        if (screeName != null) {
            Analytics.with(this).screen(screeName, properties)
            Log.v("SCHERMATA", screeName!!)
        }

        Manager.getAllNotifications { error, message, jsonArray ->
            updateNotificationBadge()
        }
    }

    protected fun setFlags(intent: Intent) {

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    /*
    fun gotoHome(view:View?){
        val intent = Intent(this, PostHomeActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
    }
    */

    fun gotoPost(view:View?){
        val intent = Intent(this, PostHomeActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_POST, null)
    }

    fun gotoNotifiche(view:View?){
        val intent = Intent(this, NotificheActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TAP_ON_NOTIFICATION_ICON, null)
    }

    fun gotoStories(view:View?){
        val intent = Intent(this, StorieHomeActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_STORIES, null)
    }

    fun gotoVideo(view:View?){
        val intent = Intent(this, AcademyActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_ACADEMY, null)
    }

    fun gotoPreferiti(view:View?){
        val intent = Intent(this, PreferitiActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_FAVOURITES, null)
    }

    fun gotoEditProfilo(view:View?){
        val intent = Intent(this, ProfileEditActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_PROFILE_EDIT, null)
    }

    fun gotoProfile(view:View?){
        val intent = Intent(this, ProfileActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TABBAR_TAP_PROFILE, null)
    }

    fun gotoForgotPassword(view:View?){
        val intent = Intent(this, ForgotPasswordActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.TAP_ON_FORGOT_PASSWORD_ICON, null)
    }

    fun gotoRegister(view:View?){
        val intent = Intent(this, RegistrazioneActivity::class.java).apply {
            setFlags(intent)
        }
        startActivity(intent)
        trackEvent(TagNames.REDIRECT_TO_REGISTRATION, null)
    }

    open fun unlinkFacebook() {
        Log.v("FACEBOOKPAOLO", "unlinkFacebook")

        LoginManager.getInstance().logOut()

        Manager.unlinkFacebookAccount {
                error, message ->

            if (error) {
                Log.v("FACEBOOKPAOLO", "[unlinkFacebook] errore: " + message)
                //showAdvice(message.toString())
            }
            else {
                Log.v("FACEBOOKPAOLO", "[unlinkFacebook] no errore: " + message)
                //showAdvice(message.toString())

                if (Utils.sharefPrefs.contains(EditorMainFragment.FB_PAGE)) {
                    Log.v("FACEBOOKPAOLO", "[unlinkFacebook] cancello dati da preferiti")
                    Utils.sharefPrefs.edit().remove(EditorMainFragment.FB_PAGE).apply()
                }
            }
        }
    }

    fun logout(view:View?, silent:Boolean = false){

        Log.v("LOGOUT", "entro in funzione")
        Manager.logged = false

        if (Utils.sharefPrefs.contains(Manager.SHARED_PREFERENCES_USER_FIELD)) {
            Utils.sharefPrefsEditor.remove(Manager.SHARED_PREFERENCES_USER_FIELD).apply()
        }

        if (!Utils.sharefPrefs.contains(EditorMainFragment.USER_ACCOUNTS)) {
            Utils.sharefPrefsEditor.remove(EditorMainFragment.USER_ACCOUNTS).apply()
        }

        if (silent){
            Log.v("LOGOUT", "logout silenzioso")

            Manager.logout() { error, message ->

                Log.v("LOGOUT", "logout silenzioso fatto")

                trackEvent(TagNames.USER_LOGOUT, null)
                Analytics.with(CustomApplication.context).reset()

                LoginManager.getInstance().logOut()
                //unlinkFacebook()
                Log.v("NOTIFICHE", "MainActivity logout")

                val intent = Intent(this, LoginActivity::class.java).apply {
                    setFlags(intent)
                }
                startActivity(intent)
                finish()
            }
        }
        else {
            Log.v("LOGOUT", "logout NON silenzioso")
            askQuestion(
                getString(R.string.warning),
                "Sei sicuro di voler effettuare il logout?",
                getString(R.string.ok),
                getString(R.string.annulla)
            ) { confirmed ->
                if (confirmed) {

                    Log.v("LOGOUT", "logout confermato")

                    Manager.logout() { error, message ->

                        Log.v("LOGOUT", "logout non silenzioso fatto")
                        Log.v("LOGOUT", "error = " + error)
                        Log.v("LOGOUT", "message = " + message)

                        trackEvent(TagNames.USER_LOGOUT, null)
                        Analytics.with(CustomApplication.context).reset()

                        LoginManager.getInstance().logOut()
                        //unlinkFacebook()
                        Log.v("NOTIFICHE", "MainActivity logout 1")

                        val intent = Intent(this, LoginActivity::class.java).apply {
                            setFlags(intent)
                        }
                        startActivity(intent)
                        finish()
                    }
                } else {
                    Log.v("LOGOUT", "logout non confermato")
                }
            }
        }
    }

    fun backAction(view:View?){
        finish()
    }

    protected fun showAdvice(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        Log.d("TOAST", "[TOAST] " + msg)
    }

    //region ALERTS/MESSAGES/QUESTIONS
    fun showAlert(title:String?, message:String?, onSelection:() -> Unit){
        if(!this.isFinishing) {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle(title ?: Utils.APP_NAME)
            builder.setCancelable(false)
            builder.setMessage(message ?: "")
            //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                onSelection()
            }
            builder.show()
        }
    }

    fun askQuestion(title:String?, message:String?, okButton:String?, cancelButton:String?, onSelection: (Boolean) -> Unit){

        if(!this.isFinishing) {
            try {
                val builder = AlertDialog.Builder(this@MainActivity)
                builder.setTitle(title ?: Utils.APP_NAME)
                builder.setCancelable(false)
                builder.setMessage(message ?: "")
                //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

                builder.setPositiveButton(okButton ?: "ok") { dialog, which ->
                    onSelection(true)
                }

                builder.setNegativeButton(cancelButton ?: "annulla") { dialog, which ->
                    onSelection(false)
                }

                builder.show()
            } catch (ex: Exception) {
                //silent ignore
            }
        }
    }
    //endregion

    fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

        if (imm != null && imm.isAcceptingText && currentFocus != null && currentFocus!!.windowToken != null) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    fun showKeyboard(target:View) {
        target.requestFocus()
        val view = currentFocus
        val methodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view!=null) methodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun updateNotificationBadge(){
        val notificationsButtonCount: Button? = findViewById(R.id.top_notification_button_counter)

        if (notificationsButtonCount!=null){
            val count = Manager.currentNotificationBadge
            notificationsButtonCount.text = "$count"
            notificationsButtonCount.visibility = if (count>0) {View.VISIBLE} else {View.GONE}
        }
    }
}

fun CharSequence.isNumeric(): Boolean {
    val v = toString().toIntOrNull()
    return when(v) {
        null -> false
        else -> true
    }
}
