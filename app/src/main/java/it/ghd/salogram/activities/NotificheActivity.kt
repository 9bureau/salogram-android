package it.ghd.salogram.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.segment.analytics.Properties
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_notifiche.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class NotificheActivity : MainActivity() {

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_notifiche)
        screeName = TagNames.Screen.NOTIFICHE_SCREEN

        topTitleTV.text  = "NOTIFICHE"

        top_button_notifications.visibility = View.INVISIBLE

        notificheRV.let {
            it.layoutManager = LinearLayoutManager(this)
        }

        swiperefresh.setOnRefreshListener {
            reloadData()
            trackEvent(TagNames.NOTIFICATION_LIST_REFRESH, null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }

    fun reloadData(){
        loadingOverlay?.visibility = View.VISIBLE

        Manager.getAllNotifications { error, message, data ->

            swiperefresh.isRefreshing = false

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore), message, getString(R.string.riprova), getString(
                        R.string.annulla
                    )
                ) { confirmed ->
                    if (confirmed) {
                        reloadData()
                    } else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }
            } else {
                //ok
                loadingOverlay?.visibility = View.GONE

                data?.let {
                    notificheRV.adapter = JSONListAdapter(it, this)
                }

                updateNotificationBadge()
            }
        }
    }

    private class DataHolder(
        inflater: LayoutInflater,
        parent: ViewGroup,
        context: NotificheActivity
    ) : RecyclerView.ViewHolder(inflater.inflate(R.layout.notifiche_list_item, parent, false)) {
        private lateinit var mTitleView: TextView
        private lateinit var mTextView: TextView
        public lateinit var mIcon: ImageView
        public lateinit var context: NotificheActivity

        init {
            mTitleView = itemView.findViewById(R.id.notifiche_list_item_title)
            mTextView = itemView.findViewById(R.id.notifiche_list_item_subtitle)
            mIcon = itemView.findViewById(R.id.readCircleIV)
            this.context = context
        }

        fun bind(row: JSONObject) {
            mTitleView.text = row.getString("title")
            mTextView.text = row.getString("content")
            mIcon.visibility = if (row.isNull("read_at")) View.VISIBLE else View.GONE  //if (Manager.notificationIsRead(row)) View.GONE else View.VISIBLE
            
            if (row.isNull("url") || row.getString("url").trim().isEmpty() || row.isNull("url")){
                mTextView.isClickable = false
                mTitleView.isClickable = false
            }
            else {

                /*

                mTitleView.isClickable = true
                mTitleView.setOnClickListener {
                    var strUrl = row.getString("url")
                    Log.v("PAOLO", "cliccato 1")

                    if (!strUrl.startsWith("http://") && !strUrl.startsWith("https://")) {
                        strUrl = "http://$strUrl"
                    }

                    Log.v("PAOLO", "strUrl 2 = " + strUrl)

                    context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(strUrl)))
                }

                mTextView.isClickable = true
                mTextView.setOnClickListener {
                    var strUrl = row.getString("url")

                    Log.v("PAOLO", "cliccato 2")

                    if (!strUrl.startsWith("http://") && !strUrl.startsWith("https://")) {
                        strUrl = "http://$strUrl"
                    }

                    Log.v("PAOLO", "strUrl 1 = " + strUrl)

                    context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(strUrl)))
                }

                */
            }
        }

    }

    private class JSONListAdapter(
        private val list: JSONArray,
        val context: NotificheActivity
    ) : RecyclerView.Adapter<DataHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataHolder {
            val inflater = LayoutInflater.from(parent.context)
            return DataHolder(inflater, parent, context)
        }

        override fun onBindViewHolder(holder: DataHolder, position: Int) {
            val notifica: JSONObject = list.getJSONObject(position)
            holder.itemView.setOnClickListener {

                /*if (!not.isNull("read_at")) {
                    //already readen, so go back
                }
                else {*/

                val postID = notifica.getInt("post_id")

                Log.v("PAOLO", "cliccato 3")
                Log.v("PAOLO", "postID = $postID")

                if (postID > 0) {
                    Manager.getPost(postID) {
                        error, message, postJson ->

                        if (error) {
                            Log.v("PAOLO", "post non scaricato $message")
                        }
                        else {
                            if (postJson != null) {

                                val idNotifica = notifica.getInt("id")
                                Log.v("PAOLO", "Notifica JSON")
                                Log.v("PAOLO", notifica.toString())
                                Manager.sendNotificationView(idNotifica) { error, message ->
                                } //update server

                                val properties = Properties().apply {
                                    putValue("action", "open post $postID")
                                }
                                trackEvent(TagNames.TAP_ON_NOTIFICATION_OPEN_POST, properties)

                                Log.v("PAOLO", "post scaricato $message, lo apro")

                                val intent = Intent(context, EditorActivity::class.java)
                                intent.putExtra("POST", true)

                                try {
                                    if (postJson.has("data")) {
                                        val data = postJson.getJSONObject("data")
                                        Log.v("PAOLO", data.toString())
                                        intent.putExtra("DATA", data)
                                        context.startActivity(intent)
                                    }
                                }
                                catch (ex : Exception ) {
                                    Log.v("PAOLO", ex.message.toString())
                                }
                            }
                        }
                    }
                } else {
                    if (notifica.has("url")) {
                        Log.v("PAOLO", "url trovato")

                        var url = notifica.getString("url")

                        if (!TextUtils.isEmpty(url) && url != "null") {

                            Log.v("PAOLO", "cliccato su url = $url")

                            if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                url = "https://$url"
                            }

                            val properties = Properties().apply {
                                putValue("action", "open url $postID")
                            }
                            trackEvent(TagNames.TAP_ON_NOTIFICATION_OPEN_URL, properties)

                            Log.v("PAOLO", "strUrl 1 = " + url)
                            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                        }
                    } else {
                        Log.v("PAOLO", "ne url ne post id")
                    }
                }

                //context.loadingOverlay?.visibility = View.VISIBLE
               // }

            }
            holder.bind(notifica)
        }

        override fun getItemCount(): Int = list.length()
    }

    override fun onPause() {

        //record notifications
        /*Manager.instance.recordNotificationsView(views) { error, message ->
            Utils.log("Updating notifications views: $(error):$(message)")
        }
        */
        super.onPause()
    }
}