package it.ghd.salogram.activities

import android.webkit.WebSettings
import android.webkit.WebViewClient
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import kotlinx.android.synthetic.main.activity_terms.*

class TermsActivity : MainActivity() {

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_terms)

        val webSetting: WebSettings = browser.settings
        webSetting.builtInZoomControls = true
        browser.webViewClient = WebViewClient()
        browser.loadUrl("file:///android_asset/terms.html")
        screeName = TagNames.Screen.TERMS_SCREEN
    }

    override fun hasLogo() : Boolean {
        return false
    }

    override fun showGroupLogo() {
    }

    override fun onResume() {
        super.onResume()
    }
}