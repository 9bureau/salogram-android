package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_post.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.post_list_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject


class PostHomeActivity : MainActivity(){

    var categories = JSONArray()
    var categoriesAll = JSONArray()
    var popolari = JSONArray()
    var postOFTheWeek = JSONObject()
    var nuovi = JSONArray()

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_post)
        screeName = TagNames.Screen.POST_HOME_SCREEN // POST_HOME
        topTitleTV.text = "POST"

        menuPostButton.isSelected = true

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL

        categorieRV.layoutManager = linearLayoutManager
        categorieRV.setHasFixedSize(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            categorieRV.setOnScrollChangeListener { view: View, i: Int, i1: Int, i2: Int, i3: Int ->
                val x: Int = i - i2
                if (x > 0) {
                    trackEvent(TagNames.SCROLL_LEFT_POST_CATEGORIES_HOME, null)
                } else if (x < 0) {
                    trackEvent(TagNames.SCROLL_RIGHT_POST_CATEGORIES_HOME, null)
                }
            }
        }

        val linearLayoutManagerNuovi = LinearLayoutManager(this)
        linearLayoutManagerNuovi.orientation = LinearLayoutManager.HORIZONTAL

        nuoviRV.layoutManager = linearLayoutManagerNuovi
        //popolariRV.setHasFixedSize(true)

        val linearLayoutManagerPopolari = LinearLayoutManager(this)
        linearLayoutManagerPopolari.orientation = LinearLayoutManager.HORIZONTAL

        popolariRV.layoutManager = linearLayoutManagerPopolari

        swipeToRefresh.setOnRefreshListener {
            loadData(true)
            trackEvent(TagNames.POST_HOME_REFRESH, null)
        }
    }

    override fun onResume() {
        super.onResume()

      if (firstStart){
          loadData(true)
          firstStart = false
      }

        Manager.postHomeContents = JSONArray()
    }

    fun usePostOfTheWeek(view: View?){
        val intent = Intent(this, EditorActivity::class.java)
        intent.putExtra("POST", true)
        intent.putExtra("DATA", postOFTheWeek)
        startActivity(intent)
        trackEvent(TagNames.TAP_ON_POST_WEEK_FROM_HOME, null)
        Companion.trackEvent(TagNames.TAP_ON_POST, null)
        Companion.trackEvent(TagNames.TAP_ON_ITEM, null)
    }

    private fun loadData(force: Boolean) {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getHome(force) { error, message, jsonObject ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData(force)
                    else {
                        loadingOverlay?.visibility = View.GONE
                        swipeToRefresh.isRefreshing = false
                    }
                }

            } else if (jsonObject != null) {

                loadingOverlay?.visibility = View.GONE

                categoriesAll = JSONArray()
                categories = JSONArray()
                popolari = JSONArray()

                categoriesAll = jsonObject.getJSONArray("categories")

                (0 until categoriesAll.length()).forEach {
                    val jo = categoriesAll.getJSONObject(it)

                    if (jo.getInt("parent") == 0 && (jo.getInt("has_children_with_posts") > 0 || jo.getJSONArray(
                            "posts"
                        ).length() > 0)
                    ) {
                        categories.put(jo)
                    }
                }

                post_categorie_ll.visibility = if (categories.length() > 0) View.VISIBLE else View.GONE

                popolari = jsonObject.getJSONArray("popular_posts")
                post_popolari_ll.visibility = if (popolari.length() > 0) View.VISIBLE else View.GONE

                nuovi = jsonObject.getJSONArray("recent_posts")
                post_nuovi_ll.visibility = if (nuovi.length() > 0) View.VISIBLE else View.GONE

                if (jsonObject.has("post_of_the_week")) {
                    postOFTheWeek = jsonObject.getJSONObject("post_of_the_week")
                    post_settimana_ll.visibility = View.VISIBLE
                } else {
                    post_settimana_ll.visibility = View.GONE
                }

                val adapterCategories = CategoriesRecyclerAdapter(categories, this)
                //adapterCategories.setHasStableIds(true)
                categorieRV.adapter = adapterCategories
                categorieRV.visibility = if (categories.length() > 0) View.VISIBLE else View.GONE

                val adapterPopolari = PopolariRecyclerAdapter(popolari, this)
                //adapterPopolari.setHasStableIds(true)

                popolariRV.adapter = adapterPopolari

                val adapterNuovi = RecentiRecyclerAdapter(nuovi, this)
                //adapterPopolari.setHasStableIds(true)

                nuoviRV.adapter = adapterNuovi

                ImageManager.loadImage(postOFTheWeek.getString("thumb_url"), postSettimanaIB)

                /*if (Utils.isDebugger ||  Utils.isEmulator) {

                    //test
                   // if (firstStart) {
                        usePostOfTheWeek(null)
                      //  firstStart = false
                    //}
                }*/

                swipeToRefresh.isRefreshing = false
            }
        }
    }

    //DATA

    private class CategoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var index: Int = 0
        private var content: JSONObject? = null
        private var contents:JSONArray = JSONArray()

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, events: JSONArray, index: Int) {
            this.content = event

            this.index = index

            contents = events

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
           // view.titleTVCell.text = event.optString("name")
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context

            val catSelected = contents.getJSONObject(index)

            val isParent = catSelected.getInt("has_children_with_posts")>0 && catSelected.getInt("parent") == 0

            if (!isParent) {

                var categoriesFiltered = JSONArray()

                //for (jo:JSONObject in contents){
                (0 until contents.length()).forEach {
                    val jo = contents.getJSONObject(it)
                    if (jo.getJSONArray("posts").length()>0 && jo.getInt("parent") == 0 ) {
                        categoriesFiltered.put(jo)
                    }
                }

                Manager.postHomeContents = categoriesFiltered

                val intent = Intent(context, PostListActivity::class.java)
                intent.putExtra("CAT", content!!)
                // intent.putExtra("CATS",contents)
                intent.putExtra("INDEX", index)
                context.startActivity(intent)
                trackEvent(TagNames.TAP_ON_POST_CATEGORY_FROM_HOME, null)
            }
            else {
                //is parent

                Manager.postHomeContents = contents

                val intent = Intent(context, PostCategoriesListActivity::class.java)
                intent.putExtra("CAT", content!!)
                //intent.putExtra("INDEX", index)
                // intent.putExtra("CATS",contents)
                context.startActivity(intent)
                trackEvent(TagNames.TAP_ON_POST_CATEGORY_FROM_HOME, null)
            }
        }
    }

    private class CategoriesRecyclerAdapter(val items: JSONArray, val context: Context) : RecyclerView.Adapter<CategoriesHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position), items, position)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
            return CategoriesHolder(
                LayoutInflater.from(context).inflate(R.layout.post_list_cell, parent, false)
            )
        }
    }

    //POPOLARI

    private class PopolariHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("POST", true)
            intent.putExtra("DATA", content!!)
            context.startActivity(intent)
            trackEvent(TagNames.TAP_ON_POPULAR_POST_FROM_HOME, null)
            trackEvent(TagNames.TAP_ON_POST, null)
            trackEvent(TagNames.TAP_ON_ITEM, null)
        }
    }

    private class PopolariRecyclerAdapter(val items: JSONArray, val context: Context) : RecyclerView.Adapter<PopolariHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: PopolariHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopolariHolder {
            return PopolariHolder(
                LayoutInflater.from(context).inflate(R.layout.post_list_cell, parent, false)
            )
        }
    }

    //RECENTI

    private class RecentiHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("POST", true)
            intent.putExtra("DATA", content!!)
            context.startActivity(intent)
            trackEvent(TagNames.TAP_ON_NEW_POST_FROM_HOME, null)
            trackEvent(TagNames.TAP_ON_POST, null)
            trackEvent(TagNames.TAP_ON_ITEM, null)
        }
    }

    private class RecentiRecyclerAdapter(val items: JSONArray, val context: Context) : RecyclerView.Adapter<RecentiHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: RecentiHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentiHolder {
            return RecentiHolder(
                LayoutInflater.from(context).inflate(R.layout.post_list_cell, parent, false)
            )
        }
    }

}
