package it.ghd.salogram.activities

import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.segment.analytics.Properties
import it.ghd.salogram.*
import it.ghd.salogram.R
import it.ghd.salogram.adapters.GroupAdapter
import it.ghd.salogram.adapters.MyRecyclerAdapter
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.model.FacebookPage
import it.ghd.salogram.model.FacebookRegistrationResponse
import it.ghd.salogram.model.Owner
import it.ghd.salogram.model.UserAccount
import kotlinx.android.synthetic.main.activity_profilo.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.group_selector.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONObject


class ProfileActivity : MainActivity() {

    private lateinit var callbackManager : CallbackManager
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = $logoImageUrl")
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    private fun checkAccessToken() : Boolean {

        Log.v("FACEBOOKPAOLO", "checkAccessToken")

        Manager.getFacebookAccounts { error, messaggio, fbResponse ->
            if (error) {
                Log.v("FACEBOOKPAOLO", "elenco accounts non scaricato: $messaggio")
                showFacebookLogin(true)
            } else {
                Log.v("FACEBOOKPAOLO", "elenco accounts scaricato: $messaggio")

                if (fbResponse != null) {
                    handleUsers(fbResponse.data.user)
                    handlePages(fbResponse.data.pages, false)
                }
            }
        }

        return true
    }

    private fun setNewFacebookPage(page: FacebookPage) {
        Manager.updateSelectedFacebookPage(page)

        val properties = Properties().apply {
            putValue("nome_pagina", page.item_name)
            putValue("id_pagina", page.item_id)
        }
        //Analytics.with(CustomApplication.context).track(
        trackEvent(TagNames.FACEBOOK_PAGE_LINK, properties)
    }

    private fun getFacebookPageInfo(page: FacebookPage) {

        Log.v("FACEBOOKPAOLO", "foto pagina = ${page.item_pic}")
        ImageManager.loadImage(page.item_pic, facebook_page_picture)
        facebook_page_name.text = page.item_name
        //showFacebookLogin(false) NON CI VA SENNO' SOVRASCRIVE
    }

    fun edit_facebook_pages(view: View?) {
        Log.v("FACEBOOKPAOLO", "edit_facebook_pages")
        downloadFacebookPages()
        trackEvent(TagNames.CHANGE_FB_PAGE_FROM_PROFILE, null)
    }

    private fun downloadFacebookPages() {
        Manager.getFacebookPages { error, msg, response ->
            if (error) {
                Log.v("FACEBOOKPAOLO", "elenco pagine non scaricato: $msg")
            } else {
                Log.v("FACEBOOKPAOLO", "elenco pagine scaricato: $msg")

                try {
                    val paginejson = msg.toString().trim()

                    Log.v("FACEBOOKPAOLO", "paginejson = $paginejson")

                    if (response != null && response.pages.isNotEmpty()) {

                        mostraSceltaPagine(response.pages)

                    } else {
                        Log.v("FACEBOOKPAOLO", "Nessuna pagina Facebook trovata")
                        MaterialDialog(this).show{
                            title(R.string.warning)
                            message(R.string.nopages)
                        }
                    }
                }
                catch (ex: Exception) {
                    Log.v("FACEBOOKPAOLO", "[downloadFacebookPages] ex = " + ex.localizedMessage)
                }

            }
        }
    }

    private fun mostraSceltaPagine(pagine: List<FacebookPage>) {
        Log.v("FACEBOOKPAOLO", "[mostraSceltaPagine] numero di pagine = " + pagine.size)
        val adapter: RecyclerView.Adapter<MyRecyclerAdapter.FacebookPageHolder> = MyRecyclerAdapter(
            pagine
        )

        MaterialDialog(this).show {
            title(text = "Seleziona la pagina del tuo salone")
            icon(R.mipmap.ic_launcher)
            customListAdapter(adapter)

            positiveButton(R.string.ok, "Seleziona") {
                Log.v("FACEBOOKPAOLO", "[mostraSceltaPagine] cliccato su si")
                val pagina = pagine.firstOrNull { it.active }
                if (pagina != null) {
                    setNewFacebookPage(pagina)
                    getFacebookPageInfo(pagina)
                }
            }
        }
    }

    fun facebookUserLogout(view: View?) {

        Log.v("FACEBOOKPAOLO", "facebookUserLogout")

        askQuestion(
            "Attenzione",
            "Sei sicuro di voler uscire da Facebook? Non potrai più pubblicare sulle tue pagine",
            "Si",
            "No"
        ) {
            if (it) {
                Log.v("FACEBOOKPAOLO", "SI")
                //showFacebookLogin(true)
                unlinkFacebook()
            }
            else {
                Log.v("FACEBOOKPAOLO", "NO")
            }
        }
    }

    override fun unlinkFacebook() {
        super.unlinkFacebook() //slogga anche da facebook
        Log.v("FACEBOOKPAOLO", "unlinkFacebook")
        showFacebookLogin(true)

        val properties = Properties().apply {
        }
        trackEvent(TagNames.FACEBOOK_PAGE_UNLINK, properties)
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_profilo)
        screeName = TagNames.Screen.PROFILE_SCREEN

        if (intent.extras != null) {
            if (intent.extras!!.containsKey("scrollbottom")) {
                val scrollbottom = intent.extras!!.getBoolean("scrollbottom")
                if (scrollbottom) profile_scroller.post { profile_scroller.fullScroll(View.FOCUS_DOWN) }
            }
        }

        topTitleTV.text = "PROFILO"

        menuProfiloButton.isSelected = true

        if (! checkAccessToken() ) {

        }

        loadOwner()

        //paolo
        //login_button.setPermissions(listOf("email", "pages_manage_posts", "pages_show_list"))
        login_button.setPermissions(listOf("email", "pages_manage_posts", "pages_show_list", "pages_read_engagement", "public_profile"))

        // Callback registration
        callbackManager = CallbackManager.Factory.create()

        // Callback registration
        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                // App code
                if (loginResult == null) {
                    Log.v("FACEBOOKPAOLO", "onSuccess ma risposta nulla")
                    return
                }

                val properties = Properties().apply {
                    putValue(TagNames.PARAM_ESITO, true)
                }
                trackEvent(TagNames.FACEBOOK_LOGIN, properties)

                val userID = Utils.sharefPrefs.getString("user_id", "")
                if (userID!!.isNotEmpty()) {
                    Log.v("FACEBOOKPAOLO", "preferenza salvata")
                    Utils.sharefPrefsEditor.putString(EditorMainFragment.FB_LOGGED_USER, userID)
                        .apply()
                }

                val aToken = loginResult.accessToken // IL TOKEN E' DI LUNGA DURATA

                Log.v("FACEBOOKPAOLO", "onSuccess ma risposta nulla")

                Manager.sendFacebookAccessToken(aToken)
                { error: Boolean, message: String?, risposta: FacebookRegistrationResponse? ->
                    if (error) {
                        Log.v("FACEBOOKPAOLO", "token non inviato")
                    } else {
                        Log.v("FACEBOOKPAOLO", "token inviato")

                        if (risposta != null) {
                            handleUsers(risposta.data.user)
                            handlePages(risposta.data.pages, true)
                        }
                    }

                    Log.v("FACEBOOKPAOLO", "message = $message")
                }
            }

            override fun onCancel() {
                // App code
                Log.v("FACEBOOK", "onCancel")
            }

            override fun onError(exception: FacebookException) {
                // App code
                Log.v("FACEBOOK", "onError: " + exception.localizedMessage)

                val properties = Properties().apply {
                    putValue(TagNames.PARAM_ESITO, false)
                }
                trackEvent(TagNames.FACEBOOK_LOGIN, properties)
            }
        })

        //

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_behavior_view_profile as LinearLayout)

        linearLayoutManager = LinearLayoutManager(this)
        lista_gruppi.layoutManager = linearLayoutManager

        val adapter = prepareAdapter()
        if (adapter != null) {
            lista_gruppi.adapter = adapter
            Log.v("PAOLO1", "ci sono gruppi")
            change_group_btn.visibility = View.VISIBLE
        }
        else {
            Log.v("PAOLO1", "non ci sono gruppi")
            change_group_btn.visibility = View.GONE
        }

        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(view: View, i: Int) {
                // do something when state changes
                Log.v("PAOLO", "onStateChanged")
            }

            override fun onSlide(view: View, v: Float) {
                // do something when slide happens
                Log.v("PAOLO", "onSlide")
            }
        })

        //ImageManager.loadImage("https://salogram.com/web/logos/wella.png", group_logo)
        // time=" + Random.nextInt(0, Int.MAX_VALUE)

        val autologin = Utils.sharefPrefs.getBoolean("autologin", false)
        Log.v("NOTIFICHE", "autologin ora è  $autologin")
    }

    private fun loadOwner() {
        Log.v("PAOLO1", "loadOwner")
        if (Utils.sharefPrefs.contains("owner")) {
            Log.v("PAOLO1", "owner presente")
            val ownerJson = Utils.sharefPrefs.getJSONObject("owner")
            val owner = Owner.parse(ownerJson)
            if (owner != null) ImageManager.loadImage(owner.getLightPicture(), group_logo)
        }
        else {
            Log.v("PAOLO1", "owner mancante")
            group_logo.visibility = View.GONE
            change_group_btn.visibility = View.GONE
        }
    }

    fun prepareAdapter(): RecyclerView.Adapter<GroupAdapter.GroupHolder>? {

        if (!Utils.sharefPrefs.contains(EditorMainFragment.USER_ACCOUNTS)) {
            return null
        }

        val accounts = Utils.sharefPrefs.getJSONArray(EditorMainFragment.USER_ACCOUNTS)
        Log.v("PAOLO1", "accounts")
        Log.v("PAOLO1", accounts.toString())

        if (accounts.length() == 0) {
            return null
        }

        val lista = ArrayList<UserAccount>()
        accounts.iterator().forEach {
            val account = Gson().fromJson(it.toString(), UserAccount::class.java)
            lista.add(account)
        }

        val adapter: RecyclerView.Adapter<GroupAdapter.GroupHolder> = GroupAdapter(R.layout.list_item_group_condensed, lista) {
            val gruppo = lista[it]

            //leggo credenziali salvate
            val email = Utils.sharefPrefs.getString("email","")
            val password = Utils.sharefPrefs.getString("password", "")

            //NO, NON VA FATTO LOGOUT
            //logout(null, true)

            val proprieta = Properties()
            proprieta["new_group"] = gruppo.goupName
            trackEvent(TagNames.CHANGE_GROUP_ACTION, properties)

            Log.v("PAOLO", "email = $email")
            Log.v("PAOLO", "password = $password")
            Log.v("PAOLO", "scelto gruppo ${gruppo.goupName}")
            Log.v("PAOLO", "groupMail ${gruppo.groupMail}")
            doGroupLogin(gruppo.groupMail, password!!, false)
        }

        return adapter
    }

    fun changeAccount(view: View) {
        slideUpDownBottomSheet()
    }

    fun hideBottomSheet() : Boolean {

        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            return true
        }

        return false
    }

    private fun slideUpDownBottomSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            //apro
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            trackEvent(TagNames.GROUP_CHANGE_MENU_OPEN, null)
        } else {
            //chiudo
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            trackEvent(TagNames.GROUP_CHANGE_MENU_CLOSE, null)
        }
    }

    private fun handleUsers(user: FacebookPage?) {
        Log.v("FACEBOOKPAOLO", "handleUsers")

        if (user != null) {

            showFacebookLogin(false)

            facebook_user_name.text = user.item_name

            if (!TextUtils.isEmpty(user.item_pic)) {
                ImageManager.loadImage(user.item_pic, facebook_user_picture)
            } else {
                ImageManager.loadImage(
                    "https://salogram.com/app/resources/facebook/pages/default.jpg",
                    facebook_user_picture
                )
                Log.v("FACEBOOKPAOLO", "foto nulla")
            }

        } else {
            Log.v("FACEBOOKPAOLO", "user nullo")

            showFacebookLogin(true)
        }
    }

    private fun handlePages(pages: List<FacebookPage>?, askPage: Boolean) {

        if (pages != null && pages.isNotEmpty()) {
            Log.v("FACEBOOKPAOLO", "numero di pagine = " + pages.size)

            val paginaScelta = pages.firstOrNull() { it.active }

            if (paginaScelta != null) {
                Log.v("FACEBOOKPAOLO", "pagina = " + paginaScelta.item_name)

                getFacebookPageInfo(paginaScelta)
            } else {
                Log.v("FACEBOOKPAOLO", "[getFacebookPageList] nessuna pagina predefinita trovata")

                showFacebookLogin(false)
                showDefaultPage()
                if (askPage) mostraSceltaPagine(pages)
            }
        } else {
            Log.v("FACEBOOKPAOLO", "[getFacebookPageList] lista pagine vuota")
            showFacebookLogin(true)
        }
    }

    private fun showDefaultPage(){
        ImageManager.loadImage(
            "https://salogram.com/app/resources/facebook/pages/default.jpg",
            facebook_page_picture
        )
        facebook_page_name.text = "nessuna"
    }

    private fun showFacebookLogin(show: Boolean) {
        if (show) {

            //ImageManager.loadImage("https://salogram.com/app/resources/facebook/pages/default.jpg", facebook_user_picture)
            //facebook_user_name.text = "nessuno"

            //ImageManager.loadImage("https://salogram.com/app/resources/facebook/pages/default.jpg", facebook_page_picture)
            //facebook_page_name.text = "nessuna"

            login_button.visibility = View.VISIBLE
            user_pages_wrapper.visibility = View.GONE
            user_login_wrapper.visibility = View.GONE
        }
        else {
            login_button.visibility = View.GONE
            user_pages_wrapper.visibility = View.VISIBLE
            user_login_wrapper.visibility = View.VISIBLE

            //ImageManager.loadImage("https://salogram.com/app/resources/facebook/pages/default.jpg", facebook_page_picture)
            //facebook_page_name.text = "nessuna"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        Log.v("FACEBOOK", "onActivityResult")
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun loadData() {
        loadingOverlay?.visibility = View.VISIBLE

        Manager.getProfileInformations { error, message, info ->
            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                loadingOverlay?.visibility = View.GONE

                if (info != null) {

                    Log.v("PAOLO1", "carico dati utente")

                    var email = ""
                    if (Utils.sharefPrefs.contains("owner")) {
                        val ownerJson = Utils.sharefPrefs.getJSONObject("owner") ?: JSONObject()
                        val owner = Owner.parse(ownerJson)
                        if (owner != null) {
                            email = owner.email
                        }
                        Log.v("PAOLO1", "messa mail owner = $email")
                    }
                    if (email.isEmpty() || !email.isValidEmail()) {
                        if (info.has("email"))  {
                            email = info.optString("email")

                            if (email.contains("+")) {
                                email = Utils.getOriginalMail(email)
                                Log.v("PAOLO1", "messa mail modificata = $email")
                            }
                            else {
                                Log.v("PAOLO1", "messa mail originale = $email")
                            }
                        }
                    }

                    profile_email_label.text = email

                    if (info.has("first_name")) profile_user_label.text =
                        "${info.optString("first_name")} ${info.optString("last_name")}"
                    else if (info.has("name")) profile_user_label.text =
                        "${info.optString("name")} ${info.optString("last_name")}"
                    if (info.has("venue")) profile_profilo_label.text =
                        (info.getJSONArray("venue").getJSONObject(0)).optString("name")
                    //if (info.has("email")) profile_email_label.text = Utils.getOriginalMail(info.optString("email"))
                    if (info.has("tel")) profile_phone_label.text = info.optString("tel")
                    if (info.has("customer_code")) profile_codicecliente_label.text =
                        info.optString("customer_code")

                    profile_password_label.text = "********"
                }
            }
        }
    }

    fun logout(view: View){
       super.logout(view, false)

    }

    fun resetPassword(view: View) {

        askQuestion(
            getString(R.string.app_name),
            "Questa operazione effettuerà il reset della password e il logout dall'attuale profilo, vuoi procedere?",
            getString(R.string.ok),
            getString(R.string.annulla)
        ) { confirmed ->
            if (confirmed) {
                Manager.forgotPassword(profile_email_label.text.toString()) { error, message ->

                    //esito negativo
                    if (error && message != null) {

                        val properties = Properties()
                        properties[TagNames.PARAM_ESITO] = false

                        trackEvent(TagNames.PASSWORD_RESET, properties)

                        askQuestion(
                            getString(R.string.errore),
                            message,
                            getString(R.string.riprova),
                            getString(R.string.annulla)
                        ) { confirmed ->
                            if (confirmed) resetPassword(view)
                            else {
                                loadingOverlay?.visibility = View.GONE
                            }
                        }
                    } else {
                        //esito positivo

                        val properties = Properties()
                        properties[TagNames.PARAM_ESITO] = true

                        trackEvent(TagNames.PASSWORD_RESET, properties)

                        showAlert(
                            Utils.APP_NAME,
                            "Abbiamo ricevuto la tua richiesta, riceverai una email contenente le procedure necessarie per effettuare il reset della password."
                        ) {
                            logout(view, true)
                        }
                    }
                }
            }
        }
    }

    fun goToTerms(view: View) {
        val link = "https://www.bsidecomm.com/termini-e-condizioni-salogram/"
        val intent = Intent(this, WebActivity::class.java).apply {
            setFlags(intent)
            putExtra("url",link)
        }
        startActivity(intent)
    }

    fun goToPrivacyPolicy(view: View) {
        val link = "https://www.iubenda.com/privacy-policy/75745741"
        val intent = Intent(this, WebActivity::class.java).apply {
            setFlags(intent)
            putExtra("url",link)
        }
        startActivity(intent)
    }

    fun goToClausole(view: View) {
        val link = "https://www.bsidecomm.com/clausole-salogram/"
        val intent = Intent(this, WebActivity::class.java).apply {
            setFlags(intent)
            putExtra("url",link)
        }
        startActivity(intent)
    }
}
