package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_post_categories_list.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.post_grid_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class PostCategoriesListActivity : MainActivity() {

    var cats:JSONArray = JSONArray()
    var selectedCat:JSONObject? = null
    var childrenCats:JSONArray = JSONArray()

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_post_categories_list)
        screeName = TagNames.Screen.POST_CATEGORIES_LIST_SCREEN
        topTitleTV.text = ""

        menuPostButton.isSelected = true

        val gridLayoutManager = GridLayoutManager(this,2)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL

        postsRV.layoutManager = gridLayoutManager
        postsRV.setHasFixedSize(true)
        cats = Manager.categories
        selectedCat = intent.getJSONObjectExtra("CAT")
        childrenCats = JSONArray()

        val category_id = selectedCat!!.getInt("category_id")

        for (jo:JSONObject in cats){
            if (jo.getInt("parent") == category_id) {
                childrenCats.put(jo)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if (firstStart){
            loadData()
            firstStart = false
        }
    }

    fun selectCategory(cat:JSONObject){

       /* var index = -1
        var counter = 0

        for (jo:JSONObject in cats){
            if (jo == cat) {
                index = counter
                break
            }
            counter++
        }

        if (counter>=0) {*/
            val isParent = cat.getInt("has_children_with_posts") > 0 && cat.getInt("parent") == 0

            if (!isParent) {

                Manager.postHomeContents = childrenCats

                val intent = Intent(this, PostListActivity::class.java)
                intent.putExtra("CAT", cat)
                // intent.putExtra("CATS",contents)
                //intent.putExtra("INDEX", index)
                startActivity(intent)
                trackEvent(TagNames.TAP_ON_POST_CATEGORY_FROM_CATEGORY, null)
            } else {
                //is parent

                var categories = JSONArray()

                for (jo:JSONObject in cats){
                    if (jo.getInt("parent") == cat.getInt("category_id")) {
                        categories.put(jo)
                    }
                }

                Manager.postHomeContents = categories

                val intent = Intent(this, PostCategoriesListActivity::class.java)
                intent.putExtra("CAT", cat)
                // intent.putExtra("CATS",contents)
                //intent.putExtra("INDEX", index)
                startActivity(intent)
                trackEvent(TagNames.TAP_ON_POST_SUBCATEGORY, null)
            }
      /*  }
        else {
            print("Category index not found!")
            Toast.makeText(this,"Impossibile accedere alla categoria selezionata",Toast.LENGTH_SHORT).show()
        }*/
    }

    private fun loadData() {
        topTitleTV.text =  selectedCat!!.getString("name")
        val adapterContents = PostsRecyclerAdapter(childrenCats, this)
        postsRV.adapter = adapterContents
    }

    //DATA

    private class PostsHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event
            ImageManager.loadImage(event.optString("thumb_url"), itemView.imageViewCell)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            // non tracciare evento qui
            (itemView.context as PostCategoriesListActivity).selectCategory(content!!)
        }
    }

    private class PostsRecyclerAdapter(
        val items: JSONArray,
        val context: Context
    ) : RecyclerView.Adapter<PostsHolder>() {

        companion object{
            var cellHeight:Int = -1
        }

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: PostsHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.categories_grid_cell, parent, false)
            cellHeight = parent.measuredWidth / 2
            view.minimumHeight = cellHeight
            view.requestLayout()
            return PostsHolder((view))
        }
    }
}