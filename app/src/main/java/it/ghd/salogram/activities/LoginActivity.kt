package it.ghd.salogram.activities
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.DialogBehavior
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.ModalDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.checkbox.checkBoxPrompt
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.customListAdapter
import com.facebook.login.LoginManager
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.gson.Gson
import com.onesignal.OneSignal
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import com.segment.analytics.Traits
import it.ghd.salogram.*
import it.ghd.salogram.CustomApplication.Companion.context
import it.ghd.salogram.adapters.GroupAdapter
import it.ghd.salogram.adapters.MyRecyclerAdapter
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.model.FacebookPage
import it.ghd.salogram.model.Notification
import it.ghd.salogram.model.UserAccount
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_profilo_edit.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.fragment_loading.loadingOverlay
import kotlinx.android.synthetic.main.loading_overlay.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.URI
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.collections.ArrayList

class LoginActivity : MainActivity(){

    //lateinit var callbackManager: CallbackManager
    var waitingServerResponse = false
    var autologin = false

    val UPDATE_REQUEST_CODE = 12345678

    lateinit var appUpdateManager : AppUpdateManager

    override fun hasLogo() : Boolean {
        return false
    }

    override fun showGroupLogo() {
    }

    override fun onCreateAfterParent() {

    }

    private fun checkNewAppVersion() {

        Log.v("AGGIORNAMENTO", "checkNewAppVersion")
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->

            Log.v("AGGIORNAMENTO", "on success")

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                // This example applies an immediate update. To apply a flexible update
                // instead, pass in AppUpdateType.FLEXIBLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {

                trackEvent(TagNames.IMMEDIATE_APP_UPDATE_REQUEST, null)

                Log.v("AGGIORNAMENTO", "aggiornamento immediato richiesto")

                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    this,
                    AppUpdateOptions.newBuilder(AppUpdateType.IMMEDIATE).setAllowAssetPackDeletion(true).build(),
                    UPDATE_REQUEST_CODE)
            } else {
                Log.v("AGGIORNAMENTO", "nessun aggiornamento richiesto")
                //showAdvice("Nessun aggiornamento richiesto")
            }
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)

        //Utils.createKeyHash(this, "it.ghd.salogram")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        screeName = TagNames.Screen.LOGIN_SCREEN

        loginContainer.visibility = View.INVISIBLE

        appUpdateManager = AppUpdateManagerFactory.create(context)
        checkNewAppVersion()

        handleDeepLink(intent)

        var isLoggedIn  = false
        /*if (isLoggedIn){
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));

            val intent = Intent(this, HomeActivity::class.java).apply {

            }
            startActivity(intent)
        }
        else*/

       /* else if (Utils.sharefPrefs.getBoolean("autologin",true) && Utils.sharefPrefs.getBoolean("facebook",true)){
            loginActionFB(null)
        }*/

        if (Utils.isDebugger || Utils.isEmulator){

            val email = "paoloburzacca@gmail.com"
            val password = "paolo1986"

            //val email = "bob@bob.com"
            //val password = "password"

            //val email = "p.burzacca@9bureau.com"
            //val password = "password"

            //welcome_usernameField.setText(email)
            //welcome_passwordField.setText(password)
            //loginAction(null)
        }

        autologin = Utils.sharefPrefs.getBoolean("autologin",false)

        Log.v("NOTIFICHE", "onCreate di Login")
        Log.v("NOTIFICHE", "autologin = $autologin")

        if (autologin){

            if (intent.extras != null) {
                val forcedEmail = intent.extras!!.getString("forced_email")
                val forcedPassword = intent.extras!!.getString("forced_password")
                welcome_usernameField.setText(forcedEmail)
                welcome_passwordField.setText(forcedPassword)
            } else {
                val savedEmail = Utils.sharefPrefs.getString("email","")
                val savedPwd = Utils.sharefPrefs.getString("password","")
                welcome_usernameField.setText(savedEmail)
                welcome_passwordField.setText(savedPwd)
            }

            login_autologin_checkbox.isSelected = true
            val defEmail = welcome_usernameField.text.toString().trim()
            val defPwd = welcome_passwordField.text.toString().trim()

            Log.v("NOTIFICHE", "procedo con autologin")
            Log.v("NOTIFICHE", "email = $defEmail")
            Log.v("NOTIFICHE", "password = $defPwd")

            if (defEmail.isNotEmpty() && defPwd.isNotEmpty()) {
                Log.v("NOTIFICHE", "credenziali valide")
                loginAction(null)
            }
            else {
                Log.v("NOTIFICHE", "credenziali non valide")
                Utils.sharefPrefsEditor.remove("autologin").apply()
            }
        } else {
            Log.v("NOTIFICHE", "NON procedo con autologin")
        }
    }

    private fun handleDeepLink(intent: Intent?) {
        val action: String? = intent?.action
        val data: Uri? = intent?.data

        val path = data?.path
        Log.v("PAOLO", "action = $action")
        Log.v("PAOLO", "path = " + data?.path)
        Log.v("PAOLO", "query = " + data?.query)

        if (path == "/web/password/reset2") {
            //recupero password
            showPasswordRetrieveForm(data)
        }
    }

    fun showPasswordRetrieveForm(uri: Uri){
        val mail = uri.getQueryParameter("mail")
        val token = uri.getQueryParameter("token")

        if (mail == null) {
            Log.v("PAOLO", "mail nulla, esco")
            return
        }

        if (token == null) {
            Log.v("PAOLO", "token nullo, esco")
            return
        }

        if (mail.isEmpty()) {
            Log.v("PAOLO", "mail vuota, esco")
            return
        }

        if (token.isEmpty()) {
            Log.v("PAOLO", "token vuoto, esco")
            return
        }

        Log.v("PAOLO", "mail = " + mail)
        Log.v("PAOLO", "token = " + token)
    }

    fun forzaLogin(email: String, password: String) {
        Log.v("NOTIFICHE", "forza Login")
        login_autologin_checkbox.isSelected = true
        welcome_usernameField.setText(email)
        welcome_passwordField.setText(password)
        loginAction(null)
    }

    fun hideLoader() {
        loadingOverlay?.visibility = View.GONE
        loginContainer.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        Log.v("NOTIFICHE", "onResume di Login")
        Log.v("STATO", "OnResume di Login")

        hideLoader()

        //pulisco eventuali rimasugli
        Manager.logged = false
        if (Utils.sharefPrefs.contains(Manager.SHARED_PREFERENCES_USER_FIELD)) {
            Log.v("NOTIFICHE", "cancello preferenze")
            Utils.sharefPrefs.edit().remove(Manager.SHARED_PREFERENCES_USER_FIELD).apply()
        }

        //LoginManager.getInstance().logOut()
        //unlinkFacebook()
        // generateReleaseHASH(null)

        appUpdateManager
            .appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->

                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, IMMEDIATE, this, UPDATE_REQUEST_CODE
                    )
                }
            }
    }

    fun loginActionFB(view:View?){
        //todo
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        // callbackManager.onActivityResult(requestCode, resultCode, data) FB

        if (requestCode == UPDATE_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Log.v("PAOLO", "Aggiornamento ok")
                trackEvent(TagNames.IMMEDIATE_APP_UPDATE_SUCCESS, null)
            }
            else {
                Log.e("PAOLO","Aggiornamento fallito: $resultCode")
                trackEvent(TagNames.IMMEDIATE_APP_UPDATE_FAIL, null)
            }
        }
    }

    fun loginAction(view: View?) {
        hideKeyboard()

        if (welcome_usernameField.text.toString().trim().length < 3) {
            val alertDialog =
                AlertDialog.Builder(this) //set icon
                    .setIcon(R.mipmap.ic_launcher) //set title
                    .setTitle("Errore") //set message
                    .setMessage("Email di lunghezza insufficiente") //set positive button
                    .setPositiveButton(
                        "Ok"
                    ) { _, _ ->
                        //set what would happen when positive button is clicked
                    }
                    .show()
            return
        }

       /* if (!Manager.isValidEmail(welcome_usernameField.text.toString())) {
            val alertDialog =
                AlertDialog.Builder(this) //set icon
                    .setIcon(R.mipmap.ic_launcher) //set title
                    .setTitle("Errore") //set message
                    .setMessage("Email in formato non valido") //set positive button
                    .setPositiveButton(
                        "Ok"
                    ) { dialogInterface, i ->
                        //set what would happen when positive button is clicked
                    }
                    .show()
            return
        }*/

        if (welcome_passwordField.text.toString().trim().length < 8) {
            val alertDialog =
                AlertDialog.Builder(this) //set icon
                    .setIcon(R.mipmap.ic_launcher) //set title
                    .setTitle("Errore") //set message
                    .setMessage("La password inserita non ha una lunghezza valida") //set positive button
                    .setPositiveButton(
                        "Ok"
                    ) { _, _ ->
                        //set what would happen when positive button is clicked
                    }
                    .show()
            return
        }

        //hideKeybord
        hideKeyboard()

        var retryIfError = false

        //unlinkFacebook(null)

        loadingOverlay?.visibility = View.VISIBLE
        loginContainer.visibility = View.INVISIBLE

        var username = welcome_usernameField.text.toString().trim()
        val password = welcome_passwordField.text.toString().trim()
        val originale = Utils.getCleanMail(username)

        // CONTROLLO SE C'E' NOTIFICA PENDING

        if (Utils.sharefPrefs.contains("notification_post")) {
            Log.v("NOTIFICHE", "ho trovato una notifica nelle preferenze")
            val data = Utils.sharefPrefs.getJSONObject("notification_post")
            val notification = Gson().fromJson(data.toString(), Notification::class.java)
            if (notification != null) {

                val postOwners = notification.owners
                val postOwnersSlug = notification.owners_slug

                Log.v("NOTIFICHE", "la notifica nelle preferenze è valida ed appartiene a $postOwnersSlug")
                var email = ""
                if (postOwners != null) {
                    if (postOwners.isNotEmpty()) {
                        if (postOwnersSlug!!.isNotEmpty()) {
                            val gruppo = postOwners.first()
                            val gruppoSlug = postOwnersSlug.first()

                            Log.v("NOTIFICHE", "gruppo = " + gruppo)
                            Log.v("NOTIFICHE", "gruppoSlug = " + gruppoSlug)

                            val parti = username.split("@")

                            if (parti.size == 2) {
                                Log.v("NOTIFICHE", "trovate 2 parti nella mail $username")
                                val primaParte = parti.first()
                                if (!primaParte.contains("+")) {
                                    Log.v("NOTIFICHE", "la mail non contiene il +")
                                    val secondaParte = parti.last()
                                    val array =
                                        arrayOf(primaParte, "+", gruppoSlug, "@", secondaParte)
                                    email = array.joinToString("")
                                    Log.v("NOTIFICHE", "nuova email = $email")
                                    username = email
                                    trackEvent(TagNames.GROUP_MAIL_CREATED_FOR_NOTIFICATION, null)
                                } else {
                                    Log.v("NOTIFICHE", "la mail già contiene il +")
                                    trackEvent(
                                        TagNames.GROUP_MAIL_NOT_CREATED_FOR_NOTIFICATION,
                                        null
                                    )
                                }
                            }
                            Log.v("NOTIFICHE", "indirizzo email = $email")
                            val properties = Properties()
                            properties["owner"] = gruppoSlug
                            properties["username"] = username
                            trackEvent(TagNames.FOUND_NOTIFICATION_ON_PREFERENCES, properties)
                            retryIfError = true
                        }
                    }
                }
            }
        }

        //PROCEDO CON LOGIN

        Log.v("NOTIFICHE", "eseguo login al server con eail $username")

        Manager.login(username, password, login_autologin_checkbox.isSelected
        ) { error, message, firstStart, userID, accounts, askPhoneConfirm ->

            if (error && message != null) {

                // LOGIN ERROR
                //todo: se presente link mostrare popup per andare a play store

                hideLoader()

                Log.v("NOTIFICHE", "errore login, NON va aggiornata app")

                if (retryIfError) {

                    Log.v("NOTIFICHE","ritento con email originale = $originale")

                    //riprovo con email originale

                    Manager.login(originale, password, login_autologin_checkbox.isSelected
                    ) { errorInner, messageInner, firstStartInner, userIDInner, accountsInner, askPhoneConfirmInner ->

                        if (errorInner && messageInner != null) {

                            Log.v("NOTIFICHE","errore anche nel secondo tentativo")
                            // LOGIN ERROR
                            //todo: se presente link mostrare popup per andare a play store
                            hideLoader()
                            onLoginError(messageInner)
                        } else {
                            Log.v("NOTIFICHE","successo al secondo tentativo")
                            onLoginSuccess(accounts, username, password, userIDInner!!, firstStartInner, false, askPhoneConfirmInner)
                        }
                    }

                } else {
                    Log.v("NOTIFICHE","non ritento con email originale")
                    onLoginError(message)
                }

            } else {
                Log.v("NOTIFICHE","successo al primo tentativo")
                onLoginSuccess(accounts, username, password, userID, firstStart,  true, askPhoneConfirm)
            }
        }
    }

    fun onUpdateNeed() {
        MaterialDialog(this).show {
            title(text = "Attenzione")
            icon(R.mipmap.ic_launcher)
            message(text = "Per proseguire devi aggiornare Salogram")
            positiveButton(0, "Aggiorna") {
                goToPlayStore()
            }
            negativeButton(0, "No") {
                // Do something
            }
        }
    }

    fun onLoginError(message : String) {
        askQuestion(
            getString(R.string.errore),
            message,
            getString(R.string.riprova),
            getString(R.string.annulla)
        ) { confirmed ->
            if (confirmed) loginAction(null)
            else {
                //noop
            }
        }
    }

    fun continueWithApp(accounts : JSONArray, username : String, password : String, userID : String?, primoAccesso:Boolean, showNotifica: Boolean) {
        if (accounts.length() > 1) {
            Log.v("NOTIFICHE", "è un utente multi gruppo")
            //MULTI LOGIN
            trackEvent(TagNames.MULTI_LOGIN, null)
            val lista = ArrayList<UserAccount>()
            accounts.iterator().forEach {
                val account = Gson().fromJson(it.toString(), UserAccount::class.java)
                lista.add(account)
            }

            saveAccountsInfoOnPreference(accounts)
            showGroupChooserModal(lista, password, showNotifica)

        } else {
            Log.v("NOTIFICHE", "è un utente singolo")
            if (userID != null) {
                trackEvent(TagNames.SINGLE_LOGIN, null)
                startAppLogin(userID, username, primoAccesso, true, showNotifica)
            }
        }
    }

    fun onLoginSuccess(accounts : JSONArray, username : String, password : String, userID : String?, primoAccesso:Boolean, showNotifica: Boolean, askPhoneConfirm: Boolean) {
        Log.v("NOTIFICHE", "accounts trovati " + accounts.length())

        if (accounts.length() == 0 && askPhoneConfirm) {
            Log.v("PAOLO1", "chiedo conferma del numero di telefono qui")
            askPhoneNumberConfirm(accounts, username, password, userID, primoAccesso, showNotifica, askPhoneConfirm)
        }
        else {
            Log.v("PAOLO1", "Non chiedo conferma del numero di telefono qui")
            continueWithApp(accounts, username, password, userID, primoAccesso, showNotifica)
        }
    }

    fun savePhoneNumber(phone: String, accounts : JSONArray, username : String, password : String, userID : String?, primoAccesso:Boolean, showNotifica: Boolean) {

        val timestamp = System.currentTimeMillis() / 1000

        Log.v("PAOLO1", "timestamp = $timestamp")

        val user:JSONObject = JSONObject().apply {
            put("tel_confirm_date", timestamp)
            put("tel", phone)
        }

        OneSignal.setSMSNumber(phone)

        Manager.updateProfile(user) { error, message ->

            loadingOverlay?.visibility = View.GONE

            if (error && message != null) {
                Log.v("PAOLO1", "numero di telefono NON salvato: $message")
                showAlert(getString(R.string.errore), message) {

                }
            } else {
                //ok
                Log.v("PAOLO1", "numero di telefono salvato")
                trackEvent(TagNames.PHONE_NUMBER_CONFIRMED, null)
                continueWithApp(accounts, username, password, userID, primoAccesso, showNotifica)
            }
        }

    }

    fun askPhoneNumberConfirm(accounts : JSONArray, username : String, password : String, userID : String?, primoAccesso:Boolean, showNotifica: Boolean, askPhoneConfirm: Boolean) {

        trackEvent(TagNames.PHONE_NUMBER_CONFIRM_REQUEST, null)

        var currentPhone = Utils.sharefPrefs.getString("phone", "")
        if (currentPhone == "null") currentPhone = ""

        val prefix = "+39"

        if (currentPhone != null) {
            if (!currentPhone.startsWith(prefix)) {
                currentPhone = prefix + currentPhone
            }
        } else currentPhone = prefix

        Log.v("PAOLO1", "current_phone 2 = " + currentPhone)

        var consensoDato = false
        val dialogBehavior = ModalDialog
        val dialog = MaterialDialog(this, dialogBehavior).show {
            title(0, "Conferma o aggiungi il tuo numero di telefono")
            icon(R.mipmap.ic_launcher)
            noAutoDismiss()

            customView(R.layout.phone_number_request_view, scrollable = true, horizontalPadding = true)
            positiveButton(0, "Conferma") { dialog ->
                val phoneInput: EditText = dialog.getCustomView().findViewById(R.id.phone_number)
                val phone = phoneInput.text.toString().trim().lowercase(Locale.getDefault())
                Log.v("PAOLO1", "cliccato su OK, phone = $phone")

                if (phone.length < 10) {
                    Log.v("PAOLO1", "non proseguo 1")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }

                /*
                if (phone == "+39") {
                    Log.v("PAOLO1", "non proseguo 2")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }*/

                if (!phone.isValidPhone()) {
                    Log.v("PAOLO1", "non proseguo 3")
                    showAdvice("Inserisci un numero di telefono valido")
                    return@positiveButton
                }

                if (!phone.startsWith("+39")) {
                    Log.v("PAOLO1", "non proseguo 4 ($phone)")
                    showAdvice("Il numero deve iniziare con il prefisso italiano +39")
                    return@positiveButton
                }

                if (!consensoDato) {
                    Log.v("PAOLO1", "non proseguo 5")
                    showAdvice("Devi dare il consenso per proseguire")
                    return@positiveButton

                } else {
                    Log.v("PAOLO1", "proseguo")
                    dismiss()
                    //savePhoneNumber(phone, userID, username, primoAccesso, autologin, showNotifica)
                    savePhoneNumber(phone, accounts, username, password, userID, primoAccesso, showNotifica)
                }
            }
            negativeButton(android.R.string.cancel) {
                    dialog ->
                    dismiss()
                    continueWithApp(accounts, username, password, userID, primoAccesso, showNotifica)
                    return@negativeButton
            }
            //lifecycleOwner(this@LoginActivity)
            //debugMode(debugMode)
        }

        val customView = dialog.getCustomView()
        val privacyPhone : Button = customView.findViewById(R.id.privacy_phone)
        privacyPhone.setOnClickListener {
            Log.v("PAOLO1", "cliccato")
            val intent = Intent(this, PrivacyActivity::class.java).apply {
                setFlags(intent)
                putExtra("url","https://www.iubenda.com/privacy-policy/75745741")
            }
            startActivity(intent)
        }
        val phoneNumberInput: EditText = customView.findViewById(R.id.phone_number)
        val phoneNumberConsent: CheckBox = customView.findViewById(R.id.consenso)
        phoneNumberConsent.setOnCheckedChangeListener { _, isChecked ->
            consensoDato = isChecked
        }

        /*
        MaterialDialog(this).show {
            input(waitForPositiveButton = true, prefill = currentPhone, inputType = InputType.TYPE_CLASS_PHONE, maxLength = 15) { dialog, text ->
                val inputField = dialog.getInputField()
                val isValid = text.isNumeric()

                inputField.error = if (isValid) null else "Inserisci solo numeri, senza spazi e divisori"
                dialog.setActionButtonEnabled(WhichButton.POSITIVE, isValid)

                val phone = text.toString().trim().lowercase(Locale.getDefault())
                Log.v("PAOLO1", "phone = " + phone)

                savePhoneNumber(phone, accounts, username, password, userID, primoAccesso, showNotifica)
            }
            title(0, "Conferma o aggiungi il tuo numero di telefono")
            icon(R.mipmap.ic_launcher)
            //message(text = "")
            checkBoxPrompt(R.string.phone_number_advice_privacy) { checked ->
                Log.v("PAOLO1", "checcato")
            }
            positiveButton(R.string.confirm)
        }
        */
    }

    private fun showGroupChooserModal(
        lista: ArrayList<UserAccount>,
        password: String,
        showNotifica:Boolean
    ) {
        val dialog = MaterialDialog(this).show {
            customView(R.layout.group_account_chooser)
        }

        val adapter: RecyclerView.Adapter<GroupAdapter.GroupHolder> =
            GroupAdapter(R.layout.list_item_group, lista) {
                Log.v("NOTIFICHE", "scelto gruppo $it")
                val gruppo = lista[it]
                Log.v("NOTIFICHE", "gruppo ${gruppo.goupName}")
                Log.v("NOTIFICHE", "groupMail ${gruppo.groupMail}")
                dialog.dismiss()
                val properties = Properties()
                properties["chosen_group"] = gruppo.goupName
                trackEvent(TagNames.START_GROUP_CHOICE_MODAL_CLOSE, properties)
                doGroupLogin(gruppo.groupMail, password, showNotifica)
            }

        dialog.customListAdapter(adapter)
        trackEvent(TagNames.START_GROUP_CHOICE_MODAL_OPEN, null)
    }

    private fun saveAccountsInfoOnPreference(accounts: JSONArray) {
        if (!Utils.sharefPrefs.contains(EditorMainFragment.USER_ACCOUNTS)) {
            Utils.sharefPrefsEditor.remove(EditorMainFragment.USER_ACCOUNTS).apply()
        }

        Utils.sharefPrefsEditor.apply {
            putJSONArray(EditorMainFragment.USER_ACCOUNTS, accounts)
        }
    }

    private fun goToPlayStore() {
        val packageName = "it.ghd.salogram"
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
        }
    }

    override fun onPause() {
        super.onPause()
    }

    fun enableAutologin(view: View) {
        login_autologin_checkbox.isSelected = !login_autologin_checkbox.isSelected
        val valore = login_autologin_checkbox.isSelected
        Log.v("NOTIFICHE", "imposto autologin a $valore")
        Utils.sharefPrefsEditor.putBoolean("autologin", valore)
    }
}

