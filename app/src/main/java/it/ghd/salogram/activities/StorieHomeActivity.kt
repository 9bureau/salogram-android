package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_stories.*
import kotlinx.android.synthetic.main.activity_stories.categorieRV
import kotlinx.android.synthetic.main.activity_stories.popolariRV
import kotlinx.android.synthetic.main.activity_stories.swipeToRefresh
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.stories_grid_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class StorieHomeActivity : MainActivity() {

    var categories = JSONArray()
    var popolari = JSONArray()
    var nuovi = JSONArray()
    var storyOFTheWeek = JSONObject()

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = $logoImageUrl")
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_stories)

        screeName = TagNames.Screen.STORIES_HOME_SCREEN
        topTitleTV.text = "STORIES"

        menuStoriesButton.isSelected = true

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL

        categorieRV.layoutManager = linearLayoutManager
        categorieRV.setHasFixedSize(true)

        val linearLayoutManagerPopolari = LinearLayoutManager(this)
        linearLayoutManagerPopolari.orientation = LinearLayoutManager.HORIZONTAL

        popolariRV.layoutManager = linearLayoutManagerPopolari
        popolariRV.setHasFixedSize(true)

        val linearLayoutManagerNuovi = LinearLayoutManager(this)
        linearLayoutManagerNuovi.orientation = LinearLayoutManager.HORIZONTAL

        nuoveRV.layoutManager = linearLayoutManagerNuovi
        nuoveRV.setHasFixedSize(true)

        swipeToRefresh.setOnRefreshListener {
            loadData(true)
            trackEvent(TagNames.STORIES_HOME_REFRESH, null)
        }

        stories_categorie_ll.visibility = if (categories.length()>0) View.VISIBLE else View.GONE
        categorieRV.visibility =  if (categories.length()>0) View.VISIBLE else View.GONE

        stories_popolari_ll.visibility = if (popolari.length()>0) View.VISIBLE else View.GONE
        popolariRV.visibility =  if (popolari.length()>0) View.VISIBLE else View.GONE

        stories_nuove_ll.visibility = if (nuovi.length()>0) View.VISIBLE else View.GONE
        nuoveRV.visibility =  if (nuovi.length()>0) View.VISIBLE else View.GONE
    }

    override fun onResume() {
        super.onResume()

        if (firstStart){
            loadData(true)
            firstStart = false
        }
    }

    fun useStoryOfTheWeek(view:View?){
        val intent = Intent(this, EditorActivity::class.java)
        intent.putExtra("POST",false)
        intent.putExtra("DATA", storyOFTheWeek)
        startActivity(intent)
        trackEvent(TagNames.TAP_ON_STORIES_WEEK_FROM_HOME, null)
        Companion.trackEvent(TagNames.TAP_ON_STORIES, null)
        Companion.trackEvent(TagNames.TAP_ON_ITEM, null)
    }

    private fun loadData(force:Boolean = false) {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getHome(force) { error, message, jsonObject ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData(force)
                    else {
                        loadingOverlay?.visibility = View.GONE
                        swipeToRefresh.isRefreshing = false
                    }
                }

            } else {

                val categoriesObj = jsonObject!!.getJSONArray("categories")

                categories = JSONArray()

                (0 until categoriesObj.length()).forEach {
                    val jo = categoriesObj.getJSONObject(it)

                    if (jo.getInt("parent") == 0 && (jo.getInt("has_children_with_stories") > 0 || jo.getJSONArray(
                            "stories"
                        ).length() > 0)
                    ) {
                        categories.put(jo)
                    }
                }

                stories_categorie_ll.visibility =
                    if (categories.length() > 0) View.VISIBLE else View.GONE
                categorieRV.visibility = if (categories.length() > 0) View.VISIBLE else View.GONE

                popolari = jsonObject.getJSONArray("popular_stories")

                stories_popolari_ll.visibility =
                    if (popolari.length() > 0) View.VISIBLE else View.GONE

                nuovi = jsonObject.getJSONArray("recent_stories")
                stories_nuove_ll.visibility = if (nuovi.length() > 0) View.VISIBLE else View.GONE

                if (jsonObject.has("story_of_the_week")) {
                    storyOFTheWeek = jsonObject.getJSONObject("story_of_the_week")
                    story_settimana_ll.visibility = View.VISIBLE
                } else {
                    story_settimana_ll.visibility = View.GONE
                }

                val adapterCategories = CategoriesRecyclerAdapter(categories, this)
                categorieRV.adapter = adapterCategories

                val adapterPopolari = PopolariRecyclerAdapter(popolari, this)
                popolariRV.adapter = adapterPopolari

                val adapterNuovi = NuoviRecyclerAdapter(nuovi, this)
                nuoveRV.adapter = adapterNuovi

                popolariRV.visibility = if (popolari.length() > 0) View.VISIBLE else View.GONE
                nuoveRV.visibility = if (nuovi.length() > 0) View.VISIBLE else View.GONE
                loadingOverlay?.visibility = View.GONE
                swipeToRefresh.isRefreshing = false
                ImageManager.loadImage(storyOFTheWeek.getString("thumb_url"), storySettimanaIB)
            }
        }
    }

    //Categories

    private class CategoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var index: Int = 0
        private var content: JSONObject? = null
        private var contents:JSONArray = JSONArray()

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, events:JSONArray, index:Int) {
            this.content = event
            this.index = index
            contents = events
            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
            //view.titleTVCell.text = event.optString("name")
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context

            val catSelected = contents.getJSONObject(index)

            val isParent = catSelected.getInt("has_children_with_stories")>0 && catSelected.getInt("parent") == 0

            if (!isParent) {

                val categoriesFiltered = JSONArray()

                (0 until contents.length()).forEach {
                    val jo = contents.getJSONObject(it)

                    if (jo.getJSONArray("stories").length()>0  && jo.getInt("parent") == 0 ) {
                        categoriesFiltered.put(jo)
                    }
                }

                Manager.storyHomeContents = categoriesFiltered

                val intent = Intent(context, StorieListActivity::class.java)
                intent.putExtra("CAT", content!!)
                // intent.putExtra("CATS",contents)
                intent.putExtra("INDEX", index)
                context.startActivity(intent)
                trackEvent(TagNames.TAP_ON_STORIES_CATEGORY_FROM_HOME, null)

            }
            else {
                //is parent

                Manager.storyHomeContents = contents

                val intent = Intent(context, StorieCategoriesListActivity::class.java)
                intent.putExtra("CAT", content!!)
                //intent.putExtra("INDEX", index)
                // intent.putExtra("CATS",contents)
                context.startActivity(intent)
                trackEvent(TagNames.TAP_ON_STORIES_CATEGORY_FROM_HOME, null)
            }
        }
    }

    //Categories
    private class CategoriesRecyclerAdapter(
        val items: JSONArray,
        val context: Context
    ) : RecyclerView.Adapter<CategoriesHolder>() {

        override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position),items,position)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
            return CategoriesHolder(
                LayoutInflater.from(context).inflate(R.layout.post_list_cell, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return items.length()
        }
    }

    //POPOLARI

    private class PopolariHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("DATA", content!!)
            intent.putExtra("POST",false)
            context.startActivity(intent)
            trackEvent(TagNames.TAP_ON_POPULAR_STORIES_FROM_HOME, null)
            trackEvent(TagNames.TAP_ON_STORIES, null)
            trackEvent(TagNames.TAP_ON_ITEM, null)
        }
    }

    private class PopolariRecyclerAdapter(val items: JSONArray, val context: Context) : RecyclerView.Adapter<PopolariHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: PopolariHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopolariHolder {
            return PopolariHolder(
                LayoutInflater.from(context).inflate(R.layout.stories_list_cell, parent, false)
            )
        }
    }

    //NUOVI

    private class NuoviHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("DATA", content!!)
            intent.putExtra("POST",false)
            context.startActivity(intent)
            trackEvent(TagNames.TAP_ON_NEW_STORIES_FROM_HOME, null)
            trackEvent(TagNames.TAP_ON_STORIES, null)
            trackEvent(TagNames.TAP_ON_ITEM, null)
        }
    }

    private class NuoviRecyclerAdapter(val items: JSONArray, val context: Context) : RecyclerView.Adapter<NuoviHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: NuoviHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position))
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NuoviHolder {
            return NuoviHolder(
                LayoutInflater.from(context).inflate(R.layout.stories_list_cell, parent, false)
            )
        }
    }
}//class