package it.ghd.salogram.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import com.segment.analytics.Properties
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.top_area_menu.*

class ForgotPasswordActivity : MainActivity() {

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_forgot_password)
        screeName = TagNames.Screen.FORGOT_PASSWORD_SCREEN

        hideKeyboard()

        if (Utils.isDebugger || Utils.isEmulator){
            //forgotPassword_emailField.setText("andrea.leganza@gmail.com")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        super.onResume()
    }

    fun resetPasswordAction(view: View?) {

        hideKeyboard()

        if ( forgotPassword_emailField.text.toString().trim().length== 0 || !forgotPassword_emailField.text.toString().isValidEmail()) {
            showAlert(getString(R.string.errore),"Email non valida"){
            }
        }
        else {
            loadingOverlay?.visibility = View.VISIBLE

            Manager.forgotPassword(forgotPassword_emailField.text.toString()) { error, message ->
                if (error && message != null) {

                    val properties = Properties().apply {
                        putValue(TagNames.PARAM_ESITO, false )
                    }

                    trackEvent(TagNames.PASSWORD_RESET, properties)

                    askQuestion(
                        getString(R.string.errore),
                        message,
                        getString(R.string.riprova),
                        getString(R.string.annulla)
                    ) { confirmed ->
                        if (confirmed) resetPasswordAction(view)
                        else {
                            loadingOverlay?.visibility = View.GONE
                        }
                    }
                } else {

                    val properties = Properties().apply {
                        putValue(TagNames.PARAM_ESITO, true )
                    }

                    trackEvent(TagNames.PASSWORD_RESET, properties)

                    showAlert(
                        Utils.APP_NAME,
                        "Abbiamo ricevuto la tua richiesta, riceverai una email contenente le procedure necessarie per effettuare il reset della password."
                    ) {
                        finish()
                    }
                }
            }
        }

    }
}
