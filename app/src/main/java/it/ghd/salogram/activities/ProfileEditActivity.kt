package it.ghd.salogram.activities

import android.util.Log
import android.view.View
import com.segment.analytics.Properties
import it.ghd.salogram.*
import it.ghd.salogram.model.Owner
import kotlinx.android.synthetic.main.activity_profilo_edit.*
import kotlinx.android.synthetic.main.loading_overlay.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class ProfileEditActivity : MainActivity() {

    lateinit var venue:JSONObject

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = $logoImageUrl")
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_profilo_edit)
        screeName = TagNames.Screen.PROFILE_EDIT_SCREEN
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    public fun hideMe(view:View){
        super.backAction(view)
    }

    private fun loadData() {
        loadingOverlay?.visibility = View.VISIBLE

        Manager.getProfileInformations { error, message, info ->
            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                loadingOverlay?.visibility = View.GONE

                if (info != null) {

                    venue = info.getJSONArray("venue").getJSONObject(0)

                    Log.v("PAOLO1", "carico dati utente")

                    var email = ""
                    if (Utils.sharefPrefs.contains("owner")) {
                        val ownerJson = Utils.sharefPrefs.getJSONObject("owner") ?: JSONObject()
                        val owner = Owner.parse(ownerJson)
                        if (owner != null) {
                            email = owner.email
                        }
                        Log.v("PAOLO1", "messa mail owner = $email")
                    }
                    if (email.isEmpty() || !email.isValidEmail()) {
                        if (info.has("email"))  {
                            email = info.optString("email")

                            if (email.contains("+")) {
                                email = Utils.getOriginalMail(email)
                                Log.v("PAOLO1", "messa mail modificata = $email")
                            }
                            else {
                                Log.v("PAOLO1", "messa mail originale = $email")
                            }
                        }
                    }

                    registration_email_field.setText(email)

                    if (info.has("first_name")) registration_nome_field.setText(info.getString("first_name"))
                    else if (info.has("name")) registration_nome_field.setText(info.getString("name"))

                    if (info.has("last_name")) registration_cognome_field.setText(info.getString("last_name"))
                    if (info.has("venue")) registration_salone_field.setText(venue.getString("name"))
                    //if (info.has("email")) registration_email_field.setText(Utils.getOriginalMail(info.getString("email")))
                    if (info.has("tel")) registration_telefono_field.setText(info.getString("tel"))
                    if (info.has("customer_code")) registration_codicecliente.setText(
                        info.getString(
                            "customer_code"
                        )
                    )
                }
            }
        }
    }

    fun saveAction(view: View) {

        val properties = Properties().apply {}
        trackEvent(TagNames.PROFILE_UPDATE, properties)

        hideKeyboard()

        registration_nome_field.setText(registration_nome_field.text.trim())

        if (registration_nome_field.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo nome obbligarorio") {

            }

            return
        }

        registration_cognome_field.setText(registration_cognome_field.text.trim())

        if (registration_cognome_field.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo cognome obbligarorio") {

            }

            return
        }

        registration_email_field.setText(registration_email_field.text.trim())
        if (registration_email_field.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo email obbligatorio") {

            }

            return
        }

        if (!registration_email_field.text.toString().isValidEmail()){

            showAlert(getString(R.string.errore),"Campo email non valido") {

            }

            return
        }

        registration_telefono_field.setText(registration_telefono_field.text.trim())

        if (!registration_telefono_field.text.toString().isValidPhone()){

            showAlert(getString(R.string.errore),"Numero di telefono in formato non valido") {

            }

            return
        }

        registration_salone_field.setText(registration_salone_field.text.trim())

        if (registration_salone_field.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Il campo nome salone deve essere compilato") {

            }

            return
        }

        updateProfile(false)
    }

    fun updateProfile(newEmail:Boolean){

        venue.put("name", registration_salone_field.text.toString())
        val venueArray = JSONArray()
        venueArray.put(venue)

        val user:JSONObject = JSONObject().apply {
            put("first_name", registration_nome_field.text.toString())
            put("last_name", registration_cognome_field.text.toString())
            put("venue", registration_salone_field.text.toString())
            put("email", registration_email_field.text.toString())
            put("tel", registration_telefono_field.text.toString())
        }

        loadingOverlay?.visibility = View.VISIBLE

        Manager.updateProfile(user) { error, message ->

            loadingOverlay?.visibility = View.GONE

            if (error && message != null) {
                showAlert(
                    getString(R.string.errore),
                    message
                ) {
                }
            } else {
                //ok
                showAlert(
                    "Tumpa",
                    "Informazioni aggiornate correttamente"
                ) {

                    finish()
                }
            }
        }
    }
}