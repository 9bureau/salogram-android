package it.ghd.salogram.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_post_list.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.post_filter_cell.view.*
import kotlinx.android.synthetic.main.post_grid_cell.view.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject

class HomeListActivity : MainActivity() {

    var posts:JSONArray = JSONArray()
    var favs:JSONArray = JSONArray()
    var cats:JSONArray = JSONArray()
    var selectedCat:JSONObject? = null
    var selectedIndex:Int = 0

    // NON USATA??

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_post_list)
        screeName = TagNames.Screen.HOME_SCREEN

        topTitleTV.text = "POST"

        menuPostButton.isSelected = true

        val gridLayoutManager = GridLayoutManager(this,2)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL

        postsRV.layoutManager = gridLayoutManager
        postsRV.setHasFixedSize(true)

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = GridLayoutManager.HORIZONTAL
        postFilterRV.layoutManager = linearLayoutManager

        cats = intent.getJSONArrayExtra("CATS")

        selectedIndex = intent.getIntExtra("INDEX",0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    fun isFaved(favID:Int): Boolean {
        for (fav:JSONObject in favs){
            if (fav.getInt("id") == favID){
                return true
            }
        }
        return false
    }

    fun selectCategory(cat:JSONObject, index:Int){
        selectedCat =  cat
        selectedIndex = index

        val categoriesAdapter = CategoriesRecyclerAdapter(cats,this,selectedCat)
        postFilterRV.adapter = categoriesAdapter

        posts = selectedCat!!.getJSONArray("posts")
        val adapterContents = PostsRecyclerAdapter(posts, this)
        postsRV.adapter = adapterContents

    }

    private fun loadData() {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getAllFavorites() { error, message, favsPostsJsonArrayPosts, _ ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                favs = favsPostsJsonArrayPosts!!

                selectedCat = cats.getJSONObject(selectedIndex)

                val categoriesAdapter = CategoriesRecyclerAdapter(cats, this, selectedCat)
                postFilterRV.adapter = categoriesAdapter

                //posts
                posts = JSONArray()

                posts = selectedCat!!.getJSONArray("posts")

                val adapterContents = PostsRecyclerAdapter(posts, this)
                postsRV.adapter = adapterContents

                loadingOverlay?.visibility = View.GONE


            }
        }


       /* Manager.getAllPosts() { error, message, jsonArrayPosts  ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                posts = jsonArrayPosts!!

                Manager.getAllFavorites() { error, message, favsPostsJsonArrayPosts, _  ->

                    if (error && message != null) {
                        askQuestion(
                            getString(R.string.errore),
                            message,
                            getString(R.string.riprova),
                            getString(R.string.annulla)
                        ) { confirmed ->
                            if (confirmed) loadData()
                            else {
                                loadingOverlay?.visibility = View.GONE
                            }
                        }

                    } else {

                        favs = favsPostsJsonArrayPosts!!

                        val adapterContents = PostsRecyclerAdapter(posts, this)
                        postsRV.adapter = adapterContents

                    }
                }
            }
        }*/
    }

    fun removeFav(fav: JSONObject){
        loadingOverlay?.visibility = View.VISIBLE

        Manager.removeFav(fav) { error, message ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {

                loadingOverlay?.visibility = View.GONE

                loadData()
            }
        }
    }

    //DATA

    private class PostsHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, context: Context, index:Int) {
            this.content = event

            ImageManager.loadImage(event.optString("thumb_url"), view.imageViewCell)
            view.titleTVCell.text = event.optString("name")

            view.fabButtonCell?.isSelected = (context as HomeListActivity).isFaved(event.getInt("id") ?: 0)
            view.fabButtonCell?.setOnClickListener {
                //remove
                Manager.removeFav((event)) { error, message ->
                    //reload data
                    (context as PreferitiActivity).removeFav(content!!, index)
                }
            }
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            val context = itemView.context
            val intent = Intent(context, EditorActivity::class.java)
            intent.putExtra("DATA", content!!)
            context.startActivity(intent)
        }
    }

    private class PostsRecyclerAdapter(
        val items: JSONArray,
        val context: Context
    ) : RecyclerView.Adapter<PostsHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: PostsHolder, position: Int) {

            holder.bindEvent(items.getJSONObject(position), context, position)

        }

        override fun getItemViewType(position: Int): Int {

            val jo = items.getJSONObject(position)

            if (jo.getString("url").contains("mp4")){
                return 1
            }

            return  0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsHolder {
            return PostsHolder(
                LayoutInflater.from(context).inflate( if (viewType==0) R.layout.post_grid_cell else R.layout.video_grid_cell, parent, false)
            )
        }
    }

    //CATEGORIES
    private class CategoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null
        private var index:Int = 0

        init {
            v.filterButton.setOnClickListener(this)
            v.setOnClickListener(this)
        }

        fun bindEvent(event: JSONObject, context: Context, index:Int) {
            this.content = event

            this.index = index

            view.filterButton.setText(event.optString("name"))

        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            (itemView.context as HomeListActivity).selectCategory(content!!,index)
        }
    }

    private class CategoriesRecyclerAdapter(
        val items: JSONArray,
        val context: Context,
        val selectedCat:JSONObject?
    ) : RecyclerView.Adapter<CategoriesHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {

            holder.bindEvent(items.getJSONObject(position), context, position)
        }

        override fun getItemViewType(position: Int): Int {
            return if (selectedCat!=null && selectedCat  == items.getJSONObject(position)) 1 else 0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
            return CategoriesHolder(
                LayoutInflater.from(context).inflate( if (viewType==1) R.layout.post_filter_cell else R.layout.post_filter_cell_unselected, parent, false)
            )
        }
    }
}