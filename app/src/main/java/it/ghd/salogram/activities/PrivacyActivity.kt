package it.ghd.salogram.activities

import android.os.Bundle
import android.util.Log
import android.webkit.WebSettings
import android.webkit.WebViewClient
import it.ghd.salogram.ImageManager
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import kotlinx.android.synthetic.main.activity_guide.*
import kotlinx.android.synthetic.main.top_area_menu.*

class PrivacyActivity : MainActivity() {

    override fun hasLogo() : Boolean {
        return false
    }

    override fun showGroupLogo() {
    }

    override fun onCreateAfterParent() {
        screeName = TagNames.Screen.PRIVACY_SCREEN
        //if (intent.getBooleanExtra("registration",false)) {
        setContentView(R.layout.activity_privacy_registration)
        //}
        /*else {
            setContentView(R.layout.activity_privacy)
        }*/

        val webSetting: WebSettings = browser.settings
        webSetting.builtInZoomControls = true
        webSetting.javaScriptEnabled = true
        browser.webViewClient = WebViewClient()

        browser.loadUrl(intent.getStringExtra("url") ?:"blank")
    }

    override fun onResume() {
        super.onResume()
    }
}
