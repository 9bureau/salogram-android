package it.ghd.salogram.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.*
import android.media.MediaScannerConnection.scanFile
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.view.allViews
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.list.customListAdapter
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.androidnetworking.interfaces.DownloadListener
import com.androidnetworking.model.Progress
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.facebook.share.Sharer
import com.facebook.share.model.*
import com.facebook.share.model.ShareStoryContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.*
import it.ghd.salogram.FileUtils
import it.ghd.salogram.adapters.MyRecyclerAdapter
import it.ghd.salogram.fragments.*
import it.ghd.salogram.model.*
import kotlinx.android.synthetic.main.activity_editor.*
import kotlinx.android.synthetic.main.activity_profilo.*
import kotlinx.android.synthetic.main.editor_bottom_sheet.*
import kotlinx.android.synthetic.main.editor_bottom_sheet.view.*
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.fragment_editor_caption_edit.*
import kotlinx.android.synthetic.main.fragment_editor_home.*
import kotlinx.android.synthetic.main.fragment_loading.loadingOverlay
import kotlinx.android.synthetic.main.loading_overlay.*
import kotlinx.android.synthetic.main.top_area_menu.*
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*
import android.os.Environment

import android.os.ParcelFileDescriptor
import androidx.activity.result.contract.ActivityResultContracts
import it.ghd.salogram.CustomApplication.Companion.context
import java.io.FileInputStream

class EditorActivity : MainActivity() {
    
    //private val FACEBOOK_PAGE_SHARE_REQUEST_CODE = 3000
    //private val LOGO_PICK_CODE_REQUEST_CODE = 1000
    private val REQUEST_WRITE_READ_PERMISSIONS = 786

    private var initialized = false
    private var textColor: Int = Color.parseColor("#FFFFFF")
    private var textToShare = ""
    private var shareDestination = ShareDestination.NONE
    lateinit var info:JSONObject

    //private var shareRequest =  false
    private var userAction = UserAction.NONE
    private var selectLogoRequest =  false

    private var ITEM_TYPE = ""
    private var MEDIA_TYPE = ""

    companion object {
        public var shareIntentStarted = false
    }

    private var callerLogoFragment : EditorLogoFragment? = null
    private val ALBUM_NAME = "Salogram" ///sdcard/DCIM/GHD
    private val APP_NAME = "salogram" ///sdcard/DCIM/GHD
    private val savedPathVideo = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + File.separator + ALBUM_NAME
    private var downloadedVideoPath = ""
    //private var media_url =""

    private var sharedVideo =  false
    private var downloadedVideo =  false

    private lateinit var currentPostItem : PostItem

    private val isAndroid6_OrMore = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    private val isAndroid9_OrMore = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
    private val isAndroid10_OrMore = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
    private val isAndroid11_OrMore = Build.VERSION.SDK_INT >= Build.VERSION_CODES.R

    private val DEFAULT_VIDEO_NAME = "$APP_NAME.mp4"
    private val DEFAULT_IMAGE_NAME = "$APP_NAME.png"

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<GridLayout>

    var callbackManager: CallbackManager? = null
    var shareDialog: ShareDialog? = null

    fun hideBottomSheet() : Boolean {

        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN;
            Log.v("AZIONI", "chiudo bottom sheet")
            return true
        }

        Log.v("AZIONI", "non chiudo bottom sheet")
        return false
    }

    override fun onBackPressed() {
        if (!hideBottomSheet()) super.onBackPressed()
        val properties = Properties()
        properties["from"] = screeName
        trackEvent(TagNames.TAP_ON_BACK_BUTTON, properties)
    }

    override fun hasLogo() : Boolean {
        return false
    }

    override fun showGroupLogo() {

    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_editor)

        screeName = TagNames.Screen.EDITOR_SCREEN

        val builder: StrictMode.VmPolicy.Builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        info = intent.getJSONObjectExtra("DATA")

        currentPostItem = Gson().fromJson(info.toString(), PostItem::class.java)

        Log.v("FACEBOOKSHARE", "info")
        Log.v("FACEBOOKSHARE", info.toString())

        //media_url = info.getString("url")

        EditorMainFragment.info = info
        EditorMainFragment.isPost = intent.getBooleanExtra("POST", false) || info.getString("type") != "story"
        EditorMainFragment.isVideo = currentPostItem.isVideo()
        //isPost = EditorMainFragment.isPost
        //isVideo = media_url.contains("mp4")

        ITEM_TYPE = currentPostItem.getItemType()
        MEDIA_TYPE = currentPostItem.getMediaType()

        //default values
        Utils.sharefPrefsEditor.apply {

            val caption = EditorMainFragment.info.getString("caption")
            putString(EditorMainFragment.CAPTION_TEXT, caption)

            putInt(EditorMainFragment.CANVAS_INDEX, -1) //-1 hiidden
            putString(EditorMainFragment.CANVAS_URL, "")
            putJSONObject(EditorMainFragment.CANVAS_INFO, JSONObject())

            putColor(EditorMainFragment.FONT_COLOR, Color.BLACK)
            putInt(EditorMainFragment.FONT_INDEX, 0)

            putFloat(EditorMainFragment.LOGO_X, 100f)
            putFloat(EditorMainFragment.LOGO_Y, 100f)
            putInt(EditorMainFragment.LOGO_W, 350)
            putInt(EditorMainFragment.LOGO_H, 350)
            putInt(EditorMainFragment.LOGO_INDEX, -1) //show  when opening the page for the first time (-1 = hidden)

            putBoolean(EditorMainFragment.MESSAGE_BOX_SHOW, false)
            putString(EditorMainFragment.MESSAGE_BOX_TEXT, "lorem ipsum")
            putFloat(EditorMainFragment.MESSAGE_BOX_CENTER_X, 450f) //300 + 1/2w
            putFloat(EditorMainFragment.MESSAGE_BOX_CENTER_Y, 400f) //200 + 1/2h
            putInt(EditorMainFragment.MESSAGE_BOX_W, 300)
            putInt(EditorMainFragment.MESSAGE_BOX_H, 200)

        }.commit()

        //create folders
        val folder: File = File(getExternalFilesDir(null), APP_NAME)
        if (!folder.exists()) {
            folder.mkdirs()
        }

        downloadedVideoPath = folder.absolutePath

        bottom_sheet_behavior_view.whatsapp.setOnClickListener {
            shareDestination = ShareDestination.WHATSAPP
            shareToSpecificAction()
        }

        bottom_sheet_behavior_view.facebook_profile.setOnClickListener{
            shareDestination = ShareDestination.FACEBOOK_PROFILE
            shareToSpecificAction()
        }

        bottom_sheet_behavior_view.facebook_page.setOnClickListener{
            shareDestination = ShareDestination.FACEBOOK_PAGE
            shareToSpecificAction()
        }

        bottom_sheet_behavior_view.instagram.setOnClickListener{
            shareDestination = ShareDestination.INSTAGRAM
            shareToSpecificAction()
        }

        bottom_sheet_behavior_view.other.setOnClickListener{
            shareDestination = ShareDestination.OTHER
            shareToSpecificAction()
        }

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_behavior_view as GridLayout)

        if (!currentPostItem.isPost()) (facebook_page.parent as ViewGroup).removeAllViews()

        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(view: View, i: Int) {
                // do something when state changes
                Log.v("PAOLO", "onStateChanged")
            }

            override fun onSlide(view: View, v: Float) {
                // do something when slide happens
                Log.v("PAOLO", "onSlide")
            }
        })

        //DEBUG
        Utils.sharefPrefs.all.map {
            //Log.v("FACEBOOKSHARE PREF", it.key + " : " + it.value)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private fun slideUpDownBottomSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            //apro
            trackEvent(TagNames.SHARE_PANEL_OPEN, null)
        } else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN;
            //chiudo
            trackEvent(TagNames.SHARE_PANEL_CLOSE, null)
        }
    }

    override fun onResume() {
        super.onResume()

        if (shareDestination != ShareDestination.NONE) {
            if (shareIntentStarted) {
                Log.v("STEPS", "onResume dopo tentativo di share su " + shareDestination)

                val eventName = when (shareDestination) {
                    ShareDestination.WHATSAPP           -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.FACEBOOK_PROFILE   -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.FACEBOOK_PAGE      -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.INSTAGRAM          -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.MESSENGER          -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.OTHER              -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.DOWNLOAD           -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.TWITTER            -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.TELEGRAM           -> TagNames.ShareAction.UNKNOWN
                    ShareDestination.NONE               -> TagNames.ShareAction.UNKNOWN
                }

                trackEvent(eventName, null)

            } else {
                Log.v("STEPS", "onResume normale")
                //whatsapp: non so se ha inviato o meno -> esito unknown

            }
            shareIntentStarted = false
        }

        properties = Properties().apply {
            putValue("media", MEDIA_TYPE)
            putValue("type", ITEM_TYPE)
            putValue("id", currentPostItem.id)
        }

        if (initialized){
            return
        }

        initialized = true;

        showOverlay()

        AndroidNetworking.get(EditorMainFragment.info.getString(if (!currentPostItem.isVideo()) "url" else "thumb_url"))
            .setTag("imageRequestTag")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsBitmap(object : BitmapRequestListener {
                override fun onResponse(bitmap: Bitmap) {
                    EditorMainFragment.image = bitmap
                    EditorMainFragment.imageIsInitialized = true

                    Log.v("FACEBOOKSHARE", "immagine scaricata")

                    val adapter = ScreenSlidePagerAdapter(
                        this@EditorActivity,
                        EditorMainFragment.info
                    )
                    editorViewPager.adapter = adapter
                    editorViewPager.isUserInputEnabled = false
                    editorViewPager.setPageTransformer { page, position ->
                        Log.v(
                            "FACEBOOKSHARE",
                            "Page ${editorViewPager.currentItem} : position: ${position}..."
                        )
                        page.alpha = 0.5f;
                        page.visibility = View.VISIBLE;
                        page.animate().setDuration(250).alpha(1f)
                    }

                    hideOverlay()

                    /*if (Utils.isDebugger ||  Utils.isEmulator) {
                        gotoTextPage(null)
                    }*/
                }

                override fun onError(error: ANError) {
                    // handle error
                    EditorMainFragment.imageIsInitialized = false
                    Log.v("FACEBOOKSHARE", "immagine NON scaricata: " + error.errorCode)
                    Log.v("FACEBOOKSHARE", "immagine NON scaricata: " + error.errorDetail)
                    Log.v("FACEBOOKSHARE", "immagine NON scaricata: " + error.response.body().toString())
                }
            })

    }

    class ScreenSlidePagerAdapter(fa: FragmentActivity, private val imageInfo: JSONObject) : FragmentStateAdapter(
        fa
    ) {
        override fun getItemCount(): Int =  7

        override fun createFragment(position: Int): Fragment = when (position) {
            0 -> if (EditorMainFragment.isPost) EditorCaptionFragment.newInstance(imageInfo) else EditorNoCaptionFragment.newInstance(
                imageInfo
            )
            1 -> EditorTextFragment.newInstance(imageInfo)
            2 -> EditorTextFontFragment.newInstance(imageInfo)
            3 -> EditorTextColorFragment.newInstance(imageInfo)
            4 -> EditorCanvasFragment.newInstance(imageInfo)
            5 -> EditorLogoFragment.newInstance(imageInfo)
            6 -> EditorCaptionEditFragment.newInstance(imageInfo)
            else -> EditorHomeFragment.newInstance(imageInfo)
        }

        override fun onBindViewHolder(
            holder: FragmentViewHolder,
            position: Int,
            payloads: MutableList<Any>
        ) {
            super.onBindViewHolder(holder, position, payloads)
            holder.setIsRecyclable(false)

            Log.v("FACEBOOKSHARE", "creo fragment a posizione " + position)
        }
        }

    fun gotoHomePage(view: View?){
        hideKeyboard()

        if (editorViewPager.currentItem!=0)
            editorViewPager.setCurrentItem(0, false)
        else
            backAction(null)
    }

    fun gotoCaptionPage(view: View){
        hideKeyboard()
        if (editorViewPager.currentItem!=0) {
            editorViewPager.setCurrentItem(0, false)
        }
    }

    fun gotoCaptionNoCaptionPage(view: View){
        hideKeyboard()
        if (editorViewPager.currentItem!=0) {
            editorViewPager.setCurrentItem(0, false)
        }
    }

    fun gotoTextPage(view: View?){

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("action", "edit_text")
        }

        trackEvent(TagNames.ACTION_EDIT_TEXT, properties)
        trackEvent(TagNames.ACTION_POST_CUSTOMIZATION, properties)

        hideKeyboard()
        if (editorViewPager.currentItem!=1) {
            editorViewPager.setCurrentItem(1, false)
        }
    }

    fun gotoTextFontPage(view: View?){
        hideKeyboard()
        if (editorViewPager.currentItem!=2) {
            editorViewPager.setCurrentItem(2, false)
        }
    }

    fun gotoTextColorPage(view: View?){
        hideKeyboard()
        if (editorViewPager.currentItem!=3) {
            editorViewPager.setCurrentItem(3, false)
        }
    }

    fun gotoCanvasPage(view: View?){

        Log.v("AZIONI", "gotoCanvasPage")

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("action", "edit_frame")
        }

        trackEvent(TagNames.ACTION_ADD_FRAME, properties)
        trackEvent(TagNames.ACTION_POST_CUSTOMIZATION, properties)

        hideKeyboard()
        if (editorViewPager.currentItem!=4) {
            editorViewPager.setCurrentItem(4, false)
        }
    }

    fun gotoLogoPage(view: View?){

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("action", "edit_logo")
        }

        trackEvent(TagNames.ACTION_ADD_LOGO, properties)
        trackEvent(TagNames.ACTION_POST_CUSTOMIZATION, properties)

        Log.v("AZIONI", "gotoLogoPage")

        hideKeyboard()
        if (editorViewPager.currentItem!=5) {
            editorViewPager.setCurrentItem(5, false)
        }
    }

    fun gotoCaptionEditPage(view: View?){

        Log.v("AZIONI", "gotoCaptionEditPage")
        hideBottomSheet()
        hideKeyboard()
        if (editorViewPager.currentItem!=6) {
            editorViewPager.setCurrentItem(6, false)
        }

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("action", "edit_copy")
        }

        trackEvent(TagNames.ACTION_EDIT_COPY, properties)
        //trackEvent(TagNames.ACTION_POST_CUSTOMIZATION, properties) NON METTERE
    }

    fun avantiAction(view: View){

        editorViewPager.setCurrentItem(
            when (editorViewPager.currentItem) {
                1 -> 4 //text  to  canvas
                4 -> 5 //canvas to logo
                5 -> 0 //logo to home
                else -> 0 //back to home
            }, false
        )
    }

    //INVOCATA DA VIEW AL CLICK
    fun shareToAction(view: View?){
        slideUpDownBottomSheet()
    }

    fun logShareActionStart() {

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("destination", shareDestination)
        }

        val eventName = when (shareDestination) {
            ShareDestination.WHATSAPP           -> TagNames.ShareAction.Start.WHATSAPP
            ShareDestination.FACEBOOK_PROFILE   -> TagNames.ShareAction.Start.FB_PROFILE
            ShareDestination.FACEBOOK_PAGE      -> TagNames.ShareAction.Start.FB_PAGE
            ShareDestination.INSTAGRAM          -> TagNames.ShareAction.Start.INSTAGRAM
            ShareDestination.MESSENGER          -> TagNames.ShareAction.Start.MESSENGER
            ShareDestination.OTHER              -> TagNames.ShareAction.Start.OTHER
            ShareDestination.DOWNLOAD           -> TagNames.ShareAction.Start.DOWNLOAD
            ShareDestination.TWITTER            -> TagNames.ShareAction.Start.TWITTER
            ShareDestination.TELEGRAM           -> TagNames.ShareAction.Start.TELEGRAM
            ShareDestination.NONE               -> TagNames.ShareAction.Start.NONE
        }

        trackEvent(TagNames.ShareAction.START, properties)
        trackEvent(eventName, properties)
        trackEvent(TagNames.EXPORT_EVENT, properties)
    }

    //fun shareToSpecificAction(view: View?){
    fun shareToSpecificAction(){

        Log.v("STEPS", "1) shareToSpecificAction")

        logShareActionStart()

        showOverlay()

        //shareRequest = true
        userAction = UserAction.SHARE

        // se android maggiore di 23
        if (isAndroid6_OrMore) {

            Log.v("STEPS", "2) chiedo permessi, android >= M")

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //se android maggiore o uguale a 6
                chiediPermessiStorage()
            }

        } else {
            //android sotto alla versione 6

            Log.v("STEPS", "1a) shareToAction")

            //VIDEO SHARE
            if (currentPostItem.isVideo()){
                condividiVideo()
                return
            }
            else if (userAction == UserAction.DOWNLOAD) { //(!shareRequest)
                val filename = generateRandomFileName("png")
                Log.v("STEPS", "2) filename = " + filename)
                downloadActionOkPermissions(filename)
                showAdvice("Errore durante il salvataggio nella galleria")
            }
            else {
                Log.v("STEPS", "1c) condivido file")
                showOverlay()
                shareImageOkPermissions(DEFAULT_IMAGE_NAME)
            }
        }
    }

    private fun generateRandomFileName(ext: String) : String {
        return "${System.currentTimeMillis()}.$ext"
    }

    private fun condividiVideo() {
        copyTextAction(null)

        Log.v("STEPS", "1b) condivido video")

        if (sharedVideo){
            hideOverlay()
            shareVideoUri(uriForVideo(DEFAULT_VIDEO_NAME))
        }
        else {
            shareVideoOkPermissions(
                this, currentPostItem.url, DEFAULT_VIDEO_NAME
            ) { error, message, uri ->
                if (!error) {
                    hideOverlay()
                    sharedVideo = true
                    shareVideoUri(uri)
                } else if (message != null) {
                    hideOverlay()
                    chiediConferma(message)
                }
            }
        }
    }

    private fun chiediConferma(message: String?) {
        askQuestion(
            getString(R.string.errore),
            message,
            getString(R.string.riprova),
            getString(R.string.annulla)
        ) { confirmed ->
            if (confirmed) shareToSpecificAction()
            else {
                hideOverlay()
            }
        }
    }

    private fun shareImageOkPermissions(filename: String){

        downloadActionOkPermissions(filename)?.let {
            shareImage(it)
        }
        ?: run {
            //something went wrong
            //showAdvice("Errore durante il salvataggio nella galleria")
        }
    }

    private fun startShareProcedure(uri: Uri) : Boolean {

        val testoToShare = getCaptionTextFromPrefs()
        val shareIntent = createShareIntent(uri, testoToShare)

        Log.v("STEPS", "19a) shareDestination = " + shareDestination)

        if (shareIntent != null &&
            shareDestination != ShareDestination.DOWNLOAD &&
            shareDestination != ShareDestination.FACEBOOK_PAGE &&
            shareDestination != ShareDestination.FACEBOOK_PROFILE) {

            Log.v("STEPS", "19) share intent valido")

            // Checking whether chosen app is installed or not
            if (shareIntent.resolveActivity(packageManager) == null) {
                showAdvice("Devi avere l'app installata nel tuo dispositivo")

                endShareProcedure(TagNames.ShareAction.FAIL, "Applicazione non presente nel dispositivo utente")
                val properties1 = Properties()
                properties1["step"] = 2
                trackEvent(TagNames.ShareAction.MISSING_APP_ERROR, properties1)
                Log.v("STEPS", "19b) torno falso")
                return false //TODO va bene?
            }

            Log.v("STEPS", "20) creo chooser")
            //creo chooser

            val receiver =  Intent(this, MyBroadCastReceiver::class.java)
            val pendingIntent =  PendingIntent.getBroadcast(
                this, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT
            )

            val chooser = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                Intent.createChooser(shareIntent, "Condividi su...", pendingIntent.intentSender)
            } else {
                Intent.createChooser(shareIntent, "Condividi su...")
            }

            Log.v("STEPS", "21) mostro chooser")
            //avvio chooser
            startChooser(shareIntent, chooser)
            return true
        }

        Log.v("STEPS", "21b) torno falso")
        return false
    }

    override fun onStop() {
        super.onStop()
        //unregisterReceiver(shareDataReceiver)
    }

    class MyBroadCastReceiver : BroadcastReceiver()  {
        override fun onReceive(context: Context, intent: Intent) {

            //super.onReceive(context, intent);
            Log.v("IntentDataReceiver", "Received broadcast")
            val dati = intent.extras

            if (dati != null) {
                for (key in dati.keySet()) {
                    Log.v("STEPS", "$key is a key in the bundle")
                    val value = dati.getParcelable<ComponentName>(key)
                    val packageName = value!!.packageName
                    Log.v("STEPS", "packageName = " + value!!.packageName)

                    val appName = when (packageName) {
                        "com.twitter.android" -> "Twitter"
                        "org.telegram.messenger" -> "Telegram"
                        "com.instagram.android" -> "Instagram"
                        "com.whatsapp" -> "Whatsapp"
                        "com.facebook.orca" -> "Messenger"
                        else -> "Altro"
                    }

                    if (packageName == "org.telegram.messenger") {

                    }

                    val endEvent = when (packageName) {
                        "com.twitter.android" -> TagNames.ShareAction.End.Unknown.TWITTER
                        "org.telegram.messenger" -> TagNames.ShareAction.End.Unknown.TELEGRAM
                        "com.instagram.android" -> TagNames.ShareAction.End.Unknown.INSTAGRAM
                        "com.whatsapp" -> TagNames.ShareAction.End.Unknown.WHATSAPP
                        "com.facebook.orca" -> TagNames.ShareAction.End.Unknown.MESSENGER
                        else -> TagNames.ShareAction.End.Unknown.OTHER
                    }

                    Log.v("STEPS", "appName = " + appName)

                    //endShareProcedure(TagNames.ShareAction.END, null)
                    val properties = resumeProperties()

                    Log.v("STEPS", "properties = " + properties.toString())

                    shareIntentStarted = true

                    trackEvent(TagNames.ShareAction.END, properties)
                    trackEvent(endEvent, properties)

                    //TODO: chiamare recordShareToServer qui
                }
            }
        }

        private fun resumeProperties(): Properties? {

            if (Utils.sharefPrefs.contains(TagNames.SHARE_DATA_KEY)) {

                val shareData = Utils.sharefPrefs.getJSONObject(TagNames.SHARE_DATA_KEY)

                if (shareData.has("type") && shareData.has("media") && shareData.has("id") && shareData.has(
                        "destination"
                    )
                ) {
                    val p = Properties().apply {
                        putValue("type", shareData.getString("type"))
                        putValue("media", shareData.getString("media"))
                        putValue("id", shareData.getString("id"))
                        putValue("destination", shareData.getString("destination"))
                    }

                    Log.v("IntentDataReceiver", "riesumate properties")

                    Utils.sharefPrefs.edit().apply {
                        remove(TagNames.SHARE_DATA_KEY)
                    }.apply()

                    return p
                }
            }

            Log.v("IntentDataReceiver", "non riesumate properties")
            return null
        }
    }

    // SEMBRA ESSERE USATO SOLO SU ANDROID < 10
    private fun shareImage(uri: Uri) {
        Log.v("STEPS", "7) shareImage uno, tipo = " + shareDestination.name);
        showOverlay()
        copyTextAction(null)
        //val shareIntent = IntentGenerator.getGenericIntentNoClip(uri, "testo", isVideo)
        startShareProcedure(uri)
    }

    private fun showOverlay() {
        loadingOverlay?.visibility = View.VISIBLE
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.v("STEPS", "3) permessi di scaricamento ottenuti")

        if (requestCode == REQUEST_WRITE_READ_PERMISSIONS && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (selectLogoRequest) {
                Log.v("STEPS", "4) devo chiedere permessi per scelta Logo")
                selectLogoOkPermissions()
            }
            else {
                Log.v("STEPS", "4) non è richiesta scelta Logo")

                if (currentPostItem.isVideo()){
                    handleVideoShare()
                    Log.v("STEPS", "5) è un video")

                    //TEMP
                    //val filename =
                    //saveVideoToStorage(filename, "video.mp4")
                }
                //else if (!shareRequest) {
                else if (userAction == UserAction.DOWNLOAD) {
                    Log.v("STEPS", "5) non è un video e non è una condivisione")

                    val filename = generateRandomFileName("png")
                    downloadActionOkPermissions(filename)
                } else {
                    Log.v("STEPS", "5) è richiesta condivisione e non è video")
                    shareImageOkPermissions(DEFAULT_IMAGE_NAME)
                }
            }
        }
        else {
            hideOverlay()
            showAdvice("Non è possibile condividere i contenuti senza aver autorizzato l'accesso in lettura e scrittura al dispositivo")
        }
    }

    private fun handleVideoShare() {
        if (userAction == UserAction.SHARE){

            Log.v("STEPS", "6) richiesta condivisione video")

            //Log.v("FACEBOOKPAOLO","[handleVideoShare] chiamo recordShareToServer 1")
            //recordShareToServer(null) TOLTO 13 ottobre 2021
            copyTextAction(null)

            if (sharedVideo){
                Log.v("STEPS", "6) video condiviso")
                hideOverlay()
                shareVideoUri(uriForVideo(DEFAULT_VIDEO_NAME))
            }
            else {

                shareVideoOkPermissions(this, currentPostItem.url, DEFAULT_VIDEO_NAME) { error, message, uri ->

                    Log.v("STEPS", "8) video scaricato")

                    if (!error) {
                        Log.v("STEPS", "9) nessun errore")
                        hideOverlay()
                        sharedVideo = true
                        shareVideoUri(uri)
                    } else if (message != null) {

                        Log.v("STEPS", "9) messaggio di errore")

                        askQuestion(
                            getString(R.string.errore),
                            message,
                            getString(R.string.riprova),
                            getString(R.string.annulla)
                        ) { confirmed ->
                            if (confirmed) shareToAction(null)
                            else {
                                hideOverlay()
                            }
                        }
                    }
                }
            }
            return
        } //share request
        else {

            Log.v("FACEBOOKPAOLO","[handleVideoShare] chiamo recordShareToServer 2")
            recordShareToServer(null)

            //download
            val filename = generateRandomFileName("mp4")

            downloadVideoOkPermissions(this, currentPostItem.url, filename) { error, message, uri ->
                if (!error){
                    hideOverlay()
                    showMediaSavedAdvice()
                }
                else if (message!=null) {
                    askQuestion(
                        getString(R.string.errore),
                        message,
                        getString(R.string.riprova),
                        getString(
                            R.string.annulla
                        )
                    ) { confirmed ->
                        if (confirmed) downloadAction(null)
                        else {
                            hideOverlay()
                        }
                    }
                }
            }
        }
    }

    private fun facebookShareStoryVideoByUri(uri: Uri?, messaggio: String) {
        if (uri == null) return

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        Log.v("FACEBOOKSHARE", "facebookSharePostVideoByUri: " + uri.path)

        val video = ShareVideo.Builder().setLocalUrl(uri).build()

        val videoStoryContent = ShareStoryContent.Builder()
            .setBackgroundAsset(video)
            .setShareHashtag(ShareHashtag.Builder().setHashtag(messaggio).build())
            .build()

        callbackManager = CallbackManager.Factory.create()

        if (ShareDialog.canShow(ShareStoryContent::class.java)) {
            val shareDialog = ShareDialog(this)

            // this part is optional
            shareDialog.registerCallback(
                callbackManager, object : FacebookCallback<Sharer.Result> {
                    override fun onSuccess(result: Sharer.Result?) {

                        showFacebookProfileShareCompleteAdvice {
                            endShareProcedure(TagNames.ShareAction.END, null)
                            val properties = Properties()
                            properties["type"] = ITEM_TYPE
                            properties["media"] = MEDIA_TYPE
                            trackEvent(TagNames.ShareAction.End.Success.FB_PROFILE, properties)
                            Log.v("FACEBOOKPAOLO", "[onSuccess] chiamo recordShareToServer")
                            recordShareToServer(null)
                            }
                    }

                    override fun onCancel() {
                        endShareProcedure(TagNames.ShareAction.CANCEL, "Annullata dall'utente")
                        trackEvent(TagNames.ShareAction.End.Cancel.FB_PROFILE, null)
                    }

                    override fun onError(error: FacebookException?) {
                        endShareProcedure(TagNames.ShareAction.FAIL, error?.localizedMessage)
                        trackEvent(TagNames.ShareAction.End.Fail.FB_PROFILE, null)
                    }
                })

            shareDialog.show(videoStoryContent)
        }
    }

    private fun showFacebookProfileShareCompleteAdvice(listener: (Boolean) -> Unit) {
        val dialog = MaterialDialog(this)
            .cancelable(false)  // calls setCancelable on the underlying dialog
            .cancelOnTouchOutside(false)
            .customView(R.layout.progress_loader)

        val prefix = if (currentPostItem.isPost()) "Post pubblicato" else "Storia pubblicata"
        val frase = "$prefix correttamente sul tuo profilo Facebook!"

        dialog.getCustomView().findViewById<ProgressBar>(R.id.progress_bar_wait).visibility = View.GONE
        dialog.getCustomView().findViewById<ImageView>(R.id.progress_check_done).visibility = View.VISIBLE
        dialog.getCustomView().findViewById<TextView>(R.id.progress_text_wait).text = frase
        dialog.show()

        val handler = Handler()
        handler.postDelayed({
            dialog.dismiss()
            listener(true)
        }, 2500)
    }

    private fun facebookSharePostVideoByUri(uri: Uri?, messaggio: String) {

        if (uri == null) return

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        Log.v("FACEBOOKPAOLO", "[FbSharePostVideoByUri] " + uri.path)

        val video = ShareVideo.Builder().setLocalUrl(uri).build()

        val videocontent = ShareVideoContent.Builder()
            .setVideo(video)
            //.setContentTitle("titolo")
            //.setContentDescription("descrizione")
            .setShareHashtag(ShareHashtag.Builder().setHashtag(messaggio).build())
            .build()

        callbackManager = CallbackManager.Factory.create()

        if (ShareDialog.canShow(ShareVideoContent::class.java)) {

            Log.v("FACEBOOKPAOLO","[FbSharePostVideoByUri] share dialog CAN share")

            val shareDialog = ShareDialog(this)

            shareDialog.registerCallback(
                callbackManager, object : FacebookCallback<Sharer.Result> {

                    override fun onSuccess(result: Sharer.Result?) {
                        showFacebookProfileShareCompleteAdvice {
                            endShareProcedure(TagNames.ShareAction.END, null)
                            trackEvent(TagNames.ShareAction.End.Success.FB_PROFILE, null)
                            Log.v("FACEBOOKPAOLO","[FbSharePostVideoByUri][onSuccess] chiamo recordShareToServer")
                            recordShareToServer(null)
                        }
                    }

                    override fun onCancel() {
                        endShareProcedure(TagNames.ShareAction.CANCEL, "Annullata dall'utente")
                        Log.v("FACEBOOKPAOLO","[FbSharePostVideoByUri][onCancel]")
                        trackEvent(TagNames.ShareAction.End.Cancel.FB_PROFILE, null)
                    }

                    override fun onError(error: FacebookException?) {
                        endShareProcedure(TagNames.ShareAction.FAIL, error?.localizedMessage)
                        Log.v("FACEBOOKPAOLO","[FbSharePostVideoByUri][onError]")
                        trackEvent(TagNames.ShareAction.End.Fail.FB_PROFILE, null)
                    }

                })

            shareDialog.show(videocontent)
        } else {
            Log.v("FACEBOOKPAOLO","[FbSharePostVideoByUri] share dialog CANNOT share")
        }
    }

    private fun decodeBitmap(uri: Uri) : Bitmap {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) { // isAndroid9_OrMore
            return ImageDecoder.decodeBitmap(
                ImageDecoder.createSource(baseContext.contentResolver, uri)
            )
        }
        else {
            return MediaStore.Images.Media.getBitmap(baseContext.contentResolver, uri)
        }
    }

    private fun facebookShareStoryImageByUri(uri: Uri?, messaggio: String) {

        if (uri == null) return

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        Log.v("FACEBOOKSHARE", "facebookShareStoryImageByUri: " + uri.path);

        val image = decodeBitmap(uri)
        val photo = SharePhoto.Builder().setBitmap(image).build()

        // Add to ShareStoryContent
        val photoStoryContent = ShareStoryContent.Builder()
            .setBackgroundAsset(photo)
            .setShareHashtag(ShareHashtag.Builder().setHashtag(messaggio).build())
            .build();

        callbackManager = CallbackManager.Factory.create()

        if (ShareDialog.canShow(ShareStoryContent::class.java)) {
            val shareDialog = ShareDialog(this)

            // this part is optional
            shareDialog.registerCallback(
                callbackManager, object : FacebookCallback<Sharer.Result> {
                    override fun onSuccess(result: Sharer.Result?) {
                        showFacebookProfileShareCompleteAdvice {
                            endShareProcedure(TagNames.ShareAction.END, null)
                            Log.v("FACEBOOKPAOLO", "[onSuccess] chiamo recordShareToServer")
                            recordShareToServer(null)
                            trackEvent(TagNames.ShareAction.End.Success.FB_PROFILE, null)
}
                    }

                    override fun onCancel() {
                        endShareProcedure(TagNames.ShareAction.CANCEL, "Annullata dall'utente")
                        trackEvent(TagNames.ShareAction.End.Cancel.FB_PROFILE, null)
                    }

                    override fun onError(error: FacebookException?) {
                        endShareProcedure(TagNames.ShareAction.FAIL, error?.localizedMessage)
                        trackEvent(TagNames.ShareAction.End.Fail.FB_PROFILE, null)
                    }

                })

            shareDialog.show(photoStoryContent)
        }
    }

    private fun facebookSharePostImageByUri(uri: Uri?, messaggio: String) {

        if (uri == null) return

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        Log.v("FACEBOOKSHARE", "facebookSharePostImageByUri: " + uri.path);

        val image = decodeBitmap(uri)

        //photo
        //val image : Bitmap
        val photo = SharePhoto.Builder().setBitmap(image).build()

        val photoPostContent = SharePhotoContent.Builder()
            .addPhoto(photo)
            .setShareHashtag(ShareHashtag.Builder().setHashtag(messaggio).build())
            .build()

        callbackManager = CallbackManager.Factory.create()

        if (ShareDialog.canShow(SharePhotoContent::class.java)) {
            val shareDialog = ShareDialog(this)

            // this part is optional
            shareDialog.registerCallback(
                callbackManager, object : FacebookCallback<Sharer.Result> {
                    override fun onSuccess(result: Sharer.Result?) {
                        showFacebookProfileShareCompleteAdvice {
                            endShareProcedure(TagNames.ShareAction.END, null)
                            trackEvent(TagNames.ShareAction.End.Success.FB_PROFILE, null)
                            Log.v("FACEBOOKPAOLO", "[onSuccess] chiamo recordShareToServer")
                            recordShareToServer(null)
                        }
                    }

                    override fun onCancel() {
                        endShareProcedure(TagNames.ShareAction.CANCEL, "Annullata dall'utente")
                        trackEvent(TagNames.ShareAction.End.Cancel.FB_PROFILE, null)
                    }

                    override fun onError(error: FacebookException?) {
                        endShareProcedure(TagNames.ShareAction.FAIL, error?.localizedMessage)
                        trackEvent(TagNames.ShareAction.End.Fail.FB_PROFILE, null)
                    }

                })

            shareDialog.show(photoPostContent)
        }
    }

    private fun endShareProcedure(azione: String, errore: String?) {
        hideOverlay()
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
            putValue("destination", shareDestination)
            if (errore != null) putValue("error", errore)
        }

        trackEvent(azione, properties)

        Log.v("FACEBOOKPAOLO SHARE", "[$azione] ")
        if (errore != null) Log.v("FACEBOOKPAOLO ERRORE", "[$errore] ")
    }

    private fun getCanvasInfo(): JSONObject? {
        var canvasInfo: JSONObject? = null

        if (Utils.sharefPrefs.getInt(EditorMainFragment.CANVAS_INDEX, -1) >= 0) {
            canvasInfo = Utils.sharefPrefs.getJSONObject(EditorMainFragment.CANVAS_INFO)
        }
        return canvasInfo
    }

    private fun shareVideoUri(uri: Uri?) {
        if (uri == null) return

        Log.v("FACEBOOKPAOLO", "shareVideoUri: " + uri.path)

        /*val content = ContentValues(4)
        content.put(MediaStore.Video.VideoColumns.DATE_ADDED, System.currentTimeMillis() / 1000)
        content.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")

        val savedPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + File.separator + ALBUM_NAME+File.separator+"GHD.mp4"

        content.put(MediaStore.Video.Media.DATA, savedPath)
        val resolver = baseContext.contentResolver
        val customURI = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, content)
        */

        //Log.v("STEPS", "10) share Video Uri")

        //Togliere qui.
        // non è detto che sia partita

        if (shareDestination != ShareDestination.FACEBOOK_PROFILE && shareDestination != ShareDestination.FACEBOOK_PAGE) {
            Log.v("FACEBOOKPAOLO", "registro share")
            recordShareToServer(null)
        } else {
            Log.v("FACEBOOKPAOLO", "non registro share")
        }

        val testoToShare = getCaptionTextFromPrefs()

        //val shareIntent = condividiAdAltro(uri)
        val shareIntent = createShareIntent(uri, testoToShare)
        if (shareIntent != null)  {
            Log.v("STEPS", "intento non è nullo")
            avviaCondivisione(shareIntent)
        } else {
            Log.v("STEPS", "intento è nullo")
        }
    }

    private fun getCaptionTextFromPrefs() : String {
        return Utils.sharefPrefs.getString(EditorMainFragment.CAPTION_TEXT, "")!!
    }

    private fun recordShareToServer(extra: String?) {
        val canvasInfo: JSONObject? = getCanvasInfo()

        //third  parameters is social (silent failure)
        if (extra != null) {
            info.put("extra", extra)
            Log.v("FACEBOOKPAOLO", "[recordShareToServer] aggiungo extra: $extra")
        }
        else {
            Log.v("FACEBOOKPAOLO", "[recordShareToServer] non aggiungo extra");
        }

        Manager.recordShare(info, canvasInfo,
            shareDestination.name.lowercase(Locale.ROOT)
        ) { error, message ->
            Utils.log("FAILED RECORDING SHARE $error : $message")
        }
    }

    private fun avviaCondivisione(shareIntent: Intent) {

        val activities = packageManager.queryIntentActivities(shareIntent, 0)

        Log.v("FACEBOOKPAOLO", "trovate ${activities.size} schermate")

        if (activities.size > 0) {
            val chooser = Intent.createChooser(shareIntent, "Condividi su...")
            if (shareIntent.resolveActivity(packageManager) != null) {
                startActivity(chooser)
            }
        } else {
            showAdvice("Nessuna applicazione disponibile")
            val properties1 = Properties()
            properties1["step"] = 3
            trackEvent(TagNames.ShareAction.MISSING_APP_ERROR, properties1)
        }
    }

    private fun condividiAdAltro(uri: Uri?): Intent {

        Log.v("STEPS", "11) condividiAdAltro")

        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "video/mp4"
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        //shareIntent.putExtra("com.facebook.platform.extra.APPLICATION_ID", "205622954020176");
        shareIntent.putExtra(Intent.EXTRA_TEXT, getCaptionTextFromPrefs())
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        shareIntent.putExtra("content_url", uri!!.path);

        val resInfoList: List<ResolveInfo> = packageManager.queryIntentActivities(
            intent,
            PackageManager.MATCH_DEFAULT_ONLY
        )
        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            grantUriPermission(
                packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }
        return shareIntent
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)
    }

        /*
        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)

            Log.v("STEPS", "onActivityResult")

            Log.v("STEPS", "resultCode = $resultCode")

                if (requestCode == LOGO_PICK_CODE_REQUEST_CODE) {

                    if (resultCode == Activity.RESULT_OK) {
                        Log.v("STEPS", "onActivityResult 1")
                        val imageUri = data!!.data
                        val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                        EditorMainFragment.logoImage = bitmap
                        callerLogoFragment?.updateLogo(bitmap)
                        callerLogoFragment = null
                    }
                    else {

                    }
                }
                else if (requestCode == FACEBOOK_PAGE_SHARE_REQUEST_CODE) {

                    Log.v("STEPS", "FACEBOOK_PAGE_SHARE_REQUEST_CODE")

                    if (resultCode == Activity.RESULT_OK) {

                        Log.v("STEPS", "RESULT_OK")

                        if (data != null && data.extras != null) {

                            Log.v("FACEBOOKPAOLO", "dentro")
                            val messaggio = data.extras!!.getString("messaggio")!!

                            if (data.extras!!.containsKey("file")) {
                                val file = data.extras!!.getSerializable("file") as File
                                sharePhotoToFacebookPageConfirmed(file, messaggio)
                            } else {
                                val video_url = data.extras!!.getSerializable("video_url") as String
                                Log.v("FACEBOOKPAOLO", "video_url qui = " + video_url)
                                shareVideoToFacebookPageConfirmed(video_url, messaggio)
                            }
                        }

                    } else {
                        Log.v("STEPS", "RESULT KO ")
                        hideOverlay()
                        //TODO controllare
                        trackEvent(TagNames.ShareAction.End.Cancel.FB_PAGE, null)
                    }
            }
            else {
                callbackManager?.onActivityResult(requestCode, resultCode, data)
            }
        }
    */

    private fun shareVideoToFacebookPageConfirmed(mediaurl: String, messaggio: String) {
        Log.v("FACEBOOKPAOLO","shareToFacebookPageConfirmed èVideo")

        //mostra loader
        val dialog = MaterialDialog(this)
           // .message(0, "Pubblicazione in corso...")
            .cancelable(false)  // calls setCancelable on the underlying dialog
            .cancelOnTouchOutside(false)
            .customView(R.layout.progress_loader)
        dialog.show()

        Manager.shareToFacebookPageByURL(mediaurl, messaggio) { error1, msg1, postID1 ->
            //nascondi loader
            if (error1) {
                Log.v("FACEBOOKPAOLO", "post non pubblicato: $msg1")
                if (msg1 != null) {
                    MaterialDialog(this).show {
                        icon(R.drawable.baseline_warning_18)
                        title(R.string.warning)
                        message(R.string.page_publish_error)
                        positiveButton(0, "Vai alla pagina Profilo") {
                            val intent = Intent(context, ProfileActivity::class.java).apply {
                                setFlags(intent)
                                putExtra("scrollbottom", true)
                            }
                            startActivity(intent)
                        }
                        negativeButton(0, "Ok") {

                        }
                    }
                }

                endShareProcedure(TagNames.ShareAction.FAIL, msg1)
                trackEvent(TagNames.ShareAction.End.Fail.FB_PAGE, null)

            } else {
                Log.v("FACEBOOKPAOLO", "post pubblicato: $msg1")
                val tipologia = ITEM_TYPE.capitalize()

                val verbo  = if (currentPostItem.isPost()) "pubblicato" else "pubblicata"
                //showAdvice("$tipologia $verbo correttamente nella pagina Facebook")

                val frase = "$tipologia $verbo correttamente nella pagina Facebook!"

                dialog.getCustomView().findViewById<ProgressBar>(R.id.progress_bar_wait).visibility = View.GONE
                dialog.getCustomView().findViewById<ImageView>(R.id.progress_check_done).visibility = View.VISIBLE
                dialog.getCustomView().findViewById<TextView>(R.id.progress_text_wait).text = frase
                //dialog.message(0, frase)

                endShareProcedure(TagNames.ShareAction.END, null)
                trackEvent(TagNames.ShareAction.End.Success.FB_PAGE, null)
                recordShareToServer(postID1)

                val handler = Handler()
                handler.postDelayed({
                    dialog.dismiss()
                }, 2500)
            }
        }
    }

    private fun sharePhotoToFacebookPageConfirmed(file: File, messaggio: String) {

        Log.v("FACEBOOKPAOLO","shareToFacebookPageConfirmed")

        Log.v("FACEBOOKPAOLO","shareToFacebookPageConfirmed èFoto")

        Manager.shareToFacebookPage(file, messaggio, false) { error1, msg1, postID1 ->

            if (error1) {
                Log.v("FACEBOOKPAOLO", "post non pubblicato: $msg1")
                if (msg1 != null) {
                    MaterialDialog(this).show {
                        icon(R.drawable.baseline_warning_18)
                        title(R.string.warning)
                        message(R.string.page_publish_error)
                        positiveButton(0, "Vai alla pagina Profilo") {
                            val intent = Intent(context, ProfileActivity::class.java).apply {
                                setFlags(intent)
                                putExtra("scrollbottom", true)
                            }
                            startActivity(intent)
                        }
                        negativeButton(0, "Ok") {

                        }
                    }
                }

                endShareProcedure(TagNames.ShareAction.FAIL, msg1)
                trackEvent(TagNames.ShareAction.End.Fail.FB_PAGE, null)

            } else {
                Log.v("FACEBOOKPAOLO", "post pubblicato: $msg1")
                val tipologia = ITEM_TYPE.capitalize()
                val verbo  = if (currentPostItem.isPost()) "pubblicato" else "pubblicata"
                showAdvice("$tipologia $verbo correttamente nella pagina Facebook")
                endShareProcedure(TagNames.ShareAction.END, null)
                trackEvent(TagNames.ShareAction.End.Success.FB_PAGE, null)
                recordShareToServer(postID1)
            }
        }
    }

    //CHIAMATO DA VIEW AL CLICK
    fun downloadAction(view: View?){

        val properties = Properties().apply {
            putValue("type", ITEM_TYPE)
            putValue("media", MEDIA_TYPE)
            putValue("id", currentPostItem.id)
        }
        trackEvent(TagNames.DOWNLOAD_ACTION, properties)
        trackEvent(TagNames.EXPORT_EVENT, properties)

        Log.v("STEPS", "1) cliccato per scaricare elemento")

        shareDestination = ShareDestination.DOWNLOAD
        hideOverlay()

        //shareRequest = false //DOWNLOAD
        userAction = UserAction.DOWNLOAD

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.v("STEPS", "2) chiedo permessi per scaricare elemento")
            chiediPermessiStorage()
        } else {
            Log.v("STEPS", "non chiedo permessi per scaricare elemento")

            if (currentPostItem.isVideo()){

                recordShareToServer(null)

                val filename = generateRandomFileName("mp4")

                downloadVideoOkPermissions(this, currentPostItem.url, filename) { error, message, uri ->
                    if (!error){
                        hideOverlay()
                        showMediaSavedAdvice()
                    }
                    else if (message!=null) {
                        askQuestion(
                            getString(R.string.errore),
                            message,
                            getString(R.string.riprova),
                            getString(
                                R.string.annulla
                            )
                        ) { confirmed ->
                            if (confirmed) downloadAction(view)
                            else {
                                hideOverlay()
                            }
                        }
                    }
                }
            }
            //else if (!shareRequest) {
            else if (userAction == UserAction.DOWNLOAD) {
                val filename = generateRandomFileName("png")
                downloadActionOkPermissions(filename)
            } else {
                shareImageOkPermissions(DEFAULT_IMAGE_NAME)
            }
        }

    }

    @SuppressLint("SetTextI18n")
    private fun downloadVideoOkPermissions(
        context: Context,
        url: String,
        fileName: String,
        listener: (Boolean, String?, Uri?) -> Unit
    ){

        showOverlay()

        Utils.log("Downloading to $savedPathVideo")

        AndroidNetworking.download(url, savedPathVideo, fileName)
            .setTag(APP_NAME)
            .setPriority(Priority.MEDIUM)
            .build()
            .setDownloadProgressListener { bytesDownloaded, totalBytes ->
                // do anything with progress
                progressBarPercent?.text = "$(($bytesDownloaded/$totalBytes)*100)%"
            }
            .startDownload(object : DownloadListener {
                override fun onDownloadComplete() {

                    downloadedVideo = true

                    val savedPath = savedPathVideo + File.separator + fileName

                    sendBroadcast(
                        Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                            Uri.parse(savedPath)
                        )
                    )

                    scanFile(context, arrayOf(savedPath), null) { path, uri ->
                        Utils.log("Finished scanning $path New row: $uri")
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                        scanEnvironment(Environment.DIRECTORY_DCIM)
                        showMediaSavedAdvice()
                        listener(false, null, Uri.parse(savedPath))
                        hideOverlay()

                    } else {
                        sendBroadcast(
                            Intent(
                                Intent.ACTION_MEDIA_MOUNTED,
                                Uri.parse("file://$savedPath")
                            )
                        )

                        // if (!shareRequest) showAdvice("Salvato nella galleria")
                        val uri = uriForVideo(fileName)
                        listener(false, null, uri)
                        hideOverlay()
                    }
                }

                override fun onError(error: ANError?) {
                    // handle error
                    listener(true, error?.localizedMessage ?: "Errore sconosciuto", null)
                    hideOverlay()
                }
            })
    }

    private fun scanEnvironment(environment: String) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val f1 = File("file://" + Environment.getExternalStoragePublicDirectory(environment))
        val contentUri = Uri.fromFile(f1)
        mediaScanIntent.data = contentUri
        sendBroadcast(mediaScanIntent)
    }

    private fun hideOverlay() {
        loadingOverlay?.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    private fun shareVideoOkPermissions(
        context: Context,
        url: String,
        fileName: String,
        listener: (Boolean, String?, Uri?) -> Unit
    ){

        Log.v("VIDEOPAOLO", "6) scarichiamo video")

        showOverlay()
        Utils.log("Downloading to $downloadedVideoPath")

        AndroidNetworking.download(url, downloadedVideoPath, fileName)
            .setTag(APP_NAME)
            .setPriority(Priority.MEDIUM)
            .build()
            .setDownloadProgressListener { bytesDownloaded, totalBytes ->
                progressBarPercent?.text = "$(($bytesDownloaded/$totalBytes)*100)%"
            }
            .startDownload(object : DownloadListener {
                override fun onDownloadComplete() {

                    val savedPath = downloadedVideoPath + File.separator + fileName

                    Log.v("VIDEOPAOLO", "5) savedPath = " + savedPath)

                    sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(savedPath)))

                    scanFile(context, arrayOf(savedPath), null) { path, uri ->
                        Utils.log("Finished scanning $path New row: $uri")
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                        scanEnvironment(Environment.DIRECTORY_DCIM)
                        showMediaSavedAdvice()
                        Log.v("VIDEOPAOLO", "8) esco da funzione")
                        listener(false, null, uriForVideo(fileName))
                        hideOverlay()
                        val uri = saveVideoToStorage(savedPath)
                        info.put("video_new_uri", uri)

                    } else {
                        sendBroadcast(
                            Intent(
                                Intent.ACTION_MEDIA_MOUNTED,
                                Uri.parse("file://$downloadedVideoPath")
                            )
                        )

                        //if (!shareRequest) showAdvice("Salvato nella galleria")
                        val uri = uriForVideo(fileName)
                        listener(false, null, uri)
                        hideOverlay()
                    }
                }

                override fun onError(error: ANError?) {
                    // handle error
                    listener(true, error?.localizedMessage ?: "Errore sconosciuto", null)
                    hideOverlay()
                }
            })
    }

    fun uriForVideo(name: String):Uri{

        val dir = getExternalFilesDir(null)
        Log.d("test", dir?.path!!)
        val path = "${dir.absolutePath}/$APP_NAME/$name"
        val f = File(path) //
        val uri = Uri.fromFile(f)
        Log.v("VIDEOPAOLO", "uriForVideo = " + uri.path)
        return uri
    }

    private fun downloadActionOkPermissions(filename: String):Uri?{

        Log.v("STEPS", "6) avvio procedura scaricamento avendo i permessi, il file è $filename")

        if (filename.isEmpty() || filename.isBlank()){
            Log.v("STEPS", "downloadActionOkPermissions, esco")
            return  null
        }

        showOverlay()

        Log.v("STEPS", "7) mostro overlay e genero bitmap")
        val render:Bitmap = generateResult(imageLayoutContainer)

        saveToGallery(CustomApplication.context, filename, render)?.let { savedPath ->

            Log.v("STEPS", "4) saveToGallery");

            if (savedPath.isEmpty() || savedPath.isBlank()) {
                Log.v("STEPS", "4a) esco, il save path è vuoto");
                return null
            }

            /*

            DEPRECATO

            sendBroadcast(
                Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.fromFile(File(savedPath))
                )
            )
            */

            val paths = arrayOf(savedPath)
            val mimeTypes = null //arrayOf(savedPath.getName())

            //commento, sta anche sotto
            //scanFile(CustomApplication.context,paths,mimeTypes, null)

            Log.v("STEPS", "5 - scansiono per cercare file")

            trackEvent(TagNames.ShareAction.End.Success.DOWNLOAD, null)

            if (isAndroid10_OrMore){

                Log.v("STEPS", "6 - Android > 29");

                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE
                val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
                val f1 = File(
                    /*"file://"*/ "" + "${Environment.DIRECTORY_PICTURES}/$ALBUM_NAME"
                )
                val contentUri = Uri.fromFile(f1)
                mediaScanIntent.data = contentUri
                sendBroadcast(mediaScanIntent)
                
                //scanEnvironment()

                scanFile(this, arrayOf(savedPath), null) { path, uri ->
                    Utils.log("Finished scanning $path New row: $uri")
                }

                showMediaSavedAdvice()
                hideOverlay()

                val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                val file = "${Environment.DIRECTORY_PICTURES}/$ALBUM_NAME/$DEFAULT_IMAGE_NAME"

                return Uri.fromFile(File(savedPath))
            }
            else {
                    // versioni da 5 a 9 comprese
                    Log.v("STEPS", "6 - Android > kit kat");

                    scanEnvironment(Environment.DIRECTORY_DCIM)

                    scanFile(this, arrayOf(savedPath), null) { path, uri ->
                        Utils.log("Finished scanning $path New row: $uri")
                        Log.v("STEPS", "6a - scanning $path New row: $uri");
                    }

                    showMediaSavedAdvice()
                    hideOverlay()

                    return Uri.fromFile(File(savedPath))

                }
        }
            ?: run {

                Log.v("STEPS", "4a - saveToGallery: run");

                return null
            }
    }

    fun showMediaSavedAdvice(){
        if (userAction == UserAction.DOWNLOAD) showAdvice("Salvato nella galleria")
        //if (!shareRequest) showAdvice("Salvato nella galleria")
    }

    fun selectLogo(frag: EditorLogoFragment){

        callerLogoFragment = frag
        selectLogoRequest  = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //se android maggiore o uguale a 6
            chiediPermessiStorage()
        } else {
            selectLogoOkPermissions()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun chiediPermessiStorage() {
        requestPermissions(
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ), REQUEST_WRITE_READ_PERMISSIONS
        )
    }

    fun stopVideo(view: View) {

        videoView.stopPlayback()
        videoView.setVideoURI(null)
        videoContainer.visibility = View.GONE
        hideOverlay()
    }

    fun playVideo(view: View?) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        playVideo(info)
    }

    fun playVideo(video: JSONObject) {

        videoContainer.visibility = View.VISIBLE
        /*videoView.setOnPreparedListener {
            videoContainer.visibility = View.VISIBLE
            hideOverlay()
        }*/

        videoView.setOnCompletionListener {
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            hideOverlay()
        }

        videoView.setOnErrorListener { mp, what, extra ->
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            hideOverlay()

            true
        }

        val videoURL = video.getString("url")
        Log.v("FACEBOOKPAOLO", "mostro video con url " + videoURL)
        videoView.setVideoURI(Uri.parse(videoURL))
        videoView.start()
        videoView.requestFocus();

        window.setFormat(PixelFormat.OPAQUE)
    }

    private fun selectLogoOkPermissions(){
        selectLogoRequest  = false
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        //startActivityForResult(intent, LOGO_PICK_CODE_REQUEST_CODE)
        getLogoResult.launch(intent) //sostituisce startActivityForResult
    }

    private val getLogoResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                    Log.v("STEPS", "onActivityResult 1")
                    val imageUri = it.data?.data
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                    EditorMainFragment.logoImage = bitmap
                    callerLogoFragment?.updateLogo(bitmap)
                    callerLogoFragment = null
                }
            }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun addImageToGalleryQ(bitmap: Bitmap, context: Context): String? {

        Log.v("STEPS", "11) addImageToGalleryQ, comprimo PNG")

        val write: (OutputStream) -> Boolean = {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }

        val resolver = context.applicationContext.contentResolver

        //val pictureFolder = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY) //MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val pictureFolder = when (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            true -> MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL)
            false -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        }

        //val fileName = "GHD_${System.currentTimeMillis()}.png"
        val fileName = generateRandomFileName("png")

        Log.v("STEPS", "12) addImageToGalleryQ, fileName = $fileName")
        Log.v("STEPS", "12a) addImageToGalleryQ, pictureFolder = ${pictureFolder.path}")

        val pictureDetails = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
            put(MediaStore.MediaColumns.TITLE, APP_NAME)
            put(MediaStore.Images.Media.DESCRIPTION, APP_NAME);
            put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
            put(MediaStore.MediaColumns.RELATIVE_PATH, "${Environment.DIRECTORY_DCIM}/$ALBUM_NAME/") //"${Environment.DIRECTORY_PICTURES}/$ALBUM_NAME")
            put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis())
            put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            put(MediaStore.Audio.Media.IS_PENDING, 1)
        }

        Log.v("STEPS", "13) prima di resolver let")

        resolver.let {
            it.insert(pictureFolder, pictureDetails)
                ?.let { uri ->
                    it.openOutputStream(uri)?.let(write)

                    pictureDetails.put(MediaStore.Images.Media.IS_PENDING, 0)
                    contentResolver.update(uri, pictureDetails, null, null)

                    /*
                    if (!startShareProcedure(uri)) {
                        Log.v("STEPS", "share intent non creato")
                        if (shareDestination == ShareDestination.DOWNLOAD) return uri.path
                    }
                    */

                    startShareProcedure(uri).run {
                        if (this) {
                         //esito positivo
                             Log.v("STEPS", "procedura share OK")
                            return ""
                        }
                        else {
                            //esito negativo
                            Log.v("STEPS", "procedura share KO")
                            if (shareDestination == ShareDestination.DOWNLOAD) return uri.path
                        }
                    }

                    return "" //uri.path!!
                }
        }

        return ""
    }

    fun main() = runBlocking {
        repeat(20) { // launch a lot of coroutines
            launch {
                delay(3000L)
                Log.v("FACEBOOKPAOLO", "passato un secondo")
            }
        }
    }

    private fun createShareIntent(uri: Uri, messaggio: String) : Intent? {

        Log.v("STEPS", "16) entrato in createShareIntent, shareDestination = $shareDestination")

        var nuovoUri : Uri?
        nuovoUri = uri

        if (isAndroid11_OrMore) {
            //QUESTO SERVE DA ANDROID 11 in su.
            // il video viene messo su /external_primary/video/media/, come le foto più o meno
            if (currentPostItem.isVideo()) nuovoUri = saveVideoToStorage(uri.path!!)
        }

        if (nuovoUri == null) {
            return null
        }

        Log.v("STEPS", "nuovoUri = " + nuovoUri.path )

        var packageID = ""
        val intento = when (shareDestination) {
            ShareDestination.WHATSAPP -> {
                IntentGenerator.getWhatsappIntent(nuovoUri, messaggio, currentPostItem.isVideo())
            }
            ShareDestination.INSTAGRAM -> {
                IntentGenerator.getInstagramIntent(nuovoUri, messaggio, currentPostItem.isVideo(), !currentPostItem.isPost())
            }
            ShareDestination.OTHER -> {
                IntentGenerator.getGenericIntent(nuovoUri, messaggio, currentPostItem.isVideo())
            }
            else -> null //Intent(Intent.ACTION_SEND)
        }

        Log.v("STEPS", "16) entrato in createShareIntent, step 2")

        if (shareDestination == ShareDestination.WHATSAPP) packageID = "com.whatsapp"
        if (shareDestination == ShareDestination.INSTAGRAM) packageID = "com.instagram.android"
        if (shareDestination == ShareDestination.FACEBOOK_PROFILE) {
            handleFacebookProfileShare(uri, messaggio)
        }

        if (shareDestination == ShareDestination.FACEBOOK_PAGE) {
            Log.v("FACEBOOKPAOLO", "16a) destinazione scelta = pagina facebook")
            Log.v("STEPS","condivisione su pagina facebook")
            startFacebookPageReadProcedure(uri, messaggio)
        }

        if (packageID.isNotEmpty()) {
            Log.v("STEPS", "17) chiedo permessi per package: " + packageID)
            //grantUriPermission(packageID, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            copyTextAction(null)
        } else {
            Log.v("STEPS", "17) non ho package $packageID, non chiedo permessi")
        }

        return intento
    }

    private fun handleFacebookProfileShare(uri: Uri, messaggio: String) {
        if (currentPostItem.isVideo()) {
            if (currentPostItem.isPost()) facebookSharePostVideoByUri(uri, messaggio)
            else facebookShareStoryVideoByUri(uri, messaggio)
        }
        else {
            if (currentPostItem.isPost()) facebookSharePostImageByUri(uri, messaggio)
            else facebookShareStoryImageByUri(uri, messaggio)
        }
    }

    private fun startFacebookPageReadProcedure(uri: Uri, messaggio: String) {

        Log.v("FACEBOOKPAOLO", "[fb page share] start")

        var pageSelected = false

        if (Utils.sharefPrefs.contains(EditorMainFragment.FB_PAGE)) {

            Log.v("FACEBOOKPAOLO", "[fb page share] trovata pagina tra i preferiti")

            val fbPageJson = Utils.sharefPrefs.getJSONObject(EditorMainFragment.FB_PAGE)

            pageSelected = try {
                val facebookPage = Gson().fromJson(fbPageJson.toString(), FacebookPage::class.java)
                Log.v("FACEBOOKPAOLO", "[fb page share] pagina selezionata = " + facebookPage.selected)
                facebookPage.selected
            } catch (ex: java.lang.Exception) {

                Log.v("FACEBOOKPAOLO", "[fb page share][Exception] " + ex.localizedMessage)
                hideOverlay()
                false
            }
        } else {
            Log.v("FACEBOOKPAOLO", "[fb page share] pagina non trovata tra i preferiti")

        }

        if (pageSelected) {
            Log.v("FACEBOOKPAOLO", "[fb page share] trovata pagina selezionata")
            showFacebookPagePostPreview(uri, messaggio)
        }
        else {

            Log.v("FACEBOOKPAOLO", "[fb page share] nessuna pagina selezionata")

            Manager.getFacebookPages { error, msg, response ->
                if (error) {
                    Log.v("FACEBOOKPAOLO", "[fb page share] elenco pagine non scaricato: $msg")
                } else {
                    Log.v("FACEBOOKPAOLO", "[fb page share] elenco pagine scaricato: $msg")

                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

                if (response != null) {
                    try {
                        Log.v("FACEBOOKPAOLO", "entro nel try - step 1")
                        val paginejson = msg.toString().trim()
                        Log.v("FACEBOOKPAOLO", "paginejson: $paginejson")

                        val pagina = response.pages.firstOrNull() { it.active }

                        Log.v("FACEBOOKPAOLO", "entro nel try - step 2")

                        if (pagina != null) {
                            Log.v("FACEBOOKPAOLO", "[fb page share] pagina trovata = " + pagina.item_name)

                            val json = pagina.getJSON()
                            Utils.sharefPrefsEditor.apply {
                                putJSONObject(EditorMainFragment.FB_PAGE, json)
                            }

                            Log.v("FACEBOOKPAOLO", "[fb page share] pagina salvata su preferiti - step 3")

                            showFacebookPagePostPreview(uri, messaggio)

                        } else

                            if (response.pages.isNotEmpty()) {
                                Log.v(
                                    "FACEBOOKPAOLO",
                                    "numero di pagine = " + response.pages.size
                                )

                                val myItems = response.getPageNameArray()

                                Log.v("FACEBOOKPAOLO", "myItems = $myItems")

                                val adapter: RecyclerView.Adapter<MyRecyclerAdapter.FacebookPageHolder> =
                                    MyRecyclerAdapter(response.pages)

                                Log.v("FACEBOOKPAOLO", "qui")

                                MaterialDialog(this).show {
                                    //title(text = "Seleziona la pagina del tuo salone")

                                    customView(R.layout.facebook_page_chooser)

                                    //icon(R.mipmap.ic_launcher)
                                    customListAdapter(adapter)

                                    negativeButton(0, "Manca la tua pagina?") {
                                        Log.v("FACEBOOKPAOLO", "cliccato su pagina mancante")
                                    }

                                    positiveButton(R.string.ok, "Seleziona") {
                                        Log.v("FACEBOOKPAOLO", "cliccato su si 1")

                                        //todo: salvare preferenza pagina scelta

                                        val idPaginaScelta = response.pages.firstOrNull() { it.active }

                                        if (idPaginaScelta != null) Manager.updateSelectedFacebookPage(
                                            idPaginaScelta
                                        )

                                        MaterialDialog(context).show {
                                            customView(R.layout.facebook_page_link_complete)

                                            positiveButton(R.string.ok, "Chiudi") {
                                                Log.v("FACEBOOKPAOLO", "cliccato su si 2")
                                                hideOverlay()

                                                showFacebookPagePostPreview(uri, messaggio)
                                            }
                                        }
                                    }

                                    negativeButton(R.string.annulla, null) {
                                        Log.v("FACEBOOKPAOLO", "cliccato su no")
                                        hideOverlay()
                                    }

                                }

                            } else {
                                Log.v("FACEBOOKPAOLO", "Nessuna pagina Facebook trovata")
                                //showAdvice("Nessuna pagina Facebook trovata")
                                hideOverlay()

                                startFacebookLoginProcedure(uri, messaggio)
                            }
                        } catch (ex: Exception) {
                            Log.v("FACEBOOKPAOLO", "ex = " + ex.localizedMessage)
                        }
                    }

                    //val selectedFilePath: String = FilePath.getPath(this, uri)
                }
            }
        }
    }

    private fun startFacebookLoginProcedure(uri: Uri, messaggio: String) {
        //val intent = Intent(this, ProfileActivity::class.java).apply {
        //}
        //startActivity(intent)

            //LayoutInflater.from(this).inflate(R.layout.facebook_modal_login, this, this)

            //val myView: inflate(context, R.layout.facebook_modal_login, this)

            val dialog = MaterialDialog(this).show {
            customView(R.layout.facebook_modal_login)

                /*
            positiveButton(0, "Fatto") { dialog ->
                Log.v("FACEBOOKPAOLO", "cliccato")

                    startFacebookPageReadProcedure(uri, messaggio)
                }
                */
            }

            bindFacebookButton(dialog, uri, messaggio)
    }

    private fun bindFacebookButton(dialog: MaterialDialog, uri: Uri, messaggio: String) {

        Log.v("FACEBOOKPAOLO", "bindFacebookButton")

        val view = dialog.getCustomView()

        // Callback registration
        callbackManager = CallbackManager.Factory.create();

        val fbLoginBtn = view.findViewById<LoginButton>(R.id.modal_fb_login_button)
        val introText = view.findViewById<TextView>(R.id.modal_fb_introtext)

        //paolo
        fbLoginBtn.setPermissions(listOf("email", "pages_manage_posts", "pages_show_list"))

        // Callback registration
        fbLoginBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {

            override fun onSuccess(loginResult: LoginResult?) {
                // App code
                if (loginResult == null) {
                    Log.v("FACEBOOKPAOLO", "onSuccess ma risposta nulla")
                    return
                }

                trackEvent(TagNames.FACEBOOK_LOGIN, null)

                //nascondo bottone FB
                fbLoginBtn.visibility = View.GONE

                val userID = Utils.sharefPrefs.getString("user_id", "")
                if (userID!!.isNotEmpty()) {
                    Log.v("FACEBOOKSHARE", "preferenza salvata")
                    Utils.sharefPrefsEditor.putString(EditorMainFragment.FB_LOGGED_USER, userID)
                        .apply()
                }

                // IL TOKEN E' DI LUNGA DURATA
                val aToken = loginResult.accessToken
                val fbUserID = aToken.userId
                //aToken.lastRefresh

                val request = GraphRequest.newMeRequest(aToken) { `object`, response ->

                    val nome = if (response.jsonObject.has("name")) response.jsonObject.get("name").toString() else ""
                    val email = if (response.jsonObject.has("email")) response.jsonObject.get("email").toString() else ""
                    val picture = if (response.jsonObject.has("picture")) response.jsonObject.get("picture").toString() else ""

                    //introText.text = "Grazie, hai effettuato login a nome di $nome. Premi 'Fatto' per uscire"

                    val pageResponse = Gson().fromJson(
                        picture,
                        FacebookProfileImageWrapper::class.java
                    )

                    Log.v(
                        "FACEBOOKPAOLO",
                        "response.jsonObject = " + response.jsonObject.toString()
                    )
                    Log.v("FACEBOOKPAOLO", "nome = " + nome)
                    Log.v("FACEBOOKPAOLO", "foto profilo = " + pageResponse.data.url)
                    Log.v("FACEBOOKPAOLO", "email = " + email)
                    Log.v("FACEBOOKPAOLO", "token = " + aToken.token)
                    Log.v("FACEBOOKPAOLO", "lastRefresh = " + aToken.lastRefresh)

                    val permessi = aToken.permissions
                    val permessiJson = JSONArray()
                    aToken.permissions.forEach {
                        Log.v("FACEBOOKPAOLO", "\tpermesso $it")
                        permessiJson.put(it)
                    }
                    Log.v("FACEBOOKPAOLO", "numero permessi approvati = " + permessi.size)

                    Manager.sendFacebookAccessToken(aToken) { error: Boolean, message: String?, result: FacebookRegistrationResponse? ->
                        if (error) {
                            Log.v("FACEBOOKPAOLO", "token non inviato")

                            introText.text = "Si è verificato un problema"
                            //dialog.

                        } else {
                            Log.v("FACEBOOKPAOLO", "token inviato")

                            dialog.dismiss()
                            startFacebookPageReadProcedure(uri, messaggio)
                        }

                        Log.v("FACEBOOKPAOLO", "message = $message")
                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "id, name, email, picture")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                // App code
                Log.v("FACEBOOKPAOLO", "onCancel");
                introText.text = "Hai annullato la procedura"
            }

            override fun onError(exception: FacebookException) {
                // App code
                Log.v("FACEBOOKPAOLO", "onError: " + exception.localizedMessage);
                introText.text =
                    "Si è verificato un errore durante la procedura: " + exception.localizedMessage
            }
        })
    }

    //RISPOSTA ASINCRONA DEL RESULT
    private val getFacebookPageShareRequestResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {

            if (it.resultCode == Activity.RESULT_OK) {
                Log.v("FACEBOOKPAOLO", "[page share] esito positivo")
                if (it.data != null && it.data!!.extras != null) {

                    val messaggio = it.data!!.extras!!.getString("messaggio")!!

                    Log.v("FACEBOOKPAOLO", "[page share] message = " + messaggio)

                    if (it.data!!.extras!!.containsKey("file")) {
                        val file = it.data!!.extras!!.getSerializable("file") as File
                        sharePhotoToFacebookPageConfirmed(file, messaggio)
                    } else {
                        val videoURL = it.data!!.extras!!.getSerializable("video_url") as String
                        Log.v("FACEBOOKPAOLO", "[page share] video_url qui = $videoURL")
                        shareVideoToFacebookPageConfirmed(videoURL, messaggio)
                    }
                }

            } else {
                Log.v("FACEBOOKPAOLO", "[page share] esito negativo")
                hideOverlay()
                //TODO controllare
                trackEvent(TagNames.ShareAction.End.Cancel.FB_PAGE, null)
            }
        }

    private fun showFacebookPagePostPreview(uri: Uri, messaggio: String)  {

        if (Utils.sharefPrefs.contains(EditorMainFragment.FB_PAGE)) {

            Log.v("FACEBOOKPAOLO", "[fb page share] pagina salvata su preferiti - step 4")

            //TODO: non funziona condivisione video, non c'è uri locale ma URL remoto

            if (currentPostItem.isVideo()) {
                //VIDEO

                val videoURL = info.getString("url")

                Log.v("FACEBOOKPAOLO", "il video è " + videoURL)

                val intent = Intent(this, PagePostPreviewActivity::class.java).apply {
                    putExtra("messaggio", messaggio)
                    putExtra("video_uri", uri)
                    putExtra("video_url", videoURL)
                    putExtra("is_video", true)
                }

                //startActivityForResult(intent, FACEBOOK_PAGE_SHARE_REQUEST_CODE)

                getFacebookPageShareRequestResult.launch(intent) //sostituisce startActivityForResult

            } else {
                    //FOTO
                    val file = FileUtils.getFileFromUri(this, uri)
                    Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] file creato, uri = " + uri.path)
                    // viene usato File Locale tramite URI
                    if (file != null && file.exists()) {
                        Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] file esiste")
                        showOverlay()
                        val intent = Intent(this, PagePostPreviewActivity::class.java).apply {
                            putExtra("messaggio", messaggio)
                            putExtra("file", file)
                            //putExtra("video_uri", uri)
                            putExtra("is_video", false)
                        }
                        //startActivityForResult(intent, FACEBOOK_PAGE_SHARE_REQUEST_CODE)
                        getFacebookPageShareRequestResult.launch(intent) //sostituisce startActivityForResult
                    } else {
                        Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] file non esiste")
                        hideOverlay()
                        showAdvice("Spiacenti, si è verificato un errore con la risorsa da condividere")
                    }
                }
        }
        else {
            Log.v("FACEBOOKPAOLO", "[shareToFacebookPage] manca preferito con nome pagina")
            hideOverlay()
            showAdvice("Spiacenti, si è verificato un errore con la pagina")
        }
    }

    private fun startChooser(shareIntent: Intent, chooser: Intent?) {
        if (shareIntent.resolveActivity(packageManager) != null) {

            Log.v("STEPS", "22) trovata activity ")
            startActivity(chooser)

            // non chiamo endShareProcedure
            hideOverlay()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

            val oggetto = JSONObject().apply {
                put("type", ITEM_TYPE)
                put("media", MEDIA_TYPE)
                put("id", currentPostItem.id)
                put("destination", shareDestination)
            }
            
            val editor = Utils.sharefPrefs.edit()

            if (Utils.sharefPrefs.contains(TagNames.SHARE_DATA_KEY)) {
                editor.apply {
                    remove(TagNames.SHARE_DATA_KEY)
                }.apply()
            }

            editor.apply{
                putJSONObject(TagNames.SHARE_DATA_KEY, oggetto)
            }.apply()


            Log.v("FACEBOOKPAOLO","[startChooser] chiamo recordShareToServer")
            recordShareToServer(null)
        } else {
            showAdvice("Non è possibile condividere il contenuto")
            endShareProcedure(
                TagNames.ShareAction.FAIL,
                "Applicazione non presente nel dispositivo utente"
            )

            val properties1 = Properties()
            properties1["step"] = 1
            trackEvent(TagNames.ShareAction.MISSING_APP_ERROR, properties1)
        }
    }

    private fun saveToGallery(context: Context, filename: String, bitmap: Bitmap):String?{

        Log.v("STEPS", "8) saveToGallery, filename = $filename")

        val myFile: File = File("${Environment.DIRECTORY_DCIM}/$ALBUM_NAME/$filename")
        if (myFile.exists()) {
            myFile.delete()
            Utils.log("Deleting previous $APP_NAME file ${myFile}")
            Log.v("STEPS", "8a) cancello doppione = $myFile")
        }

        /*val sdcard = Environment.getExternalStorageDirectory()
        if (sdcard != null) {
            val mediaDir = File(sdcard, "${Environment.DIRECTORY_DCIM}/${Environment.DIRECTORY_PICTURES}")
            if (!mediaDir.exists()) {
                mediaDir.mkdirs()
            }
        }
        */

        Log.v("STEPS", "9) comprimo bitmap")
        val write: (OutputStream) -> Boolean = {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //se android maggiore o uguale a 10
           try {

               Log.v("STEPS", "10) sono su Android >= 29")
               return addImageToGalleryQ(bitmap, context)
           }
           catch (ex: Exception){

               FirebaseCrashlytics.getInstance().log("Crashing >= Q FAILED external trying internal into ${Environment.DIRECTORY_DCIM}/$ALBUM_NAME")
               FirebaseCrashlytics.getInstance().recordException(ex)

               try {
                   val contentValues = ContentValues().apply {
                       put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                       put(MediaStore.MediaColumns.TITLE, filename)
                       put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                       put(
                           MediaStore.MediaColumns.RELATIVE_PATH,
                           "${Environment.DIRECTORY_PICTURES}/$ALBUM_NAME"
                       )
                   }

                   context.contentResolver.let {
                       it.insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, contentValues)
                           ?.let { uri ->
                               it.openOutputStream(uri)?.let(write)
                               return uri.path!!
                           }
                   }

                   return "${Environment.DIRECTORY_DCIM}/$ALBUM_NAME/$filename"
               }
               catch (ex: Exception){

                   FirebaseCrashlytics.getInstance().log("Crashing >= Q FAILED internal into ${Environment.DIRECTORY_PICTURES}/$ALBUM_NAME")
                   FirebaseCrashlytics.getInstance().recordException(ex)
                   return null
               }
           }

        } else {
            Log.v("STEPS", "10) sono su Android < 10 (api 29)")
            val imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + File.separator + ALBUM_NAME
            val file = File(imagesDir)
            if (!file.exists()) {
                file.mkdir()
            }
            val image = File(imagesDir, filename)
            write(FileOutputStream(image))

            Log.v("STEPS", "10a) imagesDir = $imagesDir")
            Log.v("STEPS", "10b) file = $file")
            return image.path
        }

    }

    fun selectTextWhiteColor(view: View?){
        Utils.sharefPrefsEditor.putColor(EditorMainFragment.FONT_COLOR, Color.WHITE).apply {  }
        textColor = Color.WHITE
        trackEvent(TagNames.TEXT_COLOR_SELECTED, null)
    }

    fun selectTextBlackColor(view: View?){
        textColor  = Color.BLACK
        Utils.sharefPrefsEditor.putColor(EditorMainFragment.FONT_COLOR, Color.BLACK).apply {  }
        trackEvent(TagNames.TEXT_COLOR_SELECTED, null)
    }

    fun selectTextColor(color: Int){
        textColor  = color
        Utils.sharefPrefsEditor.putColor(EditorMainFragment.FONT_COLOR, color).apply {  }
    }

    private fun generateResult(theView: View):Bitmap{
        theView.layout(0, 0, theView.width, theView.height);
        val b = Bitmap.createBitmap(theView.width, theView.height, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        theView.draw(c)
        return b
    }

    fun selectFont(index: Int) {
        //todo font storing
    }

    fun copyTextAction(view: View?) {

        if (!currentPostItem.isPost()) return

        val clip: ClipData = ClipData.newPlainText("simple text", getCaptionTextFromPrefs())

        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.setPrimaryClip(clip)

        if (userAction == UserAction.DOWNLOAD) {
        //if (!shareRequest) {
            showAdvice("Testo copiato negli appunti")
        }
    }

    fun confirmTextEdit(view: View) {
        textToShare = editTextTextMultiLine.text.toString()
        Utils.sharefPrefsEditor.putString(EditorMainFragment.CAPTION_TEXT, textToShare).apply()
        hideKeyboard()
        gotoHomePage(view)
        trackEvent(TagNames.ACTION_EDIT_COPY_SAVED, null)
    }

    fun editTextAction(view: View) {
        Log.v("AZIONI", "editTextAction")
        editorViewPager.setCurrentItem(1, false)
    }

    @Suppress("DEPRECATION")
    private fun saveVideoToStorage(
        filePath: String,
        filename: String = "video.mp4"
    ) : Uri? {

        val mediaContentUri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val mimeType: String =  "video/mp4"
        val directory: String = Environment.DIRECTORY_MOVIES

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            val values = ContentValues().apply {
                put(MediaStore.Images.Media.DISPLAY_NAME, filename)
                put(MediaStore.Images.Media.MIME_TYPE, mimeType)
                put(MediaStore.Images.Media.RELATIVE_PATH, directory)
                //put(MediaStore.Video.Media.RELATIVE_PATH, "Movies/" + "Folder");
                put(MediaStore.Video.Media.TITLE, filename)
                put(MediaStore.Video.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
                put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis())
                put(MediaStore.Video.Media.IS_PENDING, 1)
            }

            val collection = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)

            contentResolver.run {
                //val fileUri = contentResolver.insert(mediaContentUri, values) ?: return
                val fileUri = contentResolver.insert(collection, values) ?: return null
                //val videoOutStream: OutputStream
                //videoOutStream = openOutputStream(fileUri) ?: return

                val context = CustomApplication.context

                fileUri.let {
                    context.contentResolver.openFileDescriptor(fileUri, "w").use { descriptor ->
                        descriptor?.let {
                            FileOutputStream(descriptor.fileDescriptor).use { out ->

                                val videoFile = File(filePath)

                                FileInputStream(videoFile).use   { inputStream ->
                                    val buf = ByteArray(8192)
                                    while (true) {
                                        val sz = inputStream.read(buf)
                                        if (sz <= 0) break
                                        out.write(buf, 0, sz)
                                    }
                                }

                                //cancello vecchio file
                                //videoFile.delete()
                            }
                        }
                    }

                    values.clear()
                    values.put(MediaStore.Video.Media.IS_PENDING, 0)
                    context.contentResolver.update(fileUri, values, null, null)

                    Log.v("VIDEOPAOLO", "fileUri 1 = " + fileUri.path)
                }

                return fileUri
            }

        } else {
            val videoPath = Environment.getExternalStoragePublicDirectory(directory).absolutePath
            val video = File(videoPath, filename)
            val videoOutStream: OutputStream
            videoOutStream = FileOutputStream(video)
        }

        /*
        videoOutStream.use {

        }
        */

        return null
    }

    @Suppress("DEPRECATION")
    private fun saveImageToStorage(
        bitmap: Bitmap,
        filename: String = "screenshot.jpg",
        mimeType: String =  "image/jpeg",
        directory: String = Environment.DIRECTORY_PICTURES,
        mediaContentUri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    ) {
        val imageOutStream: OutputStream
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val values = ContentValues().apply {
                put(MediaStore.Images.Media.DISPLAY_NAME, filename)
                put(MediaStore.Images.Media.MIME_TYPE, mimeType)
                put(MediaStore.Images.Media.RELATIVE_PATH, directory)
            }

            contentResolver.run {
                val uri = contentResolver.insert(mediaContentUri, values) ?: return
                imageOutStream = openOutputStream(uri) ?: return
            }
        } else {
            val imagePath = Environment.getExternalStoragePublicDirectory(directory).absolutePath
            val image = File(imagePath, filename)
            imageOutStream = FileOutputStream(image)
        }

        imageOutStream.use {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
        }
    }
}