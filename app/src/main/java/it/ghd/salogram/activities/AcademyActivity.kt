package it.ghd.salogram.activities

import android.content.res.Configuration
import android.graphics.PixelFormat
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.*
import it.ghd.salogram.adapters.VideoRecyclerAdapter
import kotlinx.android.synthetic.main.activity_video_list.*
import kotlinx.android.synthetic.main.bottom_area_menu.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONArray
import org.json.JSONObject


class AcademyActivity : MainActivity() {

    var videos = JSONArray()

    var questo = this

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {

        screeName = TagNames.Screen.ACADEMY_SCREEN

        setContentView(R.layout.activity_video_list)

        topTitleTV.text = "ACADEMY"

        menuVideoButton.isSelected = true

        val linearLayoutManager = GridLayoutManager(this, 2)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        videoRV.layoutManager = linearLayoutManager
        videoRV.setHasFixedSize(true)

        window.setFormat(PixelFormat.TRANSLUCENT) //required to avoid black flickering issue for video, set to opaque after video is played (otherwise video is transparent)...

        videoView.setMediaController(MediaController(this))

        swipeToRefresh.setOnRefreshListener {
            loadData()
            trackEvent(TagNames.ACADEMY_VIDEO_REFRESH, null)
        }
    }

    /*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    */

    override fun onResume() {
        super.onResume()

        if (firstStart){
            loadData()
            firstStart = false
        }
    }

    fun stopVideo(view: View) {
        videoView.stopPlayback()
        videoView.setVideoURI(null)
        videoContainer.visibility = View.GONE
        loadingOverlay?.visibility = View.GONE
    }

    fun playVideo(video: JSONObject) {

        loadingOverlay?.visibility = View.VISIBLE
        videoContainer.visibility = View.VISIBLE
        /*videoView.setOnPreparedListener {
            videoContainer.visibility = View.VISIBLE
            loadingOverlay?.visibility = View.GONE
        }*/

        videoView.setOnCompletionListener {
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            loadingOverlay?.visibility = View.GONE
        }

        videoView.setOnErrorListener { mp, what, extra ->
            videoContainer.visibility = View.GONE
            videoView.setVideoURI(null)
            loadingOverlay?.visibility = View.GONE
            Toast.makeText(this, "Video non disponibile", Toast.LENGTH_SHORT).show()
            true
        }

        videoView.setVideoURI(Uri.parse(video.getString("url")))
        videoView.start()
        videoView.requestFocus();

        window.setFormat(PixelFormat.OPAQUE)

        /*Manager.downloadFile(
            video.getString("url"),
            video.getString("url").split("/").last()
        ) { error, message, uri ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) playVideo(video)
                    else {
                        loadingOverlay?.visibility = View.GONE
                        videoContainer.visibility = View.GONE
                        videoView.setVideoURI(null)
                    }
                }
            } else {


            }
        }*/
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    private fun loadData() {

        loadingOverlay?.visibility = View.VISIBLE

        Manager.getAllTutorials { error, message, jsonArray ->

            swipeToRefresh.isRefreshing = false

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) loadData()
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }

            } else {
                loadingOverlay?.visibility = View.GONE
                videos = jsonArray!!
                val adapterCategories = VideoRecyclerAdapter(videos, this)
                videoRV.adapter = adapterCategories
            }
        }
    }

    //DATA

    private class CategoriesHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var content: JSONObject? = null

        init {
           // v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
          //showVideo(content!!)
        }

    }


}