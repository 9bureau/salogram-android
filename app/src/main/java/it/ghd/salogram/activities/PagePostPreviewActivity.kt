package it.ghd.salogram.activities

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.*
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.model.FacebookPage
import kotlinx.android.synthetic.main.activity_page_post_preview.*
import java.io.File

class PagePostPreviewActivity : AppCompatActivity() {

    lateinit var messaggio: String
    lateinit var file: File
    lateinit var videoUri: Uri
    private lateinit var videoUrl: String
    var isVideo : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_post_preview)

        if (intent != null && intent.extras != null) {

            if (Utils.sharefPrefs.contains(EditorMainFragment.FB_PAGE)) {

                val fbPageJson = Utils.sharefPrefs.getJSONObject(EditorMainFragment.FB_PAGE)
                val facebookPage = Gson().fromJson(fbPageJson.toString(), FacebookPage::class.java)

                Log.v("FACEBOOKPAOLO", "facebookPage.item_pic = " + facebookPage.item_pic)

                ImageManager.loadImage(facebookPage.item_pic, page_logo)

                val extras = intent.extras ?: return

                isVideo = extras.getBoolean("is_video", false)
                messaggio = extras.getString("messaggio").toString()
                videoUrl = extras.getString("video_url").toString()

                page_name.text = facebookPage.item_name
                post_preview_text.setText(messaggio)

                if (isVideo) {
                    videoUri = extras.getParcelable<Uri>("video_uri") as Uri
                    configureVideo(videoUri)
                }
                else {
                    file = extras.getSerializable("file") as File
                    configureImage(file)
                }

            } else {
                Log.v("FACEBOOKPAOLO", "[PagePostPreviewActivity] manca preferito")
            }
        }
    }

    private fun configureImage(file : File) {
        val myBitmap = BitmapFactory.decodeFile(file.absolutePath)
        post_preview_image.setImageBitmap(myBitmap)
        post_preview_image.visibility = View.VISIBLE
        post_preview_video.visibility = View.GONE
    }

    private fun configureVideo(videoURI : Uri) {

        post_preview_video.visibility = View.VISIBLE
        post_preview_image.visibility = View.GONE

        post_preview_video.setOnCompletionListener {
            post_preview_video.setVideoURI(null)
        }

        post_preview_video.setOnErrorListener { mp, what, extra ->
            post_preview_video.setVideoURI(null)
            true
        }

        post_preview_video.setVideoURI(videoURI)
        post_preview_video.start()
        post_preview_video.requestFocus();

        window.setFormat(PixelFormat.OPAQUE)
    }

    fun closePreview(view: View) {
        finish()
    }

    fun publishToPage(view: View) {
        val intent = Intent()
        val message = post_preview_text.text.toString()
        Log.v("FACEBOOKPAOLO", "[publishToPage] $message")
        intent.putExtra("messaggio", message)
        if (isVideo) {
            //intent.putExtra("video_uri", videoUri)
            intent.putExtra("video_url", videoUrl)
        }
        else intent.putExtra("file", file)
        intent.putExtra("is_video", isVideo)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        val properties = Properties().apply {
            //putProductId(info.getString(""))
            putValue("media", if (isVideo) "video" else "image")
            putValue("type", "post")
        }
        Analytics.with(this).screen(TagNames.Screen.PAGE_POST_PREVIEW_SCREEN, Properties())
    }
}