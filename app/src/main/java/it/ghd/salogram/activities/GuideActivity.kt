package it.ghd.salogram.activities

import android.os.Bundle
import android.util.Log
import android.webkit.WebSettings
import android.webkit.WebViewClient
import it.ghd.salogram.ImageManager
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import kotlinx.android.synthetic.main.activity_guide.*
import kotlinx.android.synthetic.main.top_area_menu.*

class GuideActivity : MainActivity() {

    override fun hasLogo() : Boolean {
        return true
    }

    override fun showGroupLogo() {
        val logoImageUrl = owner?.getDarkPicture()
        Log.v("LOGO", "logoImageUrl = " + logoImageUrl)
        if (group_logo_top != null) {
            ImageManager.loadImage(logoImageUrl, group_logo_top)
            Log.v("LOGO", "showGroupLogo")
        }
    }

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_guide)
        screeName = TagNames.Screen.GUIDE_SCREEN
        val webSetting: WebSettings = browser.getSettings()
        webSetting.builtInZoomControls = true
        browser.setWebViewClient(WebViewClient())

        browser.loadUrl("file:///android_asset/how-it-works.html")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        super.onResume()

    }
}
