package it.ghd.salogram.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.*
import kotlinx.android.synthetic.main.activity_registrazione.*
import kotlinx.android.synthetic.main.fragment_loading.*
import kotlinx.android.synthetic.main.top_area_menu.*
import org.json.JSONObject

class RegistrazioneActivity : MainActivity() {

    override fun onCreateAfterParent() {
        setContentView(R.layout.activity_registrazione)
        screeName = TagNames.Screen.REGISTRATION_SCREEN
        //first_login = intent.getBooleanExtra("firstStart",false)

        /*registration_question1_checkbox.setOnClickListener {
            it.isSelected = !it.isSelected

            if (it.isSelected){
                showPrivacyAction(null)
            }
        }*/
    }

    override fun hasLogo() : Boolean {
        return false
    }

    override fun showGroupLogo() {
    }

    override fun onResume() {
        super.onResume()

        if (firstStart){
            Manager.getProfileInformations { error, message, info ->
                if (info != null) {
                    register_nomeField.setText(info.getString("first_name"))
                    register_cognomelField.setText(info.getString("last_name"))

                    register_emailField.setText(Utils.getOriginalMail(info.getString("email")))
                    register_nomesaloneField.setText(
                        (info.getJSONArray("venue").getJSONObject(0)).optString("name")
                    )
                    register_telefonoField.setText(if (!info.isNull("tel")) info.getString("tel") else "")
                    //register_passwordField.setText("12345678")
                    //register_confirmField.setText("12345678")
                    register_codiceCliente.setText(info.getString("customer_code"))
                    registration_privacy_checkbox.isSelected = false
                    registration_terms_checkbox.isSelected = false
                    registration_clausole_checkbox.isSelected = false

                    firstStart = false
                }
            }
        }
    }

    fun registerAction(view: View?) {
        hideKeyboard()

        register_nomeField.setText(register_nomeField.text.trim())

        if (register_nomeField.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo nome obbligarorio") {

            }

            return
        }

        register_cognomelField.setText(register_cognomelField.text.trim())

        if (register_cognomelField.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo cognome obbligarorio") {

            }

            return
        }

        register_emailField.setText(register_emailField.text.trim())
        if (register_emailField.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo email obbligatorio") {

            }

            return
        }

        if (!register_emailField.text.toString().isValidEmail()){

            showAlert(getString(R.string.errore),"Campo email non valido") {

            }

            return
        }

        if (!register_telefonoField.text.toString().isValidPhone()){

            showAlert(getString(R.string.errore),"Campo telefono non valido") {

            }

            return
        }

       /* if (!register_codiceCliente.text.toString().isValidPhone()){

            showAlert(getString(R.string.errore),"Campo codice cliente obbligatorio") {

            }

            return
        }*/

        /*register_passwordField.setText(register_passwordField.text.trim())

        if (register_passwordField.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo password obbligatorio") {

            }

            return
        }

        if (register_passwordField.text.length<8){

            showAlert(getString(R.string.errore),"Il campo password deve contenere almeno 8 caratteri") {

            }

            return
        }

        register_confirmField.setText(register_confirmField.text.trim())

        if (register_confirmField.text.isNullOrBlank()){

            showAlert(getString(R.string.errore),"Campo conferma password obbligatorio") {

            }

            return
        }

        if (register_passwordField.text.toString()!=register_confirmField.text.toString()){
            showAlert(getString(R.string.errore),"Le password non combaciano") {

            }

            return
        }*/

        if (register_nomesaloneField.text.trim().length<3){

            showAlert(getString(R.string.errore),"Il campo nome salone deve essere compilato") {

            }

            return
        }

        //OK PROCEDE

        if (!registration_privacy_checkbox.isSelected){
            showAlert(getString(R.string.errore),"È necessario leggere e approvare le norme sulla privacy") {

            }
            return
        }

        if (!registration_terms_checkbox.isSelected){
            showAlert(getString(R.string.errore),"È necessario leggere e acconsentire ai termini di utilizzo") {

            }
            return
        }

        if (!registration_clausole_checkbox.isSelected){
            showAlert(getString(R.string.errore),"È necessario leggere e acconsentire alle clausole") {

            }
            return
        }

        var user: JSONObject = JSONObject()
        user.apply {
            put("first_name", register_nomeField.text.toString())
            put("last_name", register_cognomelField.text.toString())
            put("email", register_emailField.text.toString())
            //put("venue",register_nomesaloneField.text.toString())
            //put("customer_code",register_codiceCliente.text.toString())
            put("tel",register_telefonoField.text.toString())
            //put("password", register_passwordField.text.toString())
        }

        Manager.register(user) { error, message ->

            if (error && message != null) {
                askQuestion(
                    getString(R.string.errore),
                    message,
                    getString(R.string.riprova),
                    getString(R.string.annulla)
                ) { confirmed ->
                    if (confirmed) registerAction(view)
                    else {
                        loadingOverlay?.visibility = View.GONE
                    }
                }
            } else {
                //ok
                loadingOverlay?.visibility = View.GONE

                showAlert(
                    getString(R.string.warning),
                    "Informazioni aggiornate correttamente, accedi nuovamente per completare l'operazione."
                ) {
                    val properties = Properties()
                    properties["email"] = user.get("email")
                    trackEvent(TagNames.REGISTRATION_COMPLETE, properties)
                    finish()
                }

                /*if (firstStart) {
                    val intent = Intent(this, PostHomeActivity::class.java).apply {

                    }
                    startActivity(intent)
                }
                else {
                    finish() //go back to login
                }*/
            }
        }
    }

    fun acceptPrivacy(view:View?){
        registration_privacy_checkbox.isSelected = !registration_privacy_checkbox.isSelected
    }

    fun acceptTerms(view:View?){
        registration_terms_checkbox.isSelected = !registration_terms_checkbox.isSelected
    }

    fun acceptClausole(view:View?){
        registration_clausole_checkbox.isSelected = !registration_clausole_checkbox.isSelected
    }

    fun showPrivacy(view: View?) {

        registration_privacy_checkbox.isSelected = true

        val intent = Intent(this, PrivacyActivity::class.java).apply {
            setFlags(intent)
            putExtra("url","https://www.iubenda.com/privacy-policy/75745741")
        }
        startActivity(intent)
    }

    fun showTerms(view: View?) {

        registration_terms_checkbox.isSelected = true

        val intent = Intent(this, PrivacyActivity::class.java).apply {
            setFlags(intent)
            putExtra("url","https://www.bsidecomm.com/termini-e-condizioni-salogram/")
        }
        startActivity(intent)
    }

    fun showClausole(view: View?) {

        registration_clausole_checkbox.isSelected = true

        val intent = Intent(this, PrivacyActivity::class.java).apply {
            setFlags(intent)
            putExtra("url","https://www.bsidecomm.com/clausole-salogram/")
        }
        startActivity(intent)
    }
}
