package it.ghd.salogram

import android.content.Context
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.util.Log
import android.widget.ImageButton
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.request.transition.TransitionFactory
import it.ghd.salogram.CustomApplication.Companion.context


class ImageManager(context: Context) {
    companion object{
        val factory = DrawableTransitionOptions.with(DrawableAlwaysCrossFadeFactory())

        fun loadImage(url:String?,
                      targetImage:ImageView,
                      skipCache:Boolean? = false,
                      scaleType:ImageView.ScaleType = ImageView.ScaleType.FIT_START,
                      errorScaleType:ImageView.ScaleType = ImageView.ScaleType.FIT_CENTER){

            if (url == null) return

            val url1 = url.replace("stage.salogram.com", "salogram.com")

            Log.v("PAOLO", "scarico foto " + url1)

            Glide
                .with(context)
                .load(url1)
                .fitCenter()
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {

                        Log.v("PAOLO", "errore foto")
                        targetImage.scaleType = errorScaleType
                        //targetImage.drawable = target.

                       return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {

                        targetImage.scaleType = scaleType

                        return false
                    }

                })
                .transition(factory)
                //.placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                //.fallback(R.mipmap.ic_launcher)
                .skipMemoryCache(skipCache ?: false)
                .into(targetImage)
        }


        fun loadImage(url:String?, target:ImageButton){
            Glide.with(context).load(url).into(target)
        }

        fun loadImageFill(url:String?, target:ImageButton){
            Glide.with(context).load(url).centerCrop().into(target)
        }

        fun loadImageFromDownloadFolder(name:String?, target:ImageView){
            //todo Glide.with(context).load(url).into(target)
        }
    }

    class DrawableAlwaysCrossFadeFactory : TransitionFactory<Drawable> {
        private val resourceTransition: DrawableCrossFadeTransition = DrawableCrossFadeTransition(300, true) //customize to your own needs or apply a builder pattern
        override fun build(dataSource: DataSource?, isFirstResource: Boolean): Transition<Drawable> {
            return resourceTransition
        }
    }
}