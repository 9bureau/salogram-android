package it.ghd.salogram

interface AppLifeCycleListener {
    fun onAppForeground()
    fun onAppBackground()
}