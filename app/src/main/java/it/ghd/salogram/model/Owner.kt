package it.ghd.salogram.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import kotlin.random.Random

data class Owner(
    @SerializedName("main_email")
    val email : String,
    @SerializedName("owner_main")
    val main : Int,
    @SerializedName("owner_name")
    val name : String,
    @SerializedName("owner_slug")
    val slug : String,
    @SerializedName("owner_id")
    val id : Int
)
{

    fun getDarkPicture() : String {
        return "https://salogram.com/web/logos/dark/${slug}.png?time=" + Random.nextInt(0, Int.MAX_VALUE)
    }
    
    fun getLightPicture() : String {
        return "https://salogram.com/web/logos/${slug}.png?time=" + Random.nextInt(0, Int.MAX_VALUE)
    }

    fun getJSON() : JSONObject {

        return JSONObject().apply {
            put("id", id)
            put("slug", slug)
            put("name", name)
            put("main", main)
            put("email", email)
        }
    }

    companion object {
        fun parse(json : JSONObject?) : Owner? {
            if (json == null) return null
            return Gson().fromJson(json.toString(), Owner::class.java)
        }
    }
}