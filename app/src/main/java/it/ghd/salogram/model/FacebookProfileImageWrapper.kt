package it.ghd.salogram.model

data class FacebookProfileImageWrapper(
    val data: FacebookProfileImage
)
