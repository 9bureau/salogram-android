package it.ghd.salogram.model

import java.util.*

open class PostItem (
    var id : String,
    var name: String,
    var caption: String,
    var type: String,
    var url: String,
    var filename: String,
    var mime: String,
    var thumb_url: String,
    var thumb_filename: String,
    var thumb_mime : String,
    var created_at : String,
    var updated_at: String,
    var category_id : Int
) {
    fun isVideo(): Boolean {
        return url.lowercase(Locale.getDefault()).contains("mp4")
    }

    fun isImage(): Boolean {
        return !url.lowercase(Locale.getDefault()).contains("mp4")
    }

    fun isPost(): Boolean {
        return type == "post"
    }

    fun isStories(): Boolean {
        return type == "story"
    }

    fun getMediaType(): String {
        return if (isVideo()) "video" else "image"
    }

    fun getItemType(): String {
        return if (isPost()) "post" else "stories"
    }
}
