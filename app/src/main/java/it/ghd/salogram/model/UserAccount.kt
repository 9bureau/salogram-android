package it.ghd.salogram.model

import com.google.gson.annotations.SerializedName
import kotlin.random.Random

data class UserAccount(
    @SerializedName("id")
    val id : Int,
    @SerializedName("group_mail")
    val groupMail : String,
    @SerializedName("name")
    val goupName : String,
    @SerializedName("slug")
    val groupSlug : String,
    @SerializedName("owner")
    val owner : Int,
    @SerializedName("main")
    val main : Boolean
)
{
    fun getPicture() : String {
        return "https://salogram.com/web/logos/${groupSlug}.png?time=" + Random.nextInt(0, Int.MAX_VALUE)
    }
}