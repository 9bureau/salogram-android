package it.ghd.salogram.model

open class BaseLaravelResponse (
    open val status: String,
    open val message: String,
    open val status_code: Int
)
