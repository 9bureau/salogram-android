package it.ghd.salogram.model

data class FacebookRegistrationResponse(
    val status : String,
    val message : String,
    val status_code : Int,
    val data : FacebookDataResponse
)
{

}