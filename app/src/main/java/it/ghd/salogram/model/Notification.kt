package it.ghd.salogram.model

class Notification(
    var owners: List<Int>?,
    var owners_slug : List<String>?,
    var postShares: Int,
    id : String,
    name: String,
    caption: String,
    type: String,
    url: String,
    filename: String,
    mime: String,
    thumb_url: String,
    thumb_filename: String,
    thumb_mime : String,
    created_at : String,
    updated_at: String,
    category_id : Int

) : PostItem(
    id,
    name,
    caption,
    type,
    url,
    filename,
    mime,
    thumb_url,
    thumb_filename,
    thumb_mime,
    created_at,
    updated_at,
    category_id
)
{



}