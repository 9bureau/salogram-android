package it.ghd.salogram.model

data class FacebookError (
    var message : String, // There was a problem uploading your video file. Please try again
    var type : String, // OAuthException
    var code :Int,// OAuthException
    var error_subcode : Int, // OAuthException
    var is_transient : Boolean, // OAuthException
    var error_user_title : String, // Tempo di caricamento del video scaduto
    var error_user_msg : String, // Il tempo di caricamento del video \\u00e8 scaduto prima del completamento. Il motivo potrebbe essere una connessione di rete lenta o le eccessive dimensioni del video che stai cercando di caricare. Riprova.
    var fbtrace_id : String, // AENjpUoGipW_ynEZV-Wpk5i
)