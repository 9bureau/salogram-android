package it.ghd.salogram.model

data class FacebookDataResponse(
    var user: FacebookPage?,
    var pages: List<FacebookPage>
){
    fun getPageNameArray() : List<String>{
        var lista = ArrayList<String>()
        pages.forEach {
            lista.add(it.item_name)
        }
        return lista
    }
}
