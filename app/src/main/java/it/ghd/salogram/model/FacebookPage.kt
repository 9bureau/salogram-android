package it.ghd.salogram.model

import org.json.JSONObject

class FacebookPage (var item_id: String, var item_name: String,  var item_pic : String, var expired: Boolean = false, var selected: Boolean = false) {

    var active: Boolean = false
        get() = selected
        set(value) {
            field = value
            selected = value
        }

    fun getJSON() : JSONObject {
        return JSONObject().apply {
            put("item_id", item_id)
            put("item_name", item_name)
            put("item_pic", item_pic)
            put("selected", "selected")
        }
    }
}
