package it.ghd.salogram.model

data class FacebookProfileImage(
    val height : Int,
    val width : Int,
    val is_silhouette : Boolean,
    val url : String
)
