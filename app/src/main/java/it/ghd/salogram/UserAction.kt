package it.ghd.salogram

enum class UserAction {
    SHARE,
    DOWNLOAD,
    NONE
}