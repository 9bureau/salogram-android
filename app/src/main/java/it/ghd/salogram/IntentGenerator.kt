package it.ghd.salogram

import android.content.ClipData
import android.content.Intent
import android.net.Uri
import it.ghd.salogram.fragments.EditorMainFragment

class IntentGenerator {

    companion object {

        /*
        fun getFacebookStoryIntent(uri: Uri, message: String, isVideo: Boolean) : Intent {

            val packageID = "com.facebook.stories.ADD_TO_STORY"

            val intent = Intent(packageID)
            intent.setDataAndType(backgroundAssetUri, MEDIA_TYPE_JPEG)
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            intent.putExtra("com.facebook.platform.extra.APPLICATION_ID", appId)

            return intent
        }
        */

        fun getInstagramIntent(uri: Uri, message: String, isVideo: Boolean, isStory: Boolean) : Intent {

            //intent storie
            if (isStory) {
                val storiesIntent = Intent("com.instagram.share.ADD_TO_STORY")
                //storiesIntent.setPackage(packageID)
                val type = if (isVideo) "video/mp4" else "image/*"
                storiesIntent.setDataAndType(uri, type)
                storiesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                //storiesIntent.putExtra(Intent.EXTRA_STREAM, uri)
                //postIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf<Intent>(storiesIntent))
                return storiesIntent
            }
            else {
                //intent post
                val packageID = "com.instagram.android"
                val postIntent = Intent(Intent.ACTION_SEND)
                postIntent.setPackage(packageID)

                postIntent.type = if (isVideo) "video/mp4" else "image/*"
                //postIntent.putExtra(Intent.EXTRA_TEXT, message)
                //postIntent.putExtra(Intent.EXTRA_TITLE, message)
                postIntent.putExtra(Intent.EXTRA_STREAM, uri)
                return postIntent
            }
            
        }

        fun getWhatsappIntent(uri: Uri, message: String, isVideo: Boolean) : Intent {

            val packageID = "com.whatsapp"
            //val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
            val intent = Intent(Intent.ACTION_SEND)
            intent.setPackage(packageID)
            //intent.type = if (isVideo) "video/mp4" else "image/*"
            intent.type = if (isVideo) "video/*" else "image/*"
            intent.putExtra(Intent.EXTRA_TEXT, message)
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            return intent
        }

        fun getGenericIntent(uri: Uri, message: String, isVideo: Boolean) : Intent {

            val clipData = ClipData.newRawUri("Image", uri)

            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = if (isVideo) "video/mp4" else "image/*"
            shareIntent.clipData = clipData
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            shareIntent.putExtra(
                Intent.EXTRA_TEXT, Utils.sharefPrefs.getString(
                    EditorMainFragment.CAPTION_TEXT,
                    ""
                )
            )

            return shareIntent
        }

        fun getGenericIntentNoClip(uri: Uri, message: String, isVideo: Boolean) : Intent {

            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = if (isVideo) "video/mp4" else "image/*"
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
            shareIntent.putExtra(
                Intent.EXTRA_TEXT, Utils.sharefPrefs.getString(
                    EditorMainFragment.CAPTION_TEXT,
                    ""
                )
            )

            return shareIntent
        }
    }
}