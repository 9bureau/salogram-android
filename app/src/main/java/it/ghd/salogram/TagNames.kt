package it.ghd.salogram

object TagNames {

    object Screen {
        const val ACADEMY_SCREEN                    = "ACADEMY"
        const val EDITOR_SCREEN                     = "EDITOR"
        const val FORGOT_PASSWORD_SCREEN            = "FORGOT_PASSWORD"
        const val GUIDE_SCREEN                      = "GUIDE"
        const val HOME_SCREEN                       = "HOME"
        const val LOGIN_SCREEN                      = "LOGIN"
        const val NOTIFICHE_SCREEN                  = "NOTIFICHE"
        const val PAGE_POST_PREVIEW_SCREEN          = "PAGE_POST_PREVIEW"
        const val POST_CATEGORIES_LIST_SCREEN       = "POST_CATEGORIES_LIST"
        const val POST_HOME_SCREEN                  = "POST_HOME"
        const val POST_LIST_SCREEN                  = "POST_LIST"
        const val PREFERITI_SCREEN                  = "PREFERITI"
        const val PRIVACY_SCREEN                    = "PRIVACY"
        const val PROFILE_SCREEN                    = "PROFILE"
        const val PROFILE_EDIT_SCREEN               = "PROFILE_EDIT"
        const val REGISTRATION_SCREEN               = "REGISTRATION"
        const val STORIES_CATEGORIES_LIST_SCREEN    = "STORIES_CATEGORIES_LIST"
        const val STORIES_HOME_SCREEN               = "STORIES_HOME"
        const val STORIES_LIST_SCREEN               = "STORIES_LIST"
        const val TERMS_SCREEN                      = "TERMS"
    }

    const val IMMEDIATE_APP_UPDATE_REQUEST = "IMMEDIATE_APP_UPDATE_REQUEST"
    const val IMMEDIATE_APP_UPDATE_SUCCESS = "IMMEDIATE_APP_UPDATE_SUCCESS"
    const val IMMEDIATE_APP_UPDATE_FAIL = "IMMEDIATE_APP_UPDATE_FAIL"

    const val PHONE_NUMBER_CONFIRM_REQUEST = "PHONE_NUMBER_CONFIRM_REQUEST"
    const val PHONE_NUMBER_CONFIRMED = "PHONE_NUMBER_CONFIRMED"

    const val BACKGROUNDED_APP = "BACKGROUNDED_APP"
    const val FOREGROUNDED_APP = "FOREGROUNDED_APP"
    const val REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE"
    const val TAP_ON_BACK_BUTTON    = "TAP_ON_BACK_BUTTON"
    const val APPLICATION_STARTED       = "APPLICATION_STARTED"

    const val LOGO_SELECTED = "LOGO_SELECTED"
    const val LOGO_UPDATED  = "LOGO_UPDATED"
    const val SHARE_DATA_KEY = "SHARE_DATA"

    const val ACTION_EDIT_COPY_SAVED    = "ACTION_COPY_SAVED"
    const val ACTION_COPY_EDITED        = "ACTION_COPY_EDITED"
    const val ACTION_EDIT_COPY          = "ACTION_EDIT_COPY_INTENT"

    const val TEXT_FONTSIZE_INCREASE = "TEXT_FONTSIZE_INCREASE"
    const val TEXT_FONTSIZE_DECREASE = "TEXT_FONTSIZE_DECREASE"
    const val TEXT_COLOR_SELECTED = "TEXT_COLOR_SELECTED"
    const val TEXT_FONT_SELECTED = "TEXT_FONT_SELECTED"

    const val USER_LOGIN        = "USER_LOGIN"
    const val USER_LOGOUT       = "USER_LOGOUT"
    const val PROFILE_UPDATE    = "USER_PROFILE_UPDATED"
    const val MULTI_LOGIN       = "USER_MULTI_LOGIN"
    const val SINGLE_LOGIN      = "USER_SINGLE_LOGIN"
    const val USER_NOT_FIRST_ACCESS = "USER_NOT_FIRST_ACCESS"
    const val USER_FIRST_ACCESS     = "USER_FIRST_ACCESS"

    //const val NOTIFICATION = "NOTIFICATION_ACTION"

    const val FACEBOOK_LOGIN        = "FACEBOOK_USER_LOGIN"
    const val FACEBOOK_LOGOUT       = "FACEBOOK_USER_LOGOUT"
    const val FACEBOOK_PAGE_LINK    = "FACEBOOK_PAGE_LINKED"
    const val FACEBOOK_PAGE_UNLINK  = "FACEBOOK_PAGE_UNLINKED"

    const val PASSWORD_RESET            = "PASSWORD_RESET_REQUEST"
    const val CHANGE_GROUP_ACTION       = "CHANGE_GROUP_ACTION"

    const val ACTION_POST_CUSTOMIZATION = "ACTION_POST_CUSTOMIZATION"
    const val ACTION_EDIT_TEXT          = "EDIT_TEXT_INTENT"
    const val ACTION_ADD_FRAME          = "ADD_FRAME_INTENT"
    const val ACTION_ADD_LOGO           = "ADD_LOGO_INTENT"
    const val ADD_TO_FAVOURITE          = "ADDED_TO_FAVOURITE"

    const val REMOVE_FROM_FAVOURITE = "REMOVED_FROM_FAVOURITE"
    const val PLAY_VIDEO = "VIDEO_PLAYED"
    const val STOP_VIDEO = "VIDEO_STOPPED"

    const val GROUP_MAIL_CREATED_FOR_NOTIFICATION = "GROUP_MAIL_CREATED_FOR_NOTIFICATION"
    const val GROUP_MAIL_NOT_CREATED_FOR_NOTIFICATION = "GROUP_MAIL_NOT_CREATED_FOR_NOTIFICATION"
    const val FOUND_NOTIFICATION_ON_PREFERENCES = "FOUND_NOTIFICATION_ON_PREFERENCES"
    const val NOT_FOUND_NOTIFICATION_ON_PREFERENCES = "NOT_FOUND_NOTIFICATION_ON_PREFERENCES"
    const val FOUND_WRONG_NOTIFICATION_ON_PREFERENCES = "FOUND_WRONG_NOTIFICATION_ON_PREFERENCES"

    object ShareAction {

        const val MISSING_APP_ERROR = "SHARE_ERROR_MISSING_APP"
        //generici
        const val START    = "SHARE_ACTION_START"
        const val END      = "SHARE_ACTION_END"
        const val FAIL     = "SHARE_ACTION_FAILED"
        const val CANCEL   = "SHARE_ACTION_CANCELLED"
        const val UNKNOWN   = "SHARE_ACTION_END_UNKNOWN"

        //specifici
        object Start {
            const val FB_PAGE       = "SHARE_ACTION_START_FB_PAGE"
            const val FB_PROFILE    = "SHARE_ACTION_START_FB_PROFILE"
            const val INSTAGRAM     = "SHARE_ACTION_START_INSTAGRAM"
            const val WHATSAPP      = "SHARE_ACTION_START_WHATSAPP"
            const val TELEGRAM      = "SHARE_ACTION_START_TELEGRAM"
            const val TWITTER       = "SHARE_ACTION_START_TWITTER"
            const val MESSENGER     = "SHARE_ACTION_START_MESSENGER"
            const val OTHER         = "SHARE_ACTION_START_OTHER"
            const val DOWNLOAD      = "SHARE_ACTION_START_DOWNLOAD"
            const val NONE          = "SHARE_ACTION_START_NONE"
        }

        object End {

            object Cancel {
                const val FB_PAGE    = "SHARE_ACTION_CANCEL_FB_PAGE"
                const val FB_PROFILE = "SHARE_ACTION_CANCEL_FB_PROFILE"
                const val INSTAGRAM  = "SHARE_ACTION_CANCEL_INSTAGRAM"
                const val WHATSAPP   = "SHARE_ACTION_CANCEL_WHATSAPP"
                const val TELEGRAM   = "SHARE_ACTION_CANCEL_TELEGRAM"
                const val TWITTER    = "SHARE_ACTION_CANCEL_TWITTER"
                const val MESSENGER  = "SHARE_ACTION_CANCEL_MESSENGER"
                const val OTHER      = "SHARE_ACTION_CANCEL_OTHER"
                const val DOWNLOAD   = "SHARE_ACTION_CANCEL_DOWNLOAD"
            }

            object Fail {
                const val FB_PAGE    = "SHARE_ACTION_FAIL_FB_PAGE"
                const val FB_PROFILE = "SHARE_ACTION_FAIL_FB_PROFILE"
                const val INSTAGRAM  = "SHARE_ACTION_FAIL_INSTAGRAM"
                const val WHATSAPP   = "SHARE_ACTION_FAIL_WHATSAPP"
                const val TELEGRAM   = "SHARE_ACTION_FAIL_TELEGRAM"
                const val TWITTER    = "SHARE_ACTION_FAIL_TWITTER"
                const val MESSENGER  = "SHARE_ACTION_FAIL_MESSENGER"
                const val OTHER      = "SHARE_ACTION_FAIL_OTHER"
                const val DOWNLOAD   = "SHARE_ACTION_FAIL_DOWNLOAD"
            }

            object Success {
                const val FB_PAGE    = "SHARE_ACTION_SUCCESS_FB_PAGE"
                const val FB_PROFILE = "SHARE_ACTION_SUCCESS_FB_PROFILE"
                const val INSTAGRAM  = "SHARE_ACTION_SUCCESS_INSTAGRAM"
                const val WHATSAPP   = "SHARE_ACTION_SUCCESS_WHATSAPP"
                const val TELEGRAM   = "SHARE_ACTION_SUCCESS_TELEGRAM"
                const val TWITTER    = "SHARE_ACTION_SUCCESS_TWITTER"
                const val MESSENGER  = "SHARE_ACTION_SUCCESS_MESSENGER"
                const val OTHER      = "SHARE_ACTION_SUCCESS_OTHER"
                const val DOWNLOAD   = "SHARE_ACTION_SUCCESS_DOWNLOAD"
            }

            object Unknown {
                //const val FB_PAGE    = "SHARE_ACTION_UNKNOWN_FB_PAGE"
                //const val FB_PROFILE = "SHARE_ACTION_UNKNOWN_FB_PROFILE"
                const val INSTAGRAM  = "SHARE_ACTION_UNKNOWN_INSTAGRAM"
                const val WHATSAPP   = "SHARE_ACTION_UNKNOWN_WHATSAPP"
                const val TELEGRAM   = "SHARE_ACTION_UNKNOWN_TELEGRAM"
                const val TWITTER    = "SHARE_ACTION_UNKNOWN_TWITTER"
                const val MESSENGER  = "SHARE_ACTION_UNKNOWN_MESSENGER"
                const val OTHER      = "SHARE_ACTION_UNKNOWN_OTHER"
                //const val DOWNLOAD   = "SHARE_ACTION_UNKNOWN_DOWNLOAD"
            }
        }
    }

    const val DOWNLOAD_ACTION   = "DOWNLOAD_ACTION"
    const val PARAM_ESITO       = "RESULT"

    const val ITEM_TYPE_POST        = "POST"
    const val ITEM_TYPE_STORIES     = "STORIES"
    const val ITEM_TYPE_TUTORIAL    = "TUTORIAL"

    const val FRAME_ADDED       = "FRAME_ADDED"
    const val FRAME_NOT_ADDED   = "FRAME_NOT_ADDED"
    const val FRAME_SELECTED    = "FRAME_SELECTED"

    const val LOGO_ADDED        = "LOGO_ADDED"
    const val LOGO_NOT_ADDED    = "LOGO_NOT_ADDED"

    const val TABBAR_TAP_POST           = "TABBAR_TAP_POST"
    const val TABBAR_TAP_STORIES        = "TABBAR_TAP_STORIES"
    const val TABBAR_TAP_ACADEMY        = "TABBAR_TAP_ACADEMY"
    const val TABBAR_TAP_FAVOURITES     = "TABBAR_TAP_FAVOURITES"
    const val TABBAR_TAP_PROFILE        = "TABBAR_TAP_PROFILE"
    const val TABBAR_TAP_PROFILE_EDIT   = "TABBAR_TAP_PROFILE"

    const val TAP_ON_POST_CATEGORY_FROM_HOME    = "TAP_ON_POST_CATEGORY_FROM_HOME"
    const val TAP_ON_NEW_POST_FROM_HOME         = "TAP_ON_NEW_POST_FROM_HOME"
    const val TAP_ON_POPULAR_POST_FROM_HOME     = "TAP_ON_POPULAR_POST_FROM_HOME"
    const val TAP_ON_POST_WEEK_FROM_HOME        = "TAP_ON_POST_WEEK_FROM_HOME"
    const val TAP_ON_POST_FROM_FAVOURITES   = "TAP_ON_POST_FROM_FAVOURITES"
    const val TAP_ON_POST = "TAP_ON_POST"

    const val TAP_ON_POST_FROM_LIST = "TAP_ON_POST_FROM_LIST"
    const val TAP_ON_STORIES_FROM_LIST = "TAP_ON_STORIES_FROM_LIST"

    const val TAP_ON_STORIES_CATEGORY_FROM_CATEGORY = "TAP_ON_STORIES_CATEGORY_FROM_CATEGORY"
    const val TAP_ON_STORIES_SUBCATEGORY            = "TAP_ON_STORIES_SUBCATEGORY"

    const val TAP_ON_POST_CATEGORY_FROM_CATEGORY = "TAP_ON_POST_CATEGORY_FROM_CATEGORY"
    const val TAP_ON_POST_SUBCATEGORY            = "TAP_ON_POST_SUBCATEGORY"

    const val TAP_ON_STORIES_CATEGORY_FROM_HOME    = "TAP_ON_STORIES_CATEGORY_FROM_HOME"
    const val TAP_ON_NEW_STORIES_FROM_HOME         = "TAP_ON_NEW_STORIES_FROM_HOME"
    const val TAP_ON_POPULAR_STORIES_FROM_HOME     = "TAP_ON_POPULAR_STORIES_FROM_HOME"
    const val TAP_ON_STORIES_WEEK_FROM_HOME        = "TAP_ON_STORIES_WEEK_FROM_HOME"
    const val TAP_ON_STORIES = "TAP_ON_STORIES"

    const val TAP_ON_ITEM = "TAP_ON_ITEM"

    const val TAP_ON_CATEGORY_FROM_POST_LIST    = "TAP_ON_CATEGORY_FROM_POST_LIST"
    const val TAP_ON_CATEGORY_FROM_STORIES_LIST = "TAP_ON_CATEGORY_FROM_STORIES_LIST"

    const val TAP_ON_NOTIFICATION_OPEN_URL      = "TAP_ON_NOTIFICATION_OPEN_URL"
    const val TAP_ON_NOTIFICATION_OPEN_POST     = "TAP_ON_NOTIFICATION_OPEN_POST"

    const val CHANGE_FB_PAGE_FROM_PROFILE       = "CHANGE_FB_PAGE_FROM_PROFILE"
    const val SCROLL_RIGHT_POST_CATEGORIES_HOME = "SCROLL_RIGHT_POST_CATEGORIES_HOME"
    const val SCROLL_LEFT_POST_CATEGORIES_HOME  = "SCROLL_LEFT_POST_CATEGORIES_HOME"

    const val POST_HOME_REFRESH             = "POST_HOME_REFRESH"
    const val POST_LIST_REFRESH             = "POST_LIST_REFRESH"
    const val STORIES_HOME_REFRESH          = "STORIES_HOME_REFRESH"
    const val STORIES_LIST_REFRESH          = "STORIES_LIST_REFRESH"
    const val ACADEMY_VIDEO_REFRESH         = "ACADEMY_VIDEO_REFRESH"
    const val FAVOURITES_POST_LIST_REFRESH  = "FAVOURITES_POST_LIST_REFRESH"
    const val NOTIFICATION_LIST_REFRESH     = "NOTIFICATION_LIST_REFRESH"

    const val EXPORT_EVENT                  = "EXPORT_EVENT"
    const val SHARE_PANEL_OPEN              = "SHARE_PANEL_OPEN"
    const val SHARE_PANEL_CLOSE             = "SHARE_PANEL_CLOSE"

    const val GROUP_CHANGE_MENU_OPEN        = "GROUP_CHANGE_MENU_OPEN"
    const val GROUP_CHANGE_MENU_CLOSE       = "GROUP_CHANGE_MENU_CLOSE"

    const val START_GROUP_CHOICE_MODAL_OPEN       = "START_GROUP_CHOICE_MODAL_OPEN"
    const val START_GROUP_CHOICE_MODAL_CLOSE       = "START_GROUP_CHOICE_MODAL_CLOSE"

    const val TAP_ON_FORGOT_PASSWORD_ICON   = "TAP_ON_FORGOT_PASSWORD_ICON"
    const val REDIRECT_TO_REGISTRATION      = "REDIRECT_TO_REGISTRATION"
    const val TAP_ON_NOTIFICATION_ICON      = "TAP_ON_NOTIFICATION_ICON"
}