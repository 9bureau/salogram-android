package it.ghd.salogram

import android.util.Log
import com.segment.analytics.Analytics
import it.ghd.salogram.activities.MainActivity

class ProcessLifeCycleListener : AppLifeCycleListener {
    override fun onAppForeground() {
        Log.d("STEPS", "App in foreground")
        MainActivity.trackEvent(TagNames.FOREGROUNDED_APP, null)
    }

    override fun onAppBackground() {
        Log.d("STEPS", "App in background")
        MainActivity.trackEvent(TagNames.BACKGROUNDED_APP, null)
    }
}