package it.ghd.salogram

enum class ShareDestination {
    FACEBOOK_PROFILE,
    FACEBOOK_PAGE,
    INSTAGRAM,
    MESSENGER,
    WHATSAPP,
    OTHER,
    TWITTER,
    TELEGRAM,
    DOWNLOAD,
    NONE
}