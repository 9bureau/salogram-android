package it.ghd.salogram

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import it.ghd.salogram.BuildConfig.DEBUG
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {

        fun createKeyHash(activity: Activity, yourPackage: String) {
           /*
            val info = activity.packageManager.getPackageInfo(yourPackage, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())

                val encodedString: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Base64.getEncoder().encodeToString(md.digest()) //.toByteArray()

                } else {
                    TODO("VERSION.SDK_INT < O")
                }
                Log.d("FACEBOOKPAOLO:","encodedString")
                Log.d("FACEBOOKPAOLO:",encodedString)

            }
            */
        }

        @SuppressLint("ObsoleteSdkInt")
        fun isValidEmail(email: String): Boolean {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                !TextUtils.isEmpty(email.trim { it <= ' ' }) && Patterns.EMAIL_ADDRESS.matcher(
                    email
                ).matches()
            } else true
        }

        var oneSignalUserID: String ?
            get() = sharefPrefs.getString(SHARED_PREFERENCES_ONE_SIGNAL_USER_ID_FIELD,"null")
            set(value){
                value?.let {
                    sharefPrefsEditor.putString(SHARED_PREFERENCES_ONE_SIGNAL_USER_ID_FIELD, it)
                    sharefPrefsEditor.commit()
                    Utils.log("Setting One Signal USER_ID $it")
                }?: run {
                    sharefPrefsEditor.remove(SHARED_PREFERENCES_ONE_SIGNAL_USER_ID_FIELD)
                    sharefPrefsEditor.commit()
                }
            }

        var pushToken: String?
            get() = sharefPrefs.getString(SHARED_PREFERENCES_PUSH_TOKEN_FIELD,"null")
            set(value){
                value?.let {
                    sharefPrefsEditor.putString(SHARED_PREFERENCES_PUSH_TOKEN_FIELD, it)
                    sharefPrefsEditor.commit()
                    Utils.log("Setting TOKEN $it")
                }?: run {
                    sharefPrefsEditor.remove(SHARED_PREFERENCES_PUSH_TOKEN_FIELD)
                    sharefPrefsEditor.commit()
                }
            }

        var appToken: String?
            get() = sharefPrefs.getString(SHARED_PREFERENCES_TOKEN_FIELD,"null")
            set(value){
                value?.let {
                    sharefPrefsEditor.putString(SHARED_PREFERENCES_TOKEN_FIELD, it)
                    sharefPrefsEditor.commit()
                }?: run {
                    sharefPrefsEditor.remove(SHARED_PREFERENCES_TOKEN_FIELD)
                    sharefPrefsEditor.commit()
                }
            }

        const val APP_NAME = "Salogram"
        val SHARED_PREFERENCES_NAME = APP_NAME
        var SHARED_PREFERENCES_ONE_SIGNAL_USER_ID_FIELD = "osuid"
        var SHARED_PREFERENCES_PUSH_TOKEN_FIELD = "push"
        var SHARED_PREFERENCES_TOKEN_FIELD = "token"
        val LOG_NAME = APP_NAME

        fun getCleanMail(indirizzo:String) : String {

            if (indirizzo.contains("+")) {
                val mailParts = indirizzo.split("@")
                if (mailParts.size == 2) {
                    val parteLeft = mailParts[0]
                    val parteRight = mailParts[1]
                    val mailSubparts = parteLeft.split("+")
                    if (mailSubparts.size == 2) {
                        val sottoParteLeft = mailSubparts[0]
                        if (mailSubparts.isNotEmpty()) {
                            return "$sottoParteLeft@$parteRight"
                        }
                    }
                }
            }
            return indirizzo
        }

        fun log(message:String){
            Log.d(LOG_NAME,message)
        }

        val isDebugger:Boolean
          get() = DEBUG

        val isEmulator: Boolean
            get() = Build.FINGERPRINT.startsWith("generic")
                    || Build.FINGERPRINT.startsWith("unknown")
                    || Build.MODEL.contains("google_sdk")
                    || Build.MODEL.contains("sdk")
                    || Build.MODEL.contains("Emulator")
                    || Build.MODEL.contains("Android SDK built for x86")
                    || Build.MANUFACTURER.contains("Genymotion")
                    || Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic")
                    || "google_sdk" == Build.PRODUCT

        val sharefPrefs: SharedPreferences
            get() = CustomApplication.context.getSharedPreferences(
                SHARED_PREFERENCES_NAME,
                Context.MODE_PRIVATE
            )
        var sharefPrefsEditor = sharefPrefs.edit()

        // ritorna mail senza suffisso "+GRUPPO"
        fun getOriginalMail(mail:String) : String {
            val pezzi = mail.split("@")
            val left = pezzi.first()
            if (left.contains("+")){
                val subparts = left.split("+")
                if (subparts.isNotEmpty()) {
                    return subparts.first() + "@" + pezzi.last()
                }
            }
            return mail
        }

        fun uriForDownlaodedFile(name:String): Uri? {
            val path = "${CustomApplication.context.getExternalFilesDir(null)?.absolutePath}/salogram/$name"

            val f = File(path) //

            if (!f.exists()){
                return null
            }

            Utils.log(Uri.fromFile(f).path.toString())

            return Uri.fromFile(f)
        }

        fun pathToDownloadFiles():String {
            val folder: File = File(CustomApplication.context.getExternalFilesDir(null), "salogram")
            if (!folder.exists()) {
                folder.mkdirs()
            }

            val path = folder.absolutePath

            return path
        }

    }//companion
}

internal class InternetCheck(private val onInternetChecked: (Boolean) -> Unit) :

    AsyncTask<Void, Void, Boolean>() {
    init {
        execute()
    }

    override fun doInBackground(vararg voids: Void): Boolean {
        return try {
            val sock = Socket()
            sock.connect(InetSocketAddress("8.8.8.8", 53), 1500)
            sock.close()
            true
        } catch (e: IOException) {
            false
        }

    }

    override fun onPostExecute(internet: Boolean) {
        onInternetChecked(internet)
    }
}

//Extensions...
operator fun JSONArray.iterator(): Iterator<JSONObject> = (0 until length()).asSequence().map { get(it) as JSONObject }.iterator()

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun String.isValidEmail() = this.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
fun String.isValidPhone() = this.isNotBlank() && Patterns.PHONE.matcher(this).matches()

fun SharedPreferences.Editor.putJSONObject(key:String, value:JSONObject){
    putString(key,value.toString()).apply()
}

fun SharedPreferences.Editor.putJSONArray(key:String, value:JSONArray){
    putString(key,value.toString()).apply()
}

fun SharedPreferences.Editor.putColor(key:String, value: Int){
    putInt(key,value).apply()
}

fun SharedPreferences.getColor(key:String):Int{
   return getInt(key,0)
}

fun SharedPreferences.getJSONObject(key:String) : JSONObject{
    return JSONObject(this.getString(key,"") ?: "{}")
}

fun SharedPreferences.getJSONArray(key:String) : JSONArray{
    return JSONArray(this.getString(key,""))
}

fun Intent.putExtra(key:String, value:JSONObject) {
    putExtra(key,value.toString())
}

fun Intent.putExtra(key:String, value:JSONArray) {
    putExtra(key,value.toString())
}

fun Intent.getJSONObjectExtra(key:String):JSONObject {
    return JSONObject(getStringExtra(key))
}

fun Intent.getJSONArrayExtra(key:String):JSONArray {
    return JSONArray(getStringExtra(key))
}

fun String.asDate(): String? {
    return try {
        val sdf = SimpleDateFormat("dd MMMM yyy", Locale.ITALY)
        val netDate = Date(this.toLong() * 1000)
        sdf.format(netDate)
    } catch (e: Exception) {
        e.toString()
    }
}

fun Int.asDate():String?{
    return this.toLong().asDate()
}

fun Int.asLongFormatDate():String?{
    return this.toLong().asLongFromatDate()
}

fun Long.asDate():String?{
    return try {
        val sdf = SimpleDateFormat("dd MMMM yyy", Locale.ITALY)
        val netDate = Date(this*1000)
        sdf.format(netDate)
    } catch (e: Exception) {
        e.toString()
    }
}

fun Long.asLongFromatDate():String?{
    return try {
        val sdf = SimpleDateFormat("dd/MM/yy @ HH:mm", Locale.ITALY)
        val netDate = Date(this*1000)
        sdf.format(netDate)
    } catch (e: Exception) {
        e.toString()
    }
}

fun String.asTime(): String? {
    return try {
        val sdf = SimpleDateFormat("HH:mm", Locale.ITALY)
        val netDate = Date(this.toLong()*1000)
        sdf.format(netDate)
    } catch (e: Exception) {
        e.toString()
    }
}

fun File.deleteDirectory(): Boolean {
    return if (exists()) {
        listFiles()?.forEach {
            if (it.isDirectory) {
                it.deleteDirectory()
            } else {
                it.delete()
            }
        }
        delete()
    } else false
}