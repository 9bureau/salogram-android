package it.ghd.salogram.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.divyanshu.colorseekbar.ColorSeekBar
import it.ghd.salogram.*
import it.ghd.salogram.activities.EditorActivity
import it.ghd.salogram.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_editor_text_color.*
import org.json.JSONObject


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class EditorTextColorFragment : EditorMainFragment() {

    private lateinit var param1: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))

        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorTextColorFragment")

        if (param1!=null) {
            //val b: Bitmap = (requireActivity() as EditorActivity).render
            //home_image_background.setImageBitmap(b)
        }

       updateTextBox()

        gradientView.post {
            gradientView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            gradientView.setOnTouchListener(View.OnTouchListener { _, event ->
                if(event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_UP) {
                    if (gradientView.width > 0) {

                       val point:Point  = getRelativePosition(gradientView,event) ?: Point(0,0)

                        val bitmap = Bitmap.createBitmap(
                            gradientView.width,
                            gradientView.height,
                            Bitmap.Config.ARGB_8888
                        )
                        gradientView.draw(Canvas(bitmap))

                        val color = bitmap.getPixel(point.x, point.y).apply {
                            selectColor(this, true)
                        }
                    }

                    true
                }
                false
            })
        }

    }

    private fun getRelativePosition(v: View, event: MotionEvent): Point? {
        val location = IntArray(2)
        v.getLocationOnScreen(location)
        val screenX = event.rawX
        val screenY = event.rawY
        val viewX = screenX - location[0]
        val viewY = screenY - location[1]
        return Point(viewX.toInt(), viewY.toInt())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        selectColor( Utils.sharefPrefs.getColor(FONT_COLOR))

        color_seek_bar.setOnColorChangeListener(object: ColorSeekBar.OnColorChangeListener{
            override fun onColorChangeListener(color: Int) {
                //gives the selected color
                selectColor(color)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_text_color, container, false)
    }

    fun selectColor(color:Int, dontupdategradientview:Boolean = false){
            (requireActivity() as EditorActivity).selectTextWhiteColor(null)
            Utils.sharefPrefsEditor.putColor(FONT_COLOR, color)
        
        updateTextBox()

        if (!dontupdategradientview) {
            val gd = GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM, intArrayOf(
                    ContextCompat.getColor(activity!!, R.color.white),
                    color,
                    ContextCompat.getColor(activity!!, R.color.salogram_black)
                )
            )
            //gd.shape = GradientDrawable.RECTANGLE
            gradientView.background = gd
        }

        MainActivity.trackEvent(TagNames.TEXT_COLOR_SELECTED, null)
    }

    companion object {
        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorTextColorFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }
}