package it.ghd.salogram.fragments

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import it.ghd.salogram.R
import it.ghd.salogram.Utils
import kotlinx.android.synthetic.main.fragment_editor_caption.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

class EditorCaptionFragment : EditorMainFragment() {

    private lateinit var param1: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorCaptionFragment")

        hideKeyboard()

        if (param1!=null) {
            //val b: Bitmap = (requireActivity() as EditorActivity).render
            //home_image_background.setImageBitmap(b)
        }

        val testoIniziale = Utils.sharefPrefs.getString(CAPTION_TEXT, "Nessun testo")
        captionText.text = testoIniziale


        //closeTextButton?.visibility = if (Utils.sharefPrefs.getBoolean(EditorActivity.SHOW_TEXT_BOX,false)) View.VISIBLE else View.GONE
    }

    fun loadStatus(){

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_caption, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorCaptionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }
}