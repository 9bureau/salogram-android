package it.ghd.salogram.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import it.ghd.salogram.activities.EditorActivity
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import it.ghd.salogram.Utils
import it.ghd.salogram.activities.MainActivity
import it.ghd.salogram.fragments.EditorTextFragment.Companion.FONTS
import it.ghd.salogram.getColor
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.editor_text_container.view.*
import kotlinx.android.synthetic.main.editor_text_font_list_element.view.*
import kotlinx.android.synthetic.main.fragment_editor_text_font.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

class EditorTextFontFragment : EditorMainFragment() {
    private lateinit var param1: JSONObject

    var selectedFontIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorTextFontFragment")

        if (param1!=null) {

            editor_text_container.visibility = View.VISIBLE

            //val b: Bitmap = (requireActivity() as EditorActivity).render
            //home_image_background.setImageBitmap(b)

            selectedFontIndex = Utils.sharefPrefs.getInt(FONT_INDEX,0)

            updateTextBox()

            val fontsAdapter = FontsRecyclerAdapter(FONTS, requireContext(), selectedFontIndex, this)
            editorFontListRV.adapter = fontsAdapter
            selectFont(selectedFontIndex)


        }

        selectBlackTextColor.setOnClickListener {
            (requireActivity() as EditorActivity).selectTextBlackColor(it)

            selectFont(selectedFontIndex)
        }

        selectWhiteTextColor.setOnClickListener {
            (requireActivity() as EditorActivity).selectTextWhiteColor(it)

            selectFont(selectedFontIndex)
        }

        editor_text_container?.visibility = if (Utils.sharefPrefs.getBoolean(MESSAGE_BOX_SHOW,false)) View.VISIBLE else View.GONE

    }

    /*

tvTitle.setTypeface(typeface, Typeface.BOLD_ITALIC);

Typeface typeface = getResources().getFont(R.font.caffee_regular);
tvTitle.setTypeface(typeface);
     */

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_text_font, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorTextFontFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }

    private class FontsHolder(v: View, val container: EditorTextFontFragment) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private var font: Int = 0
        private var index:Int = 0

        init {
            v.fontButton.setOnClickListener(this)
        }

        fun bindEvent(fontID: Int, context: Context, index:Int, selectedIndex:Int, container: EditorTextFontFragment) {
            this.font = fontID

            this.index = index

            view.fontButton.setText(if (isPost) "Post" else "Story")
            view.fontButton.isSelected = selectedIndex == index

            view.fontButton.setTypeface(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                view.context.resources.getFont(fontID)
            } else {
               ResourcesCompat.getFont(context, fontID)
            })
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            container.selectFont(index)
        }
    }

    fun selectFont(index: Int) {
        Utils.sharefPrefsEditor.putInt(FONT_INDEX,index).apply()

        selectedFontIndex = index

        val fontsAdapter = FontsRecyclerAdapter(FONTS, requireContext(), selectedFontIndex, this)
        editorFontListRV.adapter = fontsAdapter

        (requireActivity() as EditorActivity).selectFont(index)

        MainActivity.trackEvent(TagNames.TEXT_FONT_SELECTED, null)

        editor_text_container.editor_text_container_TextView.apply {
            typeface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                requireActivity().resources.getFont(FONTS[selectedFontIndex])
            } else {
                ResourcesCompat.getFont(requireActivity(), FONTS[selectedFontIndex])
            }
            setTextColor(Utils.sharefPrefs.getColor(FONT_COLOR))
          }
        }

    private class FontsRecyclerAdapter(
        val items: IntArray,
        val context: Context,
        val selectedFontIndex:Int,
        val container: EditorTextFontFragment
    ) : RecyclerView.Adapter<FontsHolder>() {

        override fun getItemCount(): Int {
            return items.size
        }

        override fun getItemViewType(position: Int): Int {
            return if (position == selectedFontIndex) 1 else 0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FontsHolder {
            return FontsHolder(
                LayoutInflater.from(context).inflate( R.layout.editor_text_font_list_element, parent, false),
                container
            )
        }

        override fun onBindViewHolder(holder: FontsHolder, position: Int) {
            holder.bindEvent(items[position], context,position, selectedFontIndex, container)
        }
    }
}