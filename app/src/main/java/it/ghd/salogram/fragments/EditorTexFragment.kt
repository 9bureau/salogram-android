package it.ghd.salogram.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.MotionEvent.ACTION_DOWN
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.core.view.doOnLayout
import com.segment.analytics.Analytics
import it.ghd.salogram.activities.EditorActivity
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import it.ghd.salogram.Utils
import it.ghd.salogram.activities.MainActivity
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.editor_text_container.*
import kotlinx.android.synthetic.main.fragment_editor_text.*
import org.json.JSONObject

private var mScaleDetector: ScaleGestureDetector? = null
private var mScaleFactor:Float = 1f
private const val ARG_PARAM1 = "param1"
var dX = 0f
var dY = 0f
var lastAction:Int  =-1

class EditorTextFragment : EditorMainFragment(), View.OnTouchListener {
    private lateinit var param1: JSONObject

    companion object {

        val FONTS:IntArray = intArrayOf(
            R.font.americantypewriterregular,
            R.font.futuramedium,
            R.font.georgiabold,
            R.font.snellroundhandblackscript,
            R.font.zapfinoextraltpro
        )

        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorTextFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onPause() {
        super.onPause()

        Utils.sharefPrefsEditor.putString(MESSAGE_BOX_TEXT,editor_text_container_TextView.text.toString()).apply()
        messageTextHiddenField.clearFocus()
        hideKeyboard()
    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorTextFragment")

        if (param1!=null) {
            //val b: Bitmap = (requireActivity() as EditorActivity).render
            //home_image_background.setImageBitmap(b)
        }

        editor_text_container?.visibility = if (Utils.sharefPrefs.getBoolean(MESSAGE_BOX_SHOW,true)) View.VISIBLE else View.GONE
        showKeyboard(messageTextHiddenField)
        updateTextBox()
    }

    private class ScaleListener(val editor_text_container:View) : SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mScaleFactor *= detector.scaleFactor

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f)).toFloat()

            Utils.log("Scale: $mScaleDetector")

            editor_text_container.layoutParams.width = (mScaleFactor *300f).toInt() //300 start value
            if ( editor_text_container.layoutParams.width<300){
                editor_text_container.layoutParams.width = 300
            }
            else if (editor_text_container.layoutParams.width>1920){
                editor_text_container.layoutParams.width = 1920
            }

            editor_text_container.layoutParams.height = (mScaleFactor *200f).toInt() + 80 //200 start value //80 is two buttons
            if ( editor_text_container.layoutParams.height<200){
                editor_text_container.layoutParams.height = 200
            }
            else  if ( editor_text_container.layoutParams.height>1920){
                editor_text_container.layoutParams.height = 1920
            }

            editor_text_container.doOnLayout {
                Utils.sharefPrefsEditor.apply {

                    editor_text_container.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

                    editor_text_container.forceLayout()
                    editor_text_container.requestLayout()

                    putFloat(MESSAGE_BOX_CENTER_X, editor_text_container.x + editor_text_container.width*0.5f)
                    putFloat(MESSAGE_BOX_CENTER_Y, editor_text_container.y + editor_text_container.height*0.5f)
                    putInt(MESSAGE_BOX_W, editor_text_container.width)
                    putInt(MESSAGE_BOX_H, editor_text_container.height)
                }.apply()
            }

            return true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        editor_text_container.visibility = View.VISIBLE
        editor_text_container.setOnTouchListener(this)

        mScaleDetector = ScaleGestureDetector(requireActivity(),
            ScaleListener(editor_text_container)
        )

        closeTextButton.visibility = View.VISIBLE
        scaleTextButton.visibility = View.VISIBLE
        editor_text_increase_font_sizeBT.visibility = View.VISIBLE
        editor_text_decrease_font_sizeBT.visibility = View.VISIBLE

        backButton.setOnClickListener {

            messageTextHiddenField.clearFocus()

            (requireActivity() as EditorActivity).gotoHomePage(null)
        }

        editor_text_increase_font_sizeBT.setOnClickListener {
            var current = Utils.sharefPrefs.getFloat(MESSAGE_BOX_TEXT_FONT_SIZE, 18f)
            current++

            Utils.sharefPrefsEditor.putFloat(MESSAGE_BOX_TEXT_FONT_SIZE,current).apply()
            editor_text_container_TextView.textSize = current
            MainActivity.trackEvent(TagNames.TEXT_FONTSIZE_INCREASE, null)
            true
        }

        editor_text_decrease_font_sizeBT.setOnClickListener {
            var current = Utils.sharefPrefs.getFloat(MESSAGE_BOX_TEXT_FONT_SIZE, 18f)
            current--

            if (current<=10){
                current = 10f
            }

            Utils.sharefPrefsEditor.putFloat(MESSAGE_BOX_TEXT_FONT_SIZE,current).apply()

            editor_text_container_TextView.textSize = current
            MainActivity.trackEvent(TagNames.TEXT_FONTSIZE_DECREASE, null)
            true
        }

        messageTextHiddenField.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
              messageTextHiddenField.clearFocus()
                hideKeyboard()
            }
            false
        })

        val vto: ViewTreeObserver = editor_text_container_TextView.viewTreeObserver
       /* vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                Toast.makeText(
                    requireActivity(),
                    "${editor_text_container_TextView.width} x ${editor_text_container_TextView.height}",
                    Toast.LENGTH_LONG
                ).show()
                editor_text_container_TextView.viewTreeObserver.removeGlobalOnLayoutListener(this)
            }
        })*/
        vto.addOnPreDrawListener {
            if (editor_text_container_TextView!=null) {
                val lineCount = editor_text_container_TextView.layout?.lineCount

                if (lineCount!=null) {
                   // Utils.log("Line counts: ${lineCount}")

                    var params = editor_text_container.layoutParams
                    params.height = lineCount*editor_text_container_TextView.lineHeight

                    if (params.height<250) {
                        params.height = 250
                    }

                    editor_text_container.layoutParams = params
                    editor_text_container.requestLayout()
                    editor_text_container_TextView.requestLayout()

                    Utils.sharefPrefsEditor.apply {
                        putInt(MESSAGE_BOX_H, params.height)
                    }.apply()

                }
            }
            true
        }

        messageTextHiddenField.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                val content = s.toString().trim()

                editor_text_container_TextView.setText( content)

                if (content.isEmpty()){
                    editor_text_container?.visibility = View.GONE
                    Utils.sharefPrefsEditor.putBoolean(MESSAGE_BOX_SHOW,false).apply()
                    Utils.sharefPrefsEditor.putString(MESSAGE_BOX_TEXT,"").apply()
                }
                else {
                    editor_text_container?.visibility = View.VISIBLE
                    Utils.sharefPrefsEditor.putBoolean(MESSAGE_BOX_SHOW,true).apply()
                    Utils.sharefPrefsEditor.putString(MESSAGE_BOX_TEXT,content).apply()
                }

                if (editor_text_container!=null) {

                    editor_text_container.doOnLayout {
                        editor_text_container_TextView.doOnLayout {

                             //editor_text_container.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

                            Utils.log("TEXTVIEW Height: ${editor_text_container_TextView.measuredHeight}")

                            //editor_text_container_TextView.measure(0,0)

                            /*val bounds = Rect()
                            bounds.set(0,0,editor_text_container.width,10000)
                            val textPaint: Paint = editor_text_container_TextView.paint
                            textPaint.getTextBounds(content, 0, content.length, bounds)
                            val height: Int = bounds.height()
                            val width: Int = bounds.width()

                            var params = editor_text_container.layoutParams
                            params.height = height
                            editor_text_container.layoutParams = params


                            editor_text_container_TextView.requestLayout()
                            editor_text_container.requestLayout()
                            editor_text_container_TextView.invalidate()

                            if (editor_text_container.layoutParams.height<300){
                                editor_text_container.layoutParams.height = 300

                                editor_text_container_TextView.requestLayout()
                                editor_text_container.requestLayout()
                            }
                            */

                            //editor_text_container_TextView.forceLayout()

                            //editor_text_container.forceLayout()
                            //editor_text_container.requestLayout()

                            Utils.log("CONTAINER Height: ${editor_text_container.measuredHeight}")

                            /*if (editor_text_container_TextView.measuredHeight+80<editor_text_container.layoutParams.height) {
                            editor_text_container.layoutParams.height = editor_text_container_TextView.measuredHeight + 80 //80 is x and scale buttons
                        }*/

                            Utils.sharefPrefsEditor.apply {

                                //val textPaint = TextPaint()
                                //textPaint.textSize = editor_text_container_TextView.textSize

                                //val width = textPaint.measureText(editor_text_container_TextView.text.toString())

                                //editor_text_container.layoutParams.width = width.toInt()+60 //margin

                                // putFloat(MESSAGE_BOX_CENTER_X, editor_text_container.x + editor_text_container.width*0.5f)
                                // putFloat(MESSAGE_BOX_CENTER_Y, editor_text_container.y + editor_text_container.height*0.5f)
                                //putInt(MESSAGE_BOX_W, editor_text_container.width)
                                //putInt(MESSAGE_BOX_H, editor_text_container.height)
                            }.apply()
                        }

                        //editor_text_container.requestLayout()
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                editor_text_container.requestLayout()
            }
        })

        closeTextButton.setOnClickListener {
            Utils.sharefPrefsEditor.putString(MESSAGE_BOX_TEXT,"").apply()
            Utils.sharefPrefsEditor.putBoolean(MESSAGE_BOX_SHOW,false).apply()
            editor_text_container?.visibility = View.GONE
        }

        /*if (Utils.isDebugger ||  Utils.isEmulator) {
            messageTextHiddenField.setText("Lorem ipsum asdadasdadd")
            editor_text_container_TextView.forceLayout()
            editor_text_container_TextView.requestLayout()
            editor_text_container.forceLayout()
            editor_text_container.requestLayout()
        }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_text, container, false)
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {

        mScaleDetector?.onTouchEvent(event);

        v?.parent?.requestDisallowInterceptTouchEvent(true);

        when (event!!.actionMasked) {
            ACTION_DOWN -> {

                showKeyboard(messageTextHiddenField)

                dX = editor_text_container!!.x - event.rawX
                dY = editor_text_container!!.y - event.rawY
                lastAction = ACTION_DOWN
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                editor_text_container!!.y = event.rawY + dY
                editor_text_container!!.x = event.rawX + dX
                lastAction = MotionEvent.ACTION_MOVE

                if (editor_text_container!=null) {
                    editor_text_container.let {
                        Utils.sharefPrefsEditor.apply {
                            putFloat(
                                MESSAGE_BOX_CENTER_X,
                                it.x + it.width * 0.5f
                            )
                            putFloat(
                                MESSAGE_BOX_CENTER_Y,
                                it.y + it.height * 0.5f
                            )
                            putInt(MESSAGE_BOX_W, it.width)
                            putInt(MESSAGE_BOX_H, it.height)
                        }.apply()
                    }
                }

                    return true
                }
            MotionEvent.ACTION_UP -> if (lastAction == ACTION_DOWN) {
                v?.performClick()
                return true
            }
            else -> {
                v?.performClick()
                return false
            }
        }
        return true
    }
}