package it.ghd.salogram.fragments

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.BitmapRequestListener
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.*
import it.ghd.salogram.activities.EditorActivity
import it.ghd.salogram.activities.MainActivity
import kotlinx.android.synthetic.main.editor_canvas_list_element.view.*
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.fragment_editor_canvas.*
import kotlinx.android.synthetic.main.loading_overlay.*
import org.json.JSONArray
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

class EditorCanvasFragment : EditorMainFragment() {
    private var contents: JSONArray = JSONArray()
    private lateinit var param1: JSONObject
    private var selectedIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorCanvasFragment")

        selectedIndex = Utils.sharefPrefs.getInt(CANVAS_INDEX,0)

        if (param1 != null) {
            //val b: Bitmap = (requireActivity() as EditorActivity).render
           // home_image_background.setImageBitmap(b)
        }

        cancelCanvasButton.setOnClickListener {

            MainActivity.trackEvent(TagNames.FRAME_NOT_ADDED, null)

            Utils.sharefPrefsEditor.apply{
                remove(CANVAS_URL)
                putInt(CANVAS_INDEX,-1)
                remove(CANVAS_INFO)
            }.apply()

            (requireActivity() as EditorActivity).gotoHomePage(null)
        }

        saveCanvasButton.setOnClickListener {
            MainActivity.trackEvent(TagNames.FRAME_ADDED, null)
            (requireActivity() as EditorActivity).gotoHomePage(null)
        }

        /*if (firsStart){
            firsStart = false*/
        //}

        //editor_text_container?.visibility = if (Utils.sharefPrefs.getBoolean(SHOW_TEXT_BOX,false)) View.VISIBLE else View.GONE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadData(null)
    }

    override fun loadData(view:View?){
        super.loadData(view)

        Manager.getAllFrames(info.getInt("id"), isPost) { error, message, jsonArray ->
            if (error && message != null) {
                try {
                    requireActivity().let {
                        if (it is MainActivity) {
                            it.askQuestion(
                                getString(R.string.errore),
                                message,
                                getString(R.string.riprova),
                                getString(R.string.annulla)
                            ) { confirmed ->
                                if (confirmed) loadData(null)
                                else {
                                    loadingOverlay?.visibility = View.GONE
                                }
                            }
                        } else {
                            //nnont MainActivity

                            val builder = AlertDialog.Builder(it)
                            builder.setTitle("Errore")
                            builder.setCancelable(false)
                            builder.setMessage(message ?: "")
                            //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

                            builder.setPositiveButton("Riprova") { dialog, which ->
                                loadData(null)
                            }

                            builder.setNegativeButton("Annulla") { dialog, which ->
                                loadingOverlay?.visibility = View.GONE
                            }

                            builder.show()
                        }
                    }//let
                        ?: run {
                            //no activity linked !
                            loadingOverlay?.visibility = View.GONE
                        }
                } catch (ex: IllegalStateException) {
                    loadingOverlay?.visibility = View.GONE
                }

            } else {
                selectedIndex = Utils.sharefPrefs.getInt(CANVAS_INDEX, -1)

                contents = jsonArray!!

                Utils.log("Found ${contents.length()} items for CANVASS for ${if (isPost) "posts" else "stories"}")

                val canvasAdapter = CanvasRecyclerAdapter(
                    contents,
                    requireActivity(),
                    selectedIndex,
                    this
                )
                editorCanvasListRV.adapter = canvasAdapter

                if (contents != null && contents.length() > 0 && selectedIndex >= 0) {
                    selectCanvas(selectedIndex, contents.getJSONObject(selectedIndex))
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_canvas, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorCanvasFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }

    fun selectCanvas(index: Int, info: JSONObject) {

        MainActivity.trackEvent(TagNames.FRAME_SELECTED, null)

        selectedIndex = index

        Utils.sharefPrefsEditor.apply{
            putString(CANVAS_URL,info.getString("url"))
            putInt(CANVAS_INDEX,index)
            putJSONObject(CANVAS_INFO,info)
        }.apply()

      AndroidNetworking.get(info.getString("url"))
            .setTag("imageRequestTag")
            .setPriority(Priority.HIGH)
            .setBitmapConfig(Bitmap.Config.ARGB_8888)
            .build()
            .getAsBitmap(object : BitmapRequestListener {
                override fun onResponse(bitmap: Bitmap) {
                    home_image_canvas.post {
                        home_image_canvas.setImageBitmap(bitmap)
                        canvasImage = bitmap
                        home_image_canvas.invalidate()
                        view?.invalidate()
                        imageContainer.invalidate()
                        home_image_canvas.visibility = View.VISIBLE
                    }

                }

                override fun onError(error: ANError) {
                    // handle error
                }
            })
    }

    private class CanvasHolder(v: View, val container: EditorCanvasFragment) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var view: View = v
        private lateinit var info: JSONObject
        private var index:Int = 0

        init {
            v.canvasButton.setOnClickListener(this)
        }

        fun bindEvent(json: JSONObject, context: Context, index:Int, selectedIndex:Int) {
            this.info = json

            this.index = index
            view.canvasButton.isSelected = selectedIndex == index
            ImageManager.loadImage(json.getString("thumb_url"), view.canvasImage)

        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
            container.selectCanvas(index,info)
        }
    }

    private class CanvasRecyclerAdapter(
        val items: JSONArray,
        val context: Context,
        val selectedFontIndex:Int,
        val container: EditorCanvasFragment
    ) : RecyclerView.Adapter<CanvasHolder>() {

        override fun getItemCount(): Int {
            return items.length()
        }

        override fun getItemViewType(position: Int): Int {
            return if (position == selectedFontIndex) 1 else 0
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CanvasHolder {
            return CanvasHolder(
                LayoutInflater.from(context).inflate( R.layout.editor_canvas_list_element, parent, false),container
            )
        }

        override fun onBindViewHolder(holder: CanvasHolder, position: Int) {
            holder.bindEvent(items.getJSONObject(position), context,position, selectedFontIndex)
        }
    }
}