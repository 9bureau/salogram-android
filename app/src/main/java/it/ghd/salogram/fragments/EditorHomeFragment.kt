package it.ghd.salogram.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import it.ghd.salogram.ImageManager
import it.ghd.salogram.R
import it.ghd.salogram.Utils
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.fragment_editor_home.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

class EditorHomeFragment : EditorMainFragment() {
    private lateinit var param1: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (param1!=null && param1.getString("url")!=null) {
            ImageManager.loadImage(param1.getString("url"), home_image_background)
        }

        //closeTextButton?.visibility = if (Utils.sharefPrefs.getBoolean(EditorActivity.SHOW_TEXT_BOX,false)) View.VISIBLE else View.GONE

    }

    override fun onResume() {
        super.onResume()
        Log.v("FRAMMENTI", "EditorHomeFragment")
        homeTextView.text = Utils.sharefPrefs.getString("SHARE_TEXT","")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_home, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(image:JSONObject) =
            EditorHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }
}