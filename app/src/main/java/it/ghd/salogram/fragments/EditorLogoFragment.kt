package it.ghd.salogram.fragments

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.*
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import it.ghd.salogram.activities.EditorActivity
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import it.ghd.salogram.Utils
import it.ghd.salogram.activities.MainActivity
import kotlinx.android.synthetic.main.editor_bottom_area.*
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.fragment_editor_logo.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

private var mScaleDetector: ScaleGestureDetector? = null
private var mScaleFactor:Float = 1f

class EditorLogoFragment : EditorMainFragment(), View.OnTouchListener {
    private lateinit var param1: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
            if (param1!=null){

            }
        }

    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorLogoFragment")

        if (param1!=null) {
            //val b:Bitmap = (requireActivity() as EditorActivity).render
            //home_image_background.setImageBitmap(b)
        }

        if (Utils.sharefPrefs.getInt(LOGO_INDEX,-1)>=0) {
            editor_logo_container.visibility = View.VISIBLE
            closeLogoButton.visibility = View.VISIBLE
        }
        else {
            editor_logo_container.visibility = View.GONE
            closeLogoButton.visibility = View.GONE

        }
        //closeTextButton?.visibility = if (Utils.sharefPrefs.getBoolean(EditorActivity.SHOW_TEXT_BOX,false)) View.VISIBLE else View.GONE

        saveLogoAction.setOnClickListener {
            (requireActivity() as EditorActivity).gotoHomePage(null)
            MainActivity.trackEvent(TagNames.LOGO_ADDED, null)
        }

        cancelLogoAction.setOnClickListener {
            Utils.sharefPrefsEditor.putInt(LOGO_INDEX,-1).apply()
            (requireActivity() as EditorActivity).gotoHomePage(null)
            MainActivity.trackEvent(TagNames.LOGO_NOT_ADDED, null)
        }
    }

    private class ScaleListener(val editor_logo_container:View, val home_image_logo:View) : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mScaleFactor *= detector.scaleFactor

            // Don't let the object get too small or too large.
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f)).toFloat()

            Utils.log("Scale: $mScaleFactor")

                val params: ViewGroup.LayoutParams = editor_logo_container.layoutParams

                params.width = (mScaleFactor * 350f).toInt() //300 start value
                if (params.width < 350) {
                    params.width = 350
                } else if (params.width > 1920) {
                    params.width = 1920
                }

                params.height = (mScaleFactor * 350f).toInt() //200 start value //80 is two buttons
                if (params.height < 350) {
                    params.height = 350
                } else if (params.height > 1920) {
                    params.height = 1920
                }

                editor_logo_container.layoutParams = params

                editor_logo_container.invalidate()
                editor_logo_container.requestLayout()
                home_image_logo.invalidate()
                home_image_logo.requestLayout()
                home_image_logo.forceLayout()

            Utils.log("${params.width}:${params.height}")

                // editor_logo_container.doOnLayout {
                Utils.sharefPrefsEditor.apply {

                    //editor_logo_container.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

                    //editor_logo_container.forceLayout()

                    putFloat(LOGO_X, editor_logo_container.x)
                    putFloat(LOGO_Y, editor_logo_container.y)
                    putInt(LOGO_W, params.width)
                    putInt(LOGO_H, params.height)
                }.apply()
                //}

            return true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mScaleDetector = ScaleGestureDetector(requireActivity(),
            ScaleListener(editor_logo_container,home_image_logo)
        )

        closeLogoButton.setOnClickListener {
            Utils.sharefPrefsEditor.putInt(LOGO_INDEX,-1).apply()
            editor_logo_container?.visibility = View.GONE
        }

        editor_logo_container.setOnTouchListener(this)
        editor_logo_container.layoutParams.width = Utils.sharefPrefs.getInt(LOGO_W,400)
        editor_logo_container.layoutParams.height = Utils.sharefPrefs.getInt(LOGO_H,300)
        editor_logo_container.x = Utils.sharefPrefs.getFloat(LOGO_X,100f)
        editor_logo_container.y = Utils.sharefPrefs.getFloat(LOGO_Y,100f)

        editor_logo_container.invalidate()
        editor_logo_container.requestLayout()
        home_image_logo.forceLayout()

        editor_bottom_area_logoBT.setOnClickListener(null) //default not used

        editor_bottom_area_logoBT.setOnClickListener {
            (requireActivity() as EditorActivity).selectLogo(this)
            MainActivity.trackEvent(TagNames.LOGO_SELECTED, null)
        }

        if (firsStart){
            firsStart = false

            editor_bottom_area_logoBT.performClick()
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {

        mScaleDetector?.onTouchEvent(event);

        v?.parent?.requestDisallowInterceptTouchEvent(true);

        when (event!!.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                dX = editor_logo_container.x - event.rawX
                dY = editor_logo_container.y - event.rawY
                lastAction = MotionEvent.ACTION_DOWN
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                editor_logo_container.y = event.rawY + dY
                editor_logo_container.x = event.rawX + dX
                lastAction = MotionEvent.ACTION_MOVE

                Utils.sharefPrefsEditor.apply {
                    putFloat(LOGO_X, editor_logo_container.x)
                    putFloat(LOGO_Y, editor_logo_container.y)
                    putInt(LOGO_W, editor_logo_container.width)
                    putInt(LOGO_H, editor_logo_container.height)
                }.apply()

                return true
            }
            MotionEvent.ACTION_UP -> if (lastAction == MotionEvent.ACTION_DOWN) {
                //v?.performClick()
                return true
            }
            else -> {
               // v?.performClick()
                return false
            }
        }
        return true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_logo, container, false)
    }

    fun updateLogo(btm:Bitmap) {
        home_image_logo.setImageBitmap(btm)
        Utils.sharefPrefsEditor.putInt(LOGO_INDEX,0).apply()
        editor_logo_container.visibility = View.VISIBLE
        closeLogoButton.visibility = View.VISIBLE
        MainActivity.trackEvent(TagNames.LOGO_UPDATED, null)
    }

    companion object {


        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorLogoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())

                }
            }
    }
}