package it.ghd.salogram.fragments

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import it.ghd.salogram.ImageManager
import it.ghd.salogram.Utils
import it.ghd.salogram.fragments.EditorTextFragment.Companion.FONTS
import it.ghd.salogram.getColor
import it.ghd.salogram.hideKeyboard
import kotlinx.android.synthetic.main.editor_bottom_area.*
import kotlinx.android.synthetic.main.editor_result_container.*
import kotlinx.android.synthetic.main.editor_text_container.*
import org.json.JSONObject

open class EditorMainFragment : Fragment(){
    var firsStart: Boolean = true

    companion object {
        val USER_ACCOUNTS = "user_accounts"
        var info:JSONObject = JSONObject()
        var isPost:Boolean = true
        var isVideo:Boolean = false

        lateinit var image:Bitmap
        var imageIsInitialized = false

        lateinit var canvasImage:Bitmap
        lateinit var logoImage:Bitmap

        val FONT_INDEX = "selectedFontIndex"
        val FONT_NAME = "selectedFontName"
        val FONT_COLOR = "selectedFontColor"

        val CANVAS_INDEX = "selectedCanvasIndex"
        val CANVAS_URL = "selectedCanvasUrl"
        val CANVAS_INFO = "selectedCanvasInfo"

        val CAPTION_TEXT = "selectedCaption"

        val IMAGE = "selectedImage"

        val MESSAGE_BOX_TEXT_FONT_SIZE = "selectedMessageBoxFontSize"
        val MESSAGE_BOX_TEXT = "selectedMessageBox"
        val MESSAGE_BOX_SHOW = "selectedMessageBoxShow"
        val MESSAGE_BOX_CENTER_X = "selectedMessageBoxX"
        val MESSAGE_BOX_CENTER_Y = "selectedMessageBoxY"
        val MESSAGE_BOX_W = "selectedMessageBoxW"
        val MESSAGE_BOX_H = "selectedMessageBoxH"

        val LOGO_INDEX = "selectedLogoIndex"
        val LOGO_URL = "selectedLogoUrl"
        val LOGO_X = "selectedLogoBoxX"
        val LOGO_Y = "selectedLogoBoxY"
        val LOGO_W = "selectedLogoBoxW"
        val LOGO_H = "selectedLogoBoxH"

        val FB_LOGGED_USER = "fbLoggedUser"
        val FB_PAGE = "fbPageSelected"

        /*fun loadImage(url:String){

            Glide.with(CustomApplication.context)
                .asBitmap()
                .load(url)
                .into(object : CustomTarget<Bitmap>(){

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // this is called when imageView is cleared on lifecycle call or for
                        // some other reason.
                        // if you are referencing the bitmap somewhere else too other than this imageView
                        // clear it here as you can no longer have the bitmap
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                    ) {
                        image = resource
                    }
                })
        }*/
    }


    fun hideKeyboard() {
        val view = requireActivity().currentFocus
        if (view!=null) {
            val methodManager =
                requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            methodManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
            view.clearFocus()
        }

        activity?.hideKeyboard()
    }

    fun updateTextBox() {

        editor_text_container?.visibility =
            if (Utils.sharefPrefs.getBoolean(MESSAGE_BOX_SHOW, true)) View.VISIBLE else View.GONE

        val selectedFontIndex = Utils.sharefPrefs.getInt(FONT_INDEX, 0)

        editor_text_container_TextView?.apply {
            setTextColor(Utils.sharefPrefs.getColor(FONT_COLOR))

            text = Utils.sharefPrefs.getString(MESSAGE_BOX_TEXT, "")

            textSize = Utils.sharefPrefs.getFloat(MESSAGE_BOX_TEXT_FONT_SIZE , 18f)

            typeface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                requireActivity().resources.getFont(FONTS[selectedFontIndex])
            } else {
                ResourcesCompat.getFont(requireActivity(), FONTS[selectedFontIndex])
            }


        }

        updateLogoBox()
    }

    private fun updateLogoBox() {
        editor_logo_container?.visibility = if (Utils.sharefPrefs.getInt(LOGO_INDEX,-1)>=0) View.VISIBLE else View.GONE

    }

    open fun loadData(view:View?){

    }

    fun showKeyboard(target:View) {
        target.requestFocus()

        val view = requireActivity().currentFocus
        val methodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view!=null)
        methodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun onResume() {
        super.onResume()

        updateTextBox()

        Utils.sharefPrefs.apply {

            editor_text_container.x =  getFloat(MESSAGE_BOX_CENTER_X, 0f)-getInt(MESSAGE_BOX_W, 300)*0.5f
            editor_text_container.y =  getFloat(MESSAGE_BOX_CENTER_Y, 0f)-getInt(MESSAGE_BOX_H, 200)*0.5f
            editor_text_container.layoutParams = FrameLayout.LayoutParams(getInt(MESSAGE_BOX_W, 300), getInt(
                MESSAGE_BOX_H, 200))

            if (getInt(CANVAS_INDEX,-1)>=0) {
                home_image_canvas.visibility = View.VISIBLE
                ImageManager.loadImage(getString(CANVAS_URL, ""), home_image_canvas)
            }
            else {
                home_image_canvas.visibility = View.GONE
            }

            //LOGO

            if (getInt(LOGO_INDEX,-1)>=0) {
                home_image_logo?.setImageBitmap(logoImage)

                editor_logo_container?.visibility = View.VISIBLE
                editor_logo_container.x = getFloat(LOGO_X,100f)
                editor_logo_container.y = getFloat(LOGO_Y,100f)
                editor_logo_container.layoutParams = FrameLayout.LayoutParams(getInt(LOGO_W, 350), getInt(
                    LOGO_H, 350))

            }
            else {
                editor_logo_container?.visibility = View.GONE
            }

        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        playVideoButton.visibility = if (info.has("url") && info.getString("url").contains("mp4")){
           View.VISIBLE
        }
        else {
            View.GONE
        }

        buttonsContainer?.alpha = if (!isVideo) 1f else 0.4f

        editor_bottom_area_testoBT?.isClickable = !isVideo
        editor_bottom_area_testoBT?.isSelected = true

        editor_bottom_area_logoBT?.isClickable = !isVideo
        editor_bottom_area_logoBT?.isSelected = true

        editor_bottom_area_corniteBT?.isClickable = !isVideo
        editor_bottom_area_corniteBT?.isSelected = true

        if (imageIsInitialized) {
            home_image_background?.setImageBitmap(image)
            Log.v("FACEBOOKSHARE", "immagine inizializzata, la carico")
        }
        else {
            Log.v("FACEBOOKSHARE", "immagine non inizializzata, non la carico")
        }
    }
}