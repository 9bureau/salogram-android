package it.ghd.salogram.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.segment.analytics.Analytics
import it.ghd.salogram.activities.MainActivity
import it.ghd.salogram.R
import it.ghd.salogram.TagNames
import it.ghd.salogram.Utils
import kotlinx.android.synthetic.main.fragment_editor_caption.*
import kotlinx.android.synthetic.main.fragment_editor_caption_edit.*
import kotlinx.android.synthetic.main.fragment_editor_text.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"

class EditorCaptionEditFragment : EditorMainFragment() {
    private lateinit var param1: JSONObject

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = JSONObject(it.getString(ARG_PARAM1))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        Log.v("FRAMMENTI", "EditorCaptionEditFragment")

        //val b: Bitmap = (requireActivity() as EditorActivity).render
        //home_image_background.setImageBitmap(b)

        val testoIniziale = Utils.sharefPrefs.getString(CAPTION_TEXT,"")
        editTextTextMultiLine.setText(testoIniziale)

        showKeyboard(editTextTextMultiLine)

        (requireActivity() as MainActivity).showKeyboard(editTextTextMultiLine)

        editTextTextMultiLine.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.v("SCHERMATA", "PRIMA MODIFICA TESTO")
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.v("SCHERMATA", "TESTO MODIFICATO")
                MainActivity.trackEvent(TagNames.ACTION_COPY_EDITED, null)
            }

            override fun afterTextChanged(s: Editable) {
                Log.v("SCHERMATA", "DOPO MODIFICA TESTO")
            }
        })

        //closeTextButton?.visibility = if (Utils.sharefPrefs.getBoolean(EditorActivity.SHOW_TEXT_BOX,false)) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_editor_caption_edit, container, false)
    }

    companion object {


        @JvmStatic
        fun newInstance(image: JSONObject) =
            EditorCaptionEditFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, image.toString())
                }
            }
    }
}