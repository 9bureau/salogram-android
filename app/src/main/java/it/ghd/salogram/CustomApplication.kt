package it.ghd.salogram

//import com.segment.analytics.android.integrations.google.analytics.GoogleAnalyticsIntegration
import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Build.MANUFACTURER
import android.os.Build.MODEL
import android.os.Bundle
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.BuildConfig
import com.facebook.appevents.AppEventsLogger
import com.facebook.appevents.internal.ActivityLifecycleTracker.getCurrentActivity
import com.facebook.login.LoginManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.gson.Gson
import com.jacksonandroidnetworking.JacksonParserFactory
import com.onesignal.OSNotificationOpenedResult
import com.onesignal.OSNotificationReceivedEvent
import com.onesignal.OneSignal
import com.segment.analytics.Analytics
import com.segment.analytics.Properties
import com.segment.analytics.Traits
import com.segment.analytics.android.integrations.firebase.FirebaseIntegration
import com.segment.analytics.android.integrations.mixpanel.MixpanelIntegration
import it.ghd.salogram.Manager.Companion.logout
import it.ghd.salogram.activities.*
import it.ghd.salogram.activities.MainActivity.Companion.trackEvent
import it.ghd.salogram.fragments.EditorMainFragment
import it.ghd.salogram.model.Notification
import it.ghd.salogram.model.Owner
import okhttp3.OkHttpClient
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class CustomApplication : Application() {

    private val processLifeCycleListener: ProcessLifeCycleListener = ProcessLifeCycleListener()

    fun getDeviceModel(): String =
        (if (MODEL.startsWith(MANUFACTURER, ignoreCase = true)) {
            MODEL
        } else {
            "$MANUFACTURER $MODEL"
        }).capitalize()

    private fun setupSegment(){

        /*
        val mixpanel = MixpanelAPI.getInstance(context, "d9c480d05418623c6a5db34134abefa6")
        val props = JSONObject()
        props.put("Gender", "Female")
        props.put("Plan", "Premium")
        mixpanel.track("Plan Selected", props)
        */

        // Create an analytics client with the given context and Segment write key.
        val analytics = Analytics.Builder(context, "qOLvp1Z5ItffO7cmZM3zuSHnXhdwUGDR")
            .use(FirebaseIntegration.FACTORY)
            //.use(GoogleAnalyticsIntegration.FACTORY)
            .use(MixpanelIntegration.FACTORY)
            .trackApplicationLifecycleEvents() // Enable this to record certain application events automatically!
            //.recordScreenViews() // Enable this to record screen views automatically!
            .build()

        // Set the initialized instance as a globally accessible instance.
        Analytics.setSingletonInstance(analytics);
        trackEvent(TagNames.APPLICATION_STARTED, null)

        //todo: CONTROLLO SE LOGGATO
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
        ImageManager(this) //init
        registerActivityLifecycleCallbacks(AppLifeCycleCallback())
        setupSegment()
        AppEventsLogger.activateApp(this)

        val versionName = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        val versionCode = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            context.packageManager.getPackageInfo(context.packageName, 0).longVersionCode
        } else {
            context.packageManager.getPackageInfo(context.packageName, 0).versionCode
        }

       val okHttpClient = OkHttpClient().newBuilder()

           .connectTimeout(60, TimeUnit.SECONDS)
           .writeTimeout(60, TimeUnit.SECONDS)
           .readTimeout(60, TimeUnit.SECONDS)

           .addNetworkInterceptor { chain ->
                val originalRequest = chain.request()
                 val tokenedRequest = originalRequest.newBuilder()
                    .header("device-os", "${android.os.Build.VERSION.SDK_INT}")
                     .header("device-type", "Android")
                     .header("device-model", getDeviceModel())
                     .header("app-build", "$versionCode")
                     //.header("app-build", "16583142")
                    .header("app-version", "$versionName")
                    .build()
                chain.proceed(tokenedRequest)
            }
            .build()

        AndroidNetworking.initialize(applicationContext, okHttpClient)

        AndroidNetworking.setParserFactory(JacksonParserFactory())
        if (BuildConfig.DEBUG) {
            //AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY); //DISABLED DUE TO UPLOAD HANGING WHEN ENABLED...
        }

        initOneSignal()
    }

    private fun startPostActivity(post: JSONObject?) {

        if (post == null) {
            Log.v("NOTIFICHE", "[startPostActivity] esco")
            return
        }

        val idNotifica = 0
        //Manager.sendNotificationView(idNotifica) { error, message ->

        //} //update server

        val intentPost = Intent(applicationContext, EditorActivity::class.java)
        intentPost.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
        intentPost.putExtra("DATA", post)
        intentPost.putExtra("POST", true)
        startActivity(intentPost)

        Log.v("NOTIFICHE", "[startPostActivity] avvio activity")
    }

    private fun initOneSignal() {
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);

        OneSignal.initWithContext(this)
        OneSignal.setAppId("62694546-20bc-4a17-906b-5775264747f8")
        OneSignal.unsubscribeWhenNotificationsAreDisabled(true)
        OneSignal.pauseInAppMessages(false)
        OneSignal.setLocationShared(false)

        OneSignal.setNotificationOpenedHandler { result: OSNotificationOpenedResult ->

            OneSignal.onesignalLog(OneSignal.LOG_LEVEL.VERBOSE, "OSNotificationOpenedResult result: $result")

            Log.v("NOTIFICHE", "setNotificationOpenedHandler")
            //Log.v("NOTIFICHE", "OSNotificationOpenedResult result: $result")

            val notification = result.notification
            val data = notification.additionalData
            if (data == null) {
                Log.v("NOTIFICHE", "OSNotificationOpenedResult data null")
                return@setNotificationOpenedHandler
            }

            Log.v("NOTIFICHE", "OSNotificationOpenedResult data: $data")

            if (Utils.sharefPrefs.contains(Manager.SHARED_PREFERENCES_USER_FIELD)) {
                val utente = Utils.sharefPrefs.getJSONObject(Manager.SHARED_PREFERENCES_USER_FIELD)
                Log.v("NOTIFICHE", "utente in prefs")
                Log.v("NOTIFICHE", utente.toString())
            }

            val resultObj = Gson().fromJson(data.toString(), Notification::class.java)

            if (resultObj.owners != null) {
                if (resultObj.owners!!.isNotEmpty()) {

                    val notificationOwner = resultObj.owners!![0]
                    val notificationOwnerSlug = resultObj.owners_slug?.get(0)

                    if (Utils.sharefPrefs.contains("owner")) {
                        val ownerJson = Utils.sharefPrefs.getJSONObject("owner")

                        val owner = Owner.parse(ownerJson)
                        if (owner != null) {

                            Log.v(
                                "NOTIFICHE",
                                "OSNotificationOpenedResult ower notifica: ${notificationOwner}"
                            )

                            Log.v(
                                "NOTIFICHE",
                                "OSNotificationOpenedResult ower utente: ${owner.id}"
                            )
                            Log.v(
                                "NOTIFICHE",
                                "OSNotificationOpenedResult = " + notificationOwnerSlug
                            )

                            if (owner.id != notificationOwner) {
                                //diverso

                                Log.v(
                                    "NOTIFICHE",
                                    "L'utente è loggato con altro gruppo (${owner.slug}), devo cambiare"
                                )
                                val emailOriginal =
                                    Utils.sharefPrefs.getString("email", "").toString().trim()
                                val email =
                                    emailOriginal.split("@")[0] + "+" + notificationOwnerSlug + "@" + emailOriginal.split(
                                        "@"
                                    )[1]
                                val password =
                                    Utils.sharefPrefs.getString("password", "").toString().trim()

                                Log.v(
                                    "NOTIFICHE",
                                    "nuova email per il login = $email e password $password"
                                )

                                /*
                            Manager.login(
                                email,
                                password,
                                false
                            ) { error, message, firstStart, userID, owners, needUpdate ->

                                if (error) {
                                    Log.v("NOTIFICHE", "[login after notifica] errore: $message")
                                } else {
                                    Log.v("NOTIFICHE", "[login after notifica] success: $message, user id = $userID")

                                    startAppLogin(userID, email, firstStart)
                                }
                            }
                            */

                                Manager.logged = false

                                if (Utils.sharefPrefs.contains(Manager.SHARED_PREFERENCES_USER_FIELD)) {
                                    Utils.sharefPrefsEditor.remove(Manager.SHARED_PREFERENCES_USER_FIELD)
                                        .apply()
                                }

                                if (!Utils.sharefPrefs.contains(EditorMainFragment.USER_ACCOUNTS)) {
                                    Utils.sharefPrefsEditor.remove(EditorMainFragment.USER_ACCOUNTS)
                                        .apply()
                                }

                                val currActivity = getCurrentActivity()
                                Log.v(
                                    "NOTIFICHE",
                                    "activity corrente ora 1 = " + currActivity?.localClassName.toString()
                                )
                                currActivity?.finish()

                                Log.v("NOTIFICHE", "eseguo logout")
                                logout() { error, message ->

                                    Log.v("NOTIFICHE", "logout fatto")
                                    //le preferenze sono state svuotate

                                    Utils.sharefPrefsEditor.apply {
                                        remove("email")
                                        putString(email, "email")
                                        remove("password")
                                        putString(password, "password")
                                        remove("autologin")
                                        putBoolean("autologin", true)
                                        putJSONObject("notification_post", data)
                                    }.apply()

                                    LoginManager.getInstance().logOut()
                                    Log.v("NOTIFICHE", "Custom App logout")

                                    val currActivity1 = getCurrentActivity()
                                    Log.v(
                                        "NOTIFICHE",
                                        "activity corrente ora 2 = " + currActivity1?.localClassName.toString()
                                    )

                                    val autologin = Utils.sharefPrefs.getBoolean("autologin", false)
                                    Log.v("NOTIFICHE", "autologin = " + autologin)

                                    /*
                                Utils.sharefPrefs.all.map {
                                    Log.v("NOTIFICHE", "preferenza: " + it.key + " : " + it.value)
                                }
                                */

                                    /*

                                }*/
                                    if (currActivity1?.localClassName.equals(
                                            "activities.LoginActivity",
                                            true
                                        )
                                    ) {
                                        Log.v("NOTIFICHE", "forza login")
                                        (currActivity1 as LoginActivity).forzaLogin(email, password)
                                    } else {
                                        Log.v("NOTIFICHE", "non forzo login")

                                        val intent = Intent(this, LoginActivity::class.java).apply {
                                            flags = FLAG_ACTIVITY_NEW_TASK
                                            putExtra("forced_email", email)
                                            putExtra("forced_password", password)
                                        }

                                        Log.v("NOTIFICHE", "lancio schermata login")
                                        startActivity(intent)
                                        currActivity1?.finish()
                                        Log.v("NOTIFICHE", "chiudo activity corrente")
                                        //val currActivity2 = getCurrentActivity()
                                        //Log.v("NOTIFICHE", "activity front ora è " + currActivity2?.localClassName)
                                        //(currActivity2 as LoginActivity).forzaLogin(email, password)
                                    }
                                }

                                Log.v("NOTIFICHE", "esco qui")

                                return@setNotificationOpenedHandler
                            } else {
                                Log.v("NOTIFICHE", "NON CAMBIO LOGIN UTENT PER NOTIFICA")
                            }
                        }
                    }
                }
            }

            Log.v("NOTIFICHE", "OSNotificationOpenedResult data: $data")

            //PROVA QUI

            if (Manager.logged) {
                Log.v("NOTIFICHE", "sono loggato")
                startPostActivityIfValidNotification(notification.additionalData)
            }
            else {

                Log.v("NOTIFICHE", "non sono loggato")

                if (data != null) {

                    Log.v("NOTIFICHE", "notification data non nulla")

                    Utils.sharefPrefsEditor.putJSONObject("notification_post", data)

                    if (Utils.sharefPrefs.getBoolean("autologin", false)) {

                        Log.v("NOTIFICHE", "autologin abilitato")

                        val email = Utils.sharefPrefs.getString("email", "").toString().trim()
                        val password = Utils.sharefPrefs.getString("password", "").toString().trim()

                        if (!(email.isNotEmpty() && password.isNotEmpty())) {
                            Utils.sharefPrefsEditor.remove("autologin").apply()
                        }
                    }
                } else {
                    Log.v("NOTIFICHE", "notification data è nulla")
                }
            }
        }

        OneSignal.setNotificationWillShowInForegroundHandler { notificationReceivedEvent: OSNotificationReceivedEvent ->

            OneSignal.onesignalLog(
                OneSignal.LOG_LEVEL.VERBOSE, "NotificationWillShowInForegroundHandler fired!" +
                        " with notification event: " + notificationReceivedEvent.toString()
            )

            Log.v("NOTIFICHE", "Notifica in foreground")

            val notification = notificationReceivedEvent.notification

            /*
            val notifica = notification.toJSONObject()
            Log.v("NOTIFICHE", notifica.toString())
            Log.v("NOTIFICHE data", data.toString())

            val titolo = notifica.getString("title")
            val corpo = notifica.getString("body")
            */


            if (Utils.sharefPrefs.contains(Manager.SHARED_PREFERENCES_USER_FIELD)) {
                val utente = Utils.sharefPrefs.getJSONObject(Manager.SHARED_PREFERENCES_USER_FIELD)
                Log.v("NOTIFICHE", "utente in prefs")
                Log.v("NOTIFICHE", utente.toString())
            } else {
                Log.v("NOTIFICHE", "utente non in prefs 1")
            }

            if (Manager.logged) {
                Log.v("NOTIFICHE", "utente loggato 1")

                //startPostActivityIfValidNotification(notification.additionalData)
            }
            else {
                Log.v("NOTIFICHE", "utente non loggato 1")
            }

            notificationReceivedEvent.complete(notification) // o null
        }

        Utils.log("Onesignal token: ${Utils.pushToken}")
        /*
        OneSignal.idsAvailable { userId, registrationId ->

            Log.d("debug", "User:$userId RegistrationID: $registrationId")

            if (registrationId != null) {
                Log.d(
                    "debug",
                    "registrationId:$registrationId"
                )
                Utils.pushToken = registrationId
                Utils.pushToken = OneSignal.getPermissionSubscriptionState().subscriptionStatus.pushToken

            }

            if (userId!=null){
                Utils.oneSignalUserID = userId

                Manager.recordDeviceInfo { error, message ->
                    //record if logged
                }
            }
        }
        Utils.pushToken = OneSignal.getPermissionSubscriptionState().subscriptionStatus.pushToken
*/

    }

    private fun startAppLogin(userID: String?, username: String, firstStart: Boolean) { //, owner: Owner?

        Analytics.with(context).reset()
        Analytics.with(context).identify(userID!!, Traits().putEmail(username), null)
        /*
        TODO
        if (owner != null) {
            Analytics.with(context).group(owner.name)
            Log.v("NOTIFICHE", "group = " + owner.name)
        }
        */

        var intent: Intent
        var notificationPostIntent: Intent? = null

        //ok
        if (firstStart) {
            intent = Intent(this, RegistrazioneActivity::class.java).apply {
                putExtra("firstStart", true)
            }

        } else {

            if (Utils.sharefPrefs.contains("notification_post")) {
                val data = Utils.sharefPrefs.getJSONObject("notification_post")
                notificationPostIntent = Intent(this, EditorActivity::class.java)
                notificationPostIntent.flags =
                    Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
                notificationPostIntent.putExtra("DATA", data)
                notificationPostIntent.putExtra("POST", true)
                Utils.sharefPrefsEditor.remove("notification_post")
            }

            intent = Intent(this, PostHomeActivity::class.java).apply {}
        }

        val properties = Properties()
        properties["first_access"] = firstStart
        properties["autologin"] = false
        trackEvent(TagNames.USER_LOGIN, properties)

        Log.v("NOTIFICHE", "[start Home Activity]")
        startActivity(intent)
        if (notificationPostIntent != null) {
            Log.v("NOTIFICHE", "[start Post Activity]")
            startActivity(notificationPostIntent)
        }
    }

    private fun startPostActivityIfValidNotification(data: JSONObject?){
        if (data != null) {

            Log.v("NOTIFICHE", "additional data")
            //Log.v("NOTIFICHE", data.toString())

            if (data.has("id") && data.has("name") && data.has("url")) {
                Log.v("NOTIFICHE", "procedo")
                startPostActivity(data)
            } else {
                Log.v("NOTIFICHE", "non procedo")
            }
        }
    }

    private fun automaticLogin(email: String, password: String, data: JSONObject?) {

        Manager.login(email, password, true) { error, message, firstStart, userID, owners, askPhoneConfirm ->

            if (error && message != null) {
                Log.v("NOTIFICHE", "Errore login automatico")
                Log.v("NOTIFICHE", message)
            }
            else {
                if (data != null) {
                    if (data.has("id") && data.has("name") && data.has("url")) {
                        Log.v("NOTIFICHE", "procedo al post")
                        val properties = Properties().apply {
                            putValue("first_access", false)
                        }
                        MainActivity.trackEvent(TagNames.USER_LOGIN, properties)
                        startPostActivity(data)
                    } else {
                        Log.v("NOTIFICHE", "non procedo")
                    }
                }
            }
        }
    }

    inner class AppLifeCycleCallback : ActivityLifecycleCallbacks {
        private var numberOfActivities = 0
        override fun onActivityPaused(activity: Activity) {
        }

        override fun onActivityStarted(activity: Activity) {
            if (numberOfActivities == 0) {
                processLifeCycleListener.onAppForeground()
            }
            numberOfActivities += 1
        }

        override fun onActivityDestroyed(activity: Activity) {
        }

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        }

        override fun onActivityStopped(activity: Activity) {
            if (numberOfActivities == 1) {
                processLifeCycleListener.onAppBackground()
            }
            numberOfActivities--
        }

        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        }

        override fun onActivityResumed(activity: Activity) {
        }
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
            // Works for Activity
            // Get called every-time when application went to background.
            Log.d("BuyByMe", "onTrimMemory UI_HIDDEN...")
        } else if (level == ComponentCallbacks2.TRIM_MEMORY_COMPLETE) {
            // Works for FragmentActivty
            Log.d("BuyByMe", "onTrimMemory COMPLETE...")
        }
    }

    companion object {
        lateinit var mContext:CustomApplication
        val context:Context
            get() = mContext
    }
}